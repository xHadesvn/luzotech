<?php

if (!defined('ABSPATH')) exit;

// Check for Visual Composer
// -------------------------
if (!defined('__VC_EXTENSIONS__')){
	define('__VC_EXTENSIONS__', QODUX_FW_PATH);
}

// Main Class for Visual Composer Extensions
// -----------------------------------------

if (!class_exists('WT_VC_EXTENSIONS')) {
	
	// Create Plugin Class
	// -------------------
	class WT_VC_EXTENSIONS {
		
		public $WT_VCSC_Elements = array(
			"WT Blog Grid"				            => array("setting" => "BlogGrid",                 "file" => "blog_grid",     	       "type" => "internal",		"active" => "true"),
			"WT Portfolio"				            => array("setting" => "Portfolio",                "file" => "portfolio",     	       "type" => "internal",		"active" => "true"),
			"WT Contact Form"					    => array("setting" => "ContactForm",              "file" => "contact_form",     	    "type" => "internal",		"active" => "true"),
			"WT Social Networks"					=> array("setting" => "SocialNetworks",           "file" => "social_networks",     	    "type" => "internal",		"active" => "true"),
			"WT Bx Rotator"					        => array("setting" => "BxRotator",                "file" => "bx_rotator",     	       "type" => "internal",		"active" => "true"),
			"WT Services Slider"					=> array("setting" => "ServicesSlider",           "file" => "services_slider",     	    "type" => "internal",		"active" => "false"),
			"WT Testimonials Slider"				=> array("setting" => "TestimonialsSlider",       "file" => "testimonials_slider",     	"type" => "internal",		"active" => "true"),
			"WT Clients"					        => array("setting" => "Clients",                  "file" => "clients",     	
"type" => "internal",		"active" => "true"),
			"WT Team"					            => array("setting" => "Team",                     "file" => "team",     	            "type" => "internal",		"active" => "true"),
			"WT Timeline"					        => array("setting" => "Timeline",                 "file" => "timeline",               "type" => "internal",		"active" => "true"),
			"WT Testimonial"					    => array("setting" => "Testimonial",              "file" => "testimonial",     	      "type" => "internal",		"active" => "true"),
			"WT Service Box"					    => array("setting" => "ServiceBox",               "file" => "service_box",      	    "type" => "internal",		"active" => "true"),
			"WT Services"					        => array("setting" => "Services",                 "file" => "services",         	    "type" => "internal",		"active" => "true"),
			"WT Counter"					        => array("setting" => "Counter",                  "file" => "counter",     	            "type" => "internal",		"active" => "true"),
			"WT Tag"					            => array("setting" => "Tag",                      "file" => "tag",     	                "type" => "internal",		"active" => "true"),
			"WT Spacer"					            => array("setting" => "Spacer",                   "file" => "spacer",     	                "type" => "internal",		"active" => "true"),
			"WT Pricing Table"					    => array("setting" => "PricingTable",             "file" => "pricing_table",     	
"type" => "internal",		"active" => "true"),
			"WT Custom Heading"					    => array("setting" => "CustomHeading",            "file" => "custom_heading",     	
"type" => "internal",		"active" => "true"),
			"WT Section Headings"				    => array("setting" => "SectionHeadings",          "file" => "section_headings",     	
"type" => "internal",		"active" => "true"),
			"WT Galler Grid"				        => array("setting" => "GalleryGrid",              "file" => "gallery_grid",     	
"type" => "internal",		"active" => "true"),
		);

		function __construct() {
			
			// Adding WhoaThemes classes to standard VC shortcodes
			// --------------------------------------------
			add_filter('vc_shortcodes_css_class',       array($this, 'WT_VCSC_Extensions_Css_Classes'), 10, 2);			
			
			// Load External Files on Back-End
			// -------------------------------------------
			add_action('admin_enqueue_scripts', 		array($this, 	'WT_VCSC_Extensions_Admin'));
			
			// Load External Files on Front-End
			// --------------------------------------------
			add_action('wp_enqueue_scripts', 			array($this, 	'WT_VCSC_Extensions_Front'), 		999999999999999999999999999);
										
			// // Load Composer Elements ( Maps + Shortcodes Output )
			// --------------------------------------------
			foreach ($this->WT_VCSC_Elements as $ElementName => $element) {
				// if not WhoaThemes
				//if () {
					if ($element['active'] == "true") {
						if ($element['type'] == 'internal') {
							//require_once($this->shortcode_dir.'/wt_vcsc_' . $element['file'] . '.php');
							require_once(QODUX_FW_PATH . '/shortcodes/elements/wt_vcsc_' . $element['file'] . '.php');
						}
					}	
			}
																			
			// Load Extended Composer Elements
			// --------------------------------------------
			add_action('init', 							array($this, 'WT_VCSC_RegisterWithComposer'), 999999999);
			
			// Load Helper Functions
			// --------------------------------------------
			add_action('init', 							array($this, 'WT_VCSC_HelperFunctions'));
			
		}
		// ! Generate param type "wt_separator"
		// --------------------------------------------		
		function wt_separator_settings_field($settings, $value) {
			$dependency = function_exists('vc_generate_dependencies_attributes') ? vc_generate_dependencies_attributes($settings) : '';
			$param_name  = isset($settings['param_name']) ? $settings['param_name'] : '';
			$type        = isset($settings['type']) ? $settings['type'].'_field' : '';
            $separator	 = isset($settings['separator']) ? $settings['separator'] : '';	
			
			$output      = '';			
			$output .= '<div class="wpb_vc_param_value ' . $param_name . ' ' . $type . '" name="' . $param_name . '" style="border-bottom: 2px solid #DEDEDE; margin-bottom: 10px; margin-top: 10px; padding-bottom: 10px; font-size: 18px; color: #BEBEBE;" ' . $dependency . '>' . $separator . '</div>';
			return $output;
		}
		
		// ! Generate param type "wt_multiselect"
		// --------------------------------------------	
		function qodux_multiselect_settings_field($settings, $value){
			$param_name   = isset($settings['param_name']) ? $settings['param_name'] : '';
			$param_option = isset($settings['options']) ? $settings['options'] : '';
			
			$output = '';			
			$output .= '<input type="hidden" name="' . $param_name . '" id="' . $param_name . '" class="wpb_vc_param_value ' . $param_name . '" value="' . $value . '"/>';
			
			$output .= '<select multiple id="' . $param_name . '_select2" name="' . $param_name . '_select2" class="multi-select">';
			if ($param_option != '' && is_array($param_option)) {
				foreach ($param_option as $text_val => $val) {
					if (is_numeric($text_val) && (is_string($val) || is_numeric($val))) {
						$text_val = $val;
					}
					$output .= '<option id="' . $val . '" value="' . $val . '">' . htmlspecialchars($text_val) . '</option>';
				}
			}
			$output .= '</select>';
						
			$output .= '<script type="text/javascript">
						jQuery(document).ready(function($){
		
							$("#' . $param_name . '_select2").selectize();
							
							/*
							var order = $("#' . $param_name . '").val();
							if (order != "") {
								order = order.split(",");
								var choices = [];
								for (var i = 0; i < order.length; i++) {
									var option = $("#' . $param_name . '_select2 option[value="+ order[i]  + "]");
									if (option.length > 0) {
										choices[i] = {id:order[i], text:option[0].label, element: option};
									}
								}
								$("#' . $param_name . '_select2").select2("data", choices);
							}
		
							$("#' . $param_name . '_select2").on("select2-selecting", function(e) {
								var ids = $("#' . $param_name . '").val();
								if (ids != "") {
									ids +=",";
								}
								ids += e.val;
								$("#' . $param_name . '").val(ids);
								console.log($("#' . $param_name . '_select2").select2("data"));
							}).on("select2-removed", function(e) {
								  var ids = $("#' . $param_name . '").val();
								  var arr_ids = ids.split(",");
								  var newIds = "";
								  for(var i = 0 ; i < arr_ids.length; i++) {
									if (arr_ids[i] != e.val){
										if (newIds != "") {
											newIds +=",";
										}
										newIds += arr_ids[i];
									}
								  }
								  $("#' . $param_name . '").val(newIds);
							 });
							 */
						});
						</script>';
						
			return $output;
		}
				 
		// ! Generate param type "wt_multidropdown"
		// --------------------------------------------				 
		function qodux_multidropdown_settings_field($settings, $value) {
			$css_option   = vc_get_dropdown_option($settings, $value);
			$dependency = function_exists('vc_generate_dependencies_attributes') ? vc_generate_dependencies_attributes($settings) : '';
			$param_name   = isset($settings['param_name']) ? $settings['param_name'] : '';
			$type         = isset($settings['type']) ? $settings['type'].'_dropdown_field' : '';	
			
			$output = '';		
			$output .= '<input type="hidden" name="' . $param_name . '" id="' . $param_name . '" class="wpb_vc_param_value ' . $param_name . '" value="' . $value . '"/>';						
			
			$output .= '<select  id="' . $param_name . '_dropdown" name="'
					   . $param_name . '_dropdown'
					   . '" class="wpb_vc_param_value wpb-input wpb-select '
					   . $param_name . '_dropdown'
					   . ' ' . $type
					   . '" data-option="' . $css_option
					   . '" ' . 'multiple'
					   . ' ' . $dependency . '">';				
			
			$current_value = explode(",", $value);
			
			// if exists $settings['target'] then auto fill the options for multidropdown related to target value
			isset($settings['target']) ? $target = 'true' : $target = 'false';
			
			if ( $target == 'false' ) {
				$values = is_array($settings['value']) ? $settings['value'] : array();
			} else {
				$values = WT_VCSC_GetSelectTargetOptions($settings['target']);
			}
			
			if ($values != '' && is_array($values)) {
				foreach ( $values as $text_val => $val ) {
					if ( is_numeric($text_val) && (is_string($val) || is_numeric($val)) ) {
						$text_val = $val;
					}				
					$text_val = htmlspecialchars( $text_val );		
					$selected = is_array($current_value) && in_array($val, $current_value) ? ' selected="selected"' : '';
					
					$output .= '<option value="' . $val . '"'.$selected.'>'.$text_val.'</option>';
				}
			}
			$output .= '</select>';
			
			$output .= '<script type="text/javascript">
						jQuery(document).ready(function($){	
							$("#' . $param_name . '_dropdown").change(function() {							
								var values    = $(this).val(),
									strValues = "";
									
								if (values != null) {
									strValues = values.toString();
								}																								
								$("#' . $param_name . '").val(strValues);
								
								//console.log(values);
								//console.log(strValues);
							});
						});
						</script>';
			return $output; 
		}
		
		// ! Generate param type "qodux_range"
		// --------------------------------------------	
		function qodux_range_settings_field($settings, $value) {
			$dependency = function_exists('vc_generate_dependencies_attributes') ? vc_generate_dependencies_attributes($settings) : '';
			$param_name  = isset($settings['param_name']) ? $settings['param_name'] : '';
			$type        = isset($settings['type']) ? $settings['type'].'_field' : '';
			
			$min         = isset($settings['min']) ? $settings['min'] : '';
			$max         = isset($settings['max']) ? $settings['max'] : '';
			$step        = isset($settings['step']) ? $settings['step'] : '';
			$unit        = isset($settings['unit']) ? $settings['unit'] : '';
			
			$unit_margin = $unit != '' ? '20px' : '10px';
			
			$output      = '';			
			$output     .= '<div class="wt_range_block clearfix">';
			$output 	.= '<input style="width: 60px; float: left; margin-left: 0; margin-right: 5px; text-align: center; padding: 6px;" name="' . $param_name . '"  class="wt-range-serial nouislider-input-selector nouislider-input-composer wpb_vc_param_value ' . $param_name . ' ' . $type . '" type="text" value="' . $value . '"/>';
			$output     .= '<span style="float: left; margin-right: '.$unit_margin.'; margin-top: 8px;" class="unit">' . $unit . '</span>';
			$output 	.= '<div class="wt-range-input-element" data-value="' . $value . '" data-min="' . $min . '" data-max="' . $max . '" data-step="' . $step . '" style="width: 200px; float: left; margin-top: 8px;"></div>';
			$output 	.= '</div>';			
			return $output;
		}	
		
		// ! Generate param type "wt_loadfile"
		// --------------------------------------------	
		function wt_loadfile_settings_field($settings, $value){
			$dependency = function_exists('vc_generate_dependencies_attributes') ? vc_generate_dependencies_attributes($settings) : '';
			$param_name = isset($settings['param_name']) ? $settings['param_name'] : '';
			$type       = isset($settings['type']) ? $settings['type'].'_field' : '';
			
			$file_path  = isset($settings['file_path']) ? $settings['file_path'] : '';
			//$url        = plugin_dir_url( __FILE__ );
			$url        = QODUX_FW_URL . '/shortcodes/assets/';
			
			$output     = '';
			if (!empty($file_path)) {
				$output .= '<script type="text/javascript" src="' . $url.$file_path . '"></script>';
			}
			return $output;
		}
		
		// ! Generate param type "wt_markers"
		// --------------------------------------------	
		function wt_markers_settings_field($settings, $value) {
			$dependency = function_exists('vc_generate_dependencies_attributes') ? vc_generate_dependencies_attributes($settings) : '';
			?>
			<script type="text/javascript">
				jQuery(document).ready(function( $ ) {
		
					// recreate from settings - textarea content
					var textarea_val = $('#vc_ui-panel-edit-element .marker_data').html();
					if(textarea_val != '') {
						// check if value is a valid json
						if (/^[\],:{}\s]*$/.test(textarea_val.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
							var obj = $.parseJSON(textarea_val);
							// loop trough the object
							Object.keys(obj).forEach(function(key) {
								var obj_data = {
									'm_address'   : obj[key].m_address,
									'm_link'      : obj[key].m_link,
									'm_title'     : obj[key].m_title,
									'm_desc'      : obj[key].m_desc,
									'm_image_id'  : obj[key].m_image_id,
									'm_image_url' : obj[key].m_image_url,
									'm_icon_id'   : obj[key].m_icon_id,
									'm_icon_url'  : obj[key].m_icon_url,
									'm_img_text'  : obj[key].m_img_text
								},
								marker = wt_marker_item_panel(obj_data);
								$('.wt_markers_wrapper').append(marker);
							});
						}
					}
				});
			</script>
			<?php
			return  '<div class="wt_markers_wrapper"></div>'
					.'<a href="#" class="new_marker">Add marker</a>';
		}
				
				
		// ! Load Composer Elements + Add Custom Parameters
		// --------------------------------------------	
		function WT_VCSC_RegisterWithComposer() {
			if (function_exists('vc_is_inline')){
				if ((vc_is_inline()) || (is_admin())) {
					$this->WT_VCSC_AddParametersToComposer();
					$this->WT_VCSC_AddElementsToComposer();
				} else {
					$this->WT_VCSC_LoadClassElements();
				}
			} else if (is_admin()) {
				$this->WT_VCSC_AddParametersToComposer();
				$this->WT_VCSC_AddElementsToComposer();
			} else {
				$this->WT_VCSC_LoadClassElements();
			}
		}
		
		// ! Add Extended Custom Parameters
		// --------------------------------------------	
		function WT_VCSC_AddParametersToComposer() {
			if (function_exists('vc_add_shortcode_param')) {
									
				// Generate param type "wt_separator"
				vc_add_shortcode_param('wt_separator',        	array($this, 'wt_separator_settings_field'));
				
				// Generate param type "wt_multidropdown"
				vc_add_shortcode_param('qodux_multidropdown',      array($this, 'qodux_multidropdown_settings_field'));
				
				// Generate param type "wt_multiselect"
				//vc_add_shortcode_param('wt_multiselect',        array($this, 'wt_multiselect_settings_field'));
				
				// Generate param type "qodux_range"
				vc_add_shortcode_param('qodux_range',              array($this, 'qodux_range_settings_field'));
								
				// Generate param type "wt_loadfile"
				vc_add_shortcode_param('wt_loadfile',           array($this, 'wt_loadfile_settings_field'));
				
				// Generate param type "wt_markers"
				vc_add_shortcode_param('wt_gmap_markers',       array($this, 'wt_markers_settings_field'));
			
			}
		}
		
		// ! Load Extended Composer Elements
		// --------------------------------------------
		function WT_VCSC_AddElementsToComposer() {			
			
			$extend_WT_VCSC_Rows = true;
			$extend_WT_VCSC_Cols = true;
			
			// Load Extended Row Settings
			if ( $extend_WT_VCSC_Rows ) {
				require_once(QODUX_FW_PATH . '/shortcodes/elements/wt_vcsc_extend_row.php');
			}
			// Load Extended Column Settings
			if ( $extend_WT_VCSC_Cols ) {
				require_once(QODUX_FW_PATH . '/shortcodes/elements/wt_vcsc_extend_column.php');
			}
			// Load Extended Params for Specific VC Shortcode
			if ( $extend_WT_VCSC_Cols ) {
				require_once(QODUX_FW_PATH . '/shortcodes/elements/wt_vcsc_extend_params.php');
			}
		}
		
		function WT_VCSC_LoadClassElements() {			
			// Load Custom Post Types
		}
		
		// ! Load extended plugin Back-End css and javascript files when Editing
		// --------------------------------------------			
		function WT_VCSC_Extensions_Admin() {
			global $pagenow, $typenow;
			$screen = get_current_screen();
			if (empty($typenow) && !empty($_GET['post'])) {
				$post 		= get_post($_GET['post']);
				$typenow 	= $post->post_type;
			}
			$url = plugin_dir_url( __FILE__ );
			
			if (WT_VCSC_IsEditPagePost()) {
				wp_enqueue_style( 'wt-visual-composer-extensions-admin', QODUX_FW_URL . '/shortcodes/assets/wt-visual-composer-extensions-admin.css', null, false, 'all' );
			  
				// If you need any javascript files on back end, here is how you can load them.				
				
				wp_enqueue_style( 'wt-extend-nouislider',                            QODUX_FW_URL . '/shortcodes/assets/lib/css/admin/jquery.nouislider.css',                null, false, 'all' );
				wp_enqueue_script( 'wt-extend-nouislider',                           QODUX_FW_URL . '/shortcodes/assets/lib/js/admin/jquery.nouislider.min.js',             array('jquery'), null, true );	
			}
		}
		
		// ! Load extended plugin Front-End css and javascript files
		// --------------------------------------------		
		function WT_VCSC_Extensions_Front() {
			global $post;
			$url = plugin_dir_url( __FILE__ );
			
				wp_register_script( 'wt-extend-waypoints',                           QODUX_FW_URL . '/shortcodes/assets/lib/js/jquery.waypoint.js',                     array('jquery'), null, true );
				
				wp_enqueue_style( 'wt-extend-bx-slider',                             QODUX_FW_URL . '/shortcodes/assets/lib/css/jquery.bxslider.css',                    null, false, 'all' );
				wp_register_script( 'wt-extend-bx-slider',                           QODUX_FW_URL . '/shortcodes/assets/lib/js/jquery.bxslider.js',                     array('jquery'), null, true );
				wp_register_script( 'wt-extend-youtube-player',                      QODUX_FW_URL . '/shortcodes/assets/lib/js/jquery.mb.YTPlayer.js',               array('jquery'), null, true );		
				
		}
		
		// ! Adding WhoaThemes classes to standard VC shortcodes
		// --------------------------------------------		
		function WT_VCSC_Extensions_Css_Classes($wt_class, $tag) {
			if ( in_array( $tag, array('vc_accordion', 'vc_toggle', 'vc_pie', 'TS-VCSC-Pricing-Table', 'TS-VCSC-Countdown') ) ) {
				$wt_class .= ' wt_vcsc_style';
			}
			return $wt_class;
		}		
				
		// ! Load Helper Functions
		// --------------------------------------------	
		function WT_VCSC_HelperFunctions() {			
			require_once(QODUX_FW_PATH . '/shortcodes/includes/wt_vcsc_functions.php');
			
			require_once(QODUX_FW_PATH . '/shortcodes/includes/aq-resizer.php');
		}
		
	}
}

if (class_exists('WT_VC_EXTENSIONS')) {
	$WT_VC_EXTENSIONS = new WT_VC_EXTENSIONS;
}

// ! Add WT_VCSC_SHORTCODE Class
// --------------------------------------------	
class WT_VCSC_SHORTCODE {
			
    public function getWTExtraId() {
		$output = '';
		$output = array(
			'type'        => 'textfield',
			'heading'     => esc_html__('Extra Unique ID name', 'besmart'),
			'param_name'  => 'el_id',
			'description' => esc_html__('If you wish to style particular content element differently, then use this field to add a UNIQUE ID name and then refer to it in your css file.', 'besmart'),
			'group'       => esc_html__('Extra settings', 'besmart')
		);		
		return $output;
	}	
	
    public function getWTExtraClass() {
		$output = '';
		$output = array(
			'type'        => 'textfield',
			'heading'     => esc_html__('Extra class name', 'besmart'),
			'param_name'  => 'el_class',
			'description' => esc_html__('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'besmart'),
			'group'       => esc_html__('Extra settings', 'besmart')
		);		
		return $output;
	}
	
    public function getWTAnimations() {
		$output = '';
		$output = array(
			"type"        => "dropdown",
			"heading"     => esc_html__("CSS WT Animation", "besmart"),
			"param_name"  => "css_animation",
			"value" => array( esc_html__("No", "besmart") => '', esc_html__("Hinge", "besmart") => "hinge", esc_html__("Flash", "besmart") => "flash", esc_html__("Shake", "besmart") => "shake", esc_html__("Bounce", "besmart") => "bounce", esc_html__("Tada", "besmart") => "tada", esc_html__("Swing", "besmart") => "swing", esc_html__("Wobble", "besmart") => "wobble", esc_html__("Pulse", "besmart") => "pulse", esc_html__("Flip", "besmart") => "flip", esc_html__("FlipInX", "besmart") => "flipInX", esc_html__("FlipOutX", "besmart") => "flipOutX", esc_html__("FlipInY", "besmart") => "flipInY", esc_html__("FlipOutY", "besmart") => "flipOutY", esc_html__("FadeIn", "besmart") => "fadeIn", esc_html__("FadeInUp", "besmart") => "fadeInUp", esc_html__("FadeInDown", "besmart") => "fadeInDown", esc_html__("FadeInLeft", "besmart") => "fadeInLeft", esc_html__("FadeInRight", "besmart") => "fadeInRight", esc_html__("FadeInUpBig", "besmart") => "fadeInUpBig", esc_html__("FadeInDownBig", "besmart") => "fadeInDownBig", esc_html__("FadeInLeftBig", "besmart") => "fadeInLeftBig", esc_html__("FadeInRightBig", "besmart") => "fadeInRightBig", esc_html__("FadeOut", "besmart") => "fadeOut", esc_html__("FadeOutUp", "besmart") => "fadeOutUp", esc_html__("FadeOutDown", "besmart") => "fadeOutDown", esc_html__("FadeOutLeft", "besmart") => "fadeOutLeft", esc_html__("FadeOutRight", "besmart") => "fadeOutRight", esc_html__("fadeOutUpBig", "besmart") => "fadeOutUpBig", esc_html__("FadeOutDownBig", "besmart") => "fadeOutDownBig", esc_html__("FadeOutLeftBig", "besmart") => "fadeOutLeftBig", esc_html__("FadeOutRightBig", "besmart") => "fadeOutRightBig", esc_html__("BounceIn", "besmart") => "bounceIn", esc_html__("BounceInUp", "besmart") => "bounceInUp", esc_html__("BounceInDown", "besmart") => "bounceInDown", esc_html__("BounceInLeft", "besmart") => "bounceInLeft", esc_html__("BounceInRight", "besmart") => "bounceInRight", esc_html__("BounceOut", "besmart") => "bounceOut", esc_html__("BounceOutUp", "besmart") => "bounceOutUp", esc_html__("BounceOutDown", "besmart") => "bounceOutDown", esc_html__("BounceOutLeft", "besmart") => "bounceOutLeft", esc_html__("BounceOutRight", "besmart") => "bounceOutRight", esc_html__("RotateIn", "besmart") => "rotateIn", esc_html__("RotateInUpLeft", "besmart") => "rotateInUpLeft", esc_html__("RotateInDownLeft", "besmart") => "rotateInDownLeft", esc_html__("RotateInUpRight", "besmart") => "rotateInUpRight", esc_html__("RotateInDownRight", "besmart") => "rotateInDownRight", esc_html__("RotateOut", "besmart") => "rotateOut", esc_html__("RotateOutUpLeft", "besmart") => "rotateOutUpLeft", esc_html__("RotateOutDownLeft", "besmart") => "rotateOutDownLeft", esc_html__("RotateOutUpRight", "besmart") => "rotateOutUpRight", esc_html__("RotateOutDownRight", "besmart") => "rotateOutDownRight", esc_html__("RollIn", "besmart") => "rollIn", esc_html__("RollOut", "besmart") => "rollOut", esc_html__("LightSpeedIn", "besmart") => "lightSpeedIn", esc_html__("LightSpeedOut", "besmart") => "lightSpeedOut" ),
			"description" => esc_html__("Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.", "besmart"),
			'group'       => esc_html__('Extra settings', 'besmart')
		);		
		return $output;
	}
	
    public function getWTAnimationsType() {
		$output = '';
		$output = array(
			"type"        => "dropdown",
			"heading"     => esc_html__("WT Animation Visible Type", "besmart"),
			"param_name"  => "anim_type",
			"value"       => array( esc_html__("Animate when element is visible", "besmart") => 'wt_animate_if_visible', esc_html__("Animate if element is almost visible", "besmart") => "wt_animate_if_almost_visible" ),
			"description" => esc_html__("Select when the type of animation should start for this element.", "besmart"),
			'group'       => esc_html__('Extra settings', 'besmart')
		);		
		return $output;
	}
	
    public function getWTAnimationsDelay() {
		$output = '';
		$output = array(
			"type"        => "textfield",
			"heading"     => esc_html__("WT Animation Delay", "besmart"),
			"param_name"  => "anim_delay",
			"description" => esc_html__("Here you can set a specific delay for the animation (miliseconds). Example: '100', '500', '1000'.", "besmart"),
			'group'       => esc_html__('Extra settings', 'besmart')
		);		
		return $output;
	}

	public function getWTCSSAnimationClass($css_animation,$anim_type) {
		$output = '';
		if ( $css_animation != '' ) {
			wp_enqueue_script( 'waypoints' ); // VC file
			if ($anim_type == false) {
				$output = ' wt_animate' ;
			} else {
				$output = ' wt_animate ' . $anim_type ;
			}
		}
		return $output;
	}
	
	public function getWTCSSAnimationData($css_animation,$anim_delay) {
		$output = '';
		if ( $css_animation != '' ) {
			$output = ' data-animation="'.$css_animation.'"';
			if ( $anim_delay != '' ) {
				$output .= ' data-animation-delay="'.absint( $anim_delay ).'"';
			}
		}
		return $output;
	}
	
	public function getWTElementStyle($el_style) {
		$output = '';
		if ( trim($el_style) != '' ) {
			$output = ' style="'.$el_style.'"';
		}
		return $output;
	}
	
}

/*** WhoaThemes Visual Composer Content elements refresh ***/
class WT_VCSC_SharedLibrary {
	// Here we will store plugin wise (shared) settings. sizes, sizes etc...

	public static $sizes = array(
		'Mini'   => 'xs',
		'Small'  => 'sm',
		'Normal' => 'md',
		'Large'  => 'lg'
	);
	
	public static $text_align = array(
		'Left'    => 'left',
		'Right'   => 'right',
		'Center'  => 'center',
		'Justify' => 'justify'
	);
	
	public static $sep_styles = array(
		'Border' => '',
		'Dashed' => 'dashed',
		'Dotted' => 'dotted',
		'Double' => 'double'
	);
	
	public static $box_styles = array(
		'Default'                => '',
		'Rounded'                => 'vc_box_rounded',
		'Border'                 => 'vc_box_border',
		'Outline'                => 'vc_box_outline',
		'Shadow'                 => 'vc_box_shadow',
		'Bordered shadow'        => 'vc_box_shadow_border',
		'3D Shadow'              => 'vc_box_shadow_3d',
		'Circle'                 => 'vc_box_circle', //new
		'Circle Border'          => 'vc_box_border_circle', //new
		'Circle Outline'         => 'vc_box_outline_circle', //new
		'Circle Shadow'          => 'vc_box_shadow_circle', //new
		'Circle Border Shadow'   => 'vc_box_shadow_border_circle' //new
	);
	
	public static function getSizes() {
		return self::$sizes;
	}
	
	public static function getTextAlign() {
		return self::$text_align;
	}
	
	public static function getSeparatorStyles() {
		return self::$sep_styles;
	}

	public static function getBoxStyles() {
		return self::$box_styles;
	}
}

function WT_VCSC_getShared( $asset = '' ) {
	switch ( $asset ) {
		
		case 'sizes':
			return WT_VCSC_SharedLibrary::getSizes();
			break;
		
		case 'text align':
			return WT_VCSC_SharedLibrary::getTextAlign();
			break;
		
		case 'separator styles':
			return WT_VCSC_SharedLibrary::getSeparatorStyles();
			break;

		case 'single image styles':
			return WT_VCSC_SharedLibrary::getBoxStyles();
			break;

		default:
			# code...
			break;
			
	}
}

?>