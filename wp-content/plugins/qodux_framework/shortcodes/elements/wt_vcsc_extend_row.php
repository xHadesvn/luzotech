<?php
	if (function_exists('vc_add_param')) {
		
		// Row WT_VC Extensions
		vc_add_param("vc_row", array(
			'type'              			=> 'wt_separator',
			'heading'           			=> esc_html__( '', 'besmart' ),
			'param_name'        			=> 'separator',
			'separator'             		=> 'Background Extended Settings',
			'description'       			=> esc_html__( '', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));
		vc_add_param("vc_row", array(
			'type'                          => 'textfield',
			'heading'                       => esc_html__('Extra Unique ID name', 'besmart'),
			'param_name'                    => 'el_id',
			'description'                   => esc_html__('If you wish to style particular content element differently, then use this field to add a UNIQUE ID name and then refer to it in your css file.', 'besmart')
		));
		vc_add_param("vc_row", array(
			'type'                  		=> 'qodux_range',
			'heading'               		=> esc_html__( 'Minimum Height', 'besmart' ),
			'param_name'            		=> 'min_height',
			'value'                 		=> '0',
			'min'                   		=> '0',
			'max'                   		=> '2048',
			'step'                  		=> '1',
			'unit'                  		=> 'px',
			'description'           		=> esc_html__( 'Define the minimum height for this row.', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));
		vc_add_param( "vc_row", array(
			'type'							=> 'checkbox',
			'class'							=> '',
			'heading'						=> esc_html__('Center Row Content?','besmart'),
			'param_name'					=> 'center_row',
			'value'							=> Array( esc_html__('Yes please.', 'besmart') => 'yes'),
			'description'           		=> esc_html__( 'Use this option to horizontally center the inner content of this row. <strong>Useful when using pages based on \'Full Screen\' templates.</strong>', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart')
		));
		vc_add_param( "vc_row", array(
			'type'							=> 'checkbox',
			'class'							=> '',
			'heading'						=> esc_html__('Full Screen Width on Mobiles?','besmart'),
			'param_name'					=> 'full_mobile_row',
			'value'							=> Array( esc_html__('Yes please.', 'besmart') => 'yes'),
			'description'           		=> esc_html__( 'Use this option to keep full width content of this row on mobiles. <strong>Useful when using pages based on \'Full Screen\' templates.</strong>', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart')
		));
		vc_add_param( "vc_row", array(
			'type'							=> 'checkbox',
			'heading'						=> esc_html__('Default Background?','besmart'),
			'param_name'					=> 'default_bg',
			'value'							=> Array( esc_html__('Yes please.', 'besmart') => 'yes'),
			'description'           		=> esc_html__( 'Check this option to add a default background color.', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart')
		));	
		vc_add_param( "vc_row", array(
			'type'							=> 'checkbox',
			'heading'						=> esc_html__('Default Skin Background?','besmart'),
			'param_name'					=> 'default_skin_bg',
			'value'							=> Array( esc_html__('Yes please.', 'besmart') => 'yes'),
			'description'           		=> esc_html__( 'Check this option to add a default skin background color.', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart')
		));	
		vc_add_param( "vc_row", array(
			'type'							=> 'checkbox',
			'heading'						=> esc_html__('Default Border?','besmart'),
			'param_name'					=> 'default_border',
			'value'							=> Array( esc_html__('Yes please.', 'besmart') => 'yes'),
			'description'           		=> esc_html__( 'Check this option to add a default border.', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart')
		));
		vc_add_param( "vc_row", array(
			'type'							=> 'checkbox',
			'heading'						=> esc_html__('Drop Shadow?','besmart'),
			'param_name'					=> 'shadow',
			'value'							=> Array( esc_html__('Yes please.', 'besmart') => 'yes'),
			'description'           		=> esc_html__( 'Check this option to add a default shadow to this row.', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart')
		));
		vc_add_param("vc_row", array(
			'type' 							=> 'dropdown',
			'heading' 						=> esc_html__( 'Typography Style', 'besmart'),
			'param_name' 					=> 'typography',
			'value' 						=> array(
				__( 'Dark Text', 'besmart')		=> 'dark',
				__( 'White Text', 'besmart')	=> 'light'
			),
			'description' 					=> esc_html__('Select typography style.', 'besmart'),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));
		vc_add_param("vc_row", array(
			'type'                          => 'colorpicker',
			'heading'                       => esc_html__('Background Color', 'besmart'),
			'param_name'                    => 'bck_color',
			'description'                   => esc_html__( 'Select background color for this row.', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart')
		));	
		vc_add_param("vc_row", array(
			'type' 							=> 'dropdown',
			'heading' 						=> esc_html__( 'Background Type', 'besmart'),
			'param_name' 					=> 'bg_type',
			'value' 						=> array(
				__( 'None', 'besmart')					=> '',
				__( 'Simple Image', 'besmart')			=> 'image',
				__( 'Fixed Image', 'besmart')			=> 'fixed',
				__( 'Parallax Image', 'besmart')		=> 'parallax',
				__( 'YouTube Video', 'besmart')			=> 'youtube',
				//__( 'Self Hosted Video', 'besmart')		=> 'video',
			),
			'admin_label' 					=> true,
			'description' 					=> esc_html__('Select background type for this row.', 'besmart'),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));
		vc_add_param("vc_row", array(
			'type'							=> 'attach_image',
			'heading'						=> esc_html__( 'Background Image', 'besmart' ),
			'param_name'					=> 'bck_image',
			'value'							=> '',
			'description'					=> esc_html__( 'Select the background image for your row.', 'besmart' ),
			'dependency' 					=> array(
				'element' 	=> 'bg_type',
				'value' 	=> array('image', 'fixed', 'parallax')
			),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));
		vc_add_param("vc_row", array(
			'type'                  		=> 'dropdown',
			'heading'               		=> esc_html__( 'Background Image Size', 'besmart' ),
			'param_name'            		=> 'bg_size',
			'value'                 		=> array(
				__( 'Full Size Image', 'besmart' )			=> 'full',
				__( 'Large Size Image', 'besmart' )			=> 'large',
				__( 'Medium Size Image', 'besmart' )		=> 'medium',
				__( 'Thumbnail Size Image', 'besmart' )		=> 'thumbnail',
			),
			'description'           		=> esc_html__( 'Select which image size based on WordPress settings should be used.', 'besmart' ),
			'dependency' 					=> array(
				'element' 	=> 'bg_type',
				'value' 	=> array('image', 'fixed', 'parallax')
			),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));
		vc_add_param("vc_row", array(
			'type' 							=> 'dropdown',
			'heading' 						=> esc_html__( 'Background Position', 'besmart' ),
			'param_name' 					=> 'bg_position',
			'value' 						=> array(
				__( 'Top', 'besmart' )			=> 'top',
				__( 'Middle', 'besmart' ) 		=> 'center',
				__( 'Bottom', 'besmart' ) 		=> 'bottom'
			),
			'dependency' 					=> array(
				'element' 	=> 'bg_type',
				'value' 	=> array('image', 'fixed', 'parallax')
			),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' ),
		));
		vc_add_param("vc_row", array(
			'type' 							=> 'dropdown',
			'heading' 						=> esc_html__( 'Background Size', 'besmart' ),
			'param_name' 					=> 'bg_size_standard',
			'value' 						=> array(
				__( 'Cover', 'besmart' ) 		=> 'cover',
				__( 'Contain', 'besmart' ) 		=> 'contain',
				__( 'Initial', 'besmart' ) 		=> 'initial',
				__( 'Auto', 'besmart' ) 		=> 'auto',
			),
			'dependency' 					=> array(
				'element' 	=> 'bg_type',
				'value' 	=> array('image', 'fixed', 'parallax')
			),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));
		vc_add_param("vc_row", array(
			'type' 							=> 'dropdown',
			'heading' 						=> esc_html__( 'Background Repeat', 'besmart' ),
			'param_name' 					=> 'bg_repeat',
			'value' 						=> array(
				__( 'No Repeat', 'besmart' )	=> 'no-repeat',
				__( 'Repeat X + Y', 'besmart' )	=> 'repeat',
				__( 'Repeat X', 'besmart' )		=> 'repeat-x',
				__( 'Repeat Y', 'besmart' )		=> 'repeat-y'
			),
			'dependency' 					=> array(
				'element' 	=> 'bg_type',
				'value' 	=> array('image', 'fixed', 'parallax')
			),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));
		
		// YouTube Video Background
		vc_add_param("vc_row", array(
			'type'              			=> 'textfield',
			'heading'           			=> esc_html__( 'YouTube Video ID', 'besmart' ),
			'param_name'        			=> 'youtube_video_id',
			'value'             			=> '',
			'description'       			=> esc_html__( 'Enter the YouTube video ID.', 'besmart' ),
			'dependency' 					=> array(
				'element' 	=> 'bg_type',
				'value' 	=> 'youtube'
			),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));	
		vc_add_param("vc_row", array(
			'type'                          => 'colorpicker',
			'heading'                       => esc_html__('Background Overlay Color', 'besmart'),
			'param_name'                    => 'bg_color_overlay',
			'description'                   => esc_html__( 'Select overlay color for this element.', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart')
		));	
		vc_add_param("vc_row", array(
			'type'                  		=> 'dropdown',
			'heading'               		=> esc_html__( 'Background Overlay Pattern', 'besmart' ),
			'param_name'            		=> 'bg_pattern_overlay',
			'value'                 		=> array(
				__( 'None', 'besmart' )			=> '',
				__( 'Dotted', 'besmart' )		=> 'dotted',
				__( 'Dashed', 'besmart' )		=> 'dashed',
			),
			'description'           		=> esc_html__( 'Select overlay pattern type for this element.', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));
		vc_add_param("vc_row", array(
			'type'							=> 'colorpicker',
			'class'							=> '',
			'heading'						=> esc_html__('Border Color','besmart'),
			'param_name'					=> 'border_color',
			'value' 						=> '',
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart')
		));
		
		vc_add_param("vc_row", array(
			'type'							=> 'dropdown',
			'class'							=> '',
			'heading'						=> esc_html__('Border Style','besmart'),
			'param_name'					=> 'border_style',
			'value'							=> array(
				__('Solid', 'besmart')	=> 'solid',
				__('Dotted', 'besmart')	=> 'dotted',
				__('Dashed', 'besmart')	=> 'dashed',
			),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart')
		));
		
		vc_add_param("vc_row", array(
			'type'							=> 'textfield',
			'class'							=> '',
			'heading'						=> esc_html__('Border Width','besmart'),
			'param_name'					=> 'border_width',
			'value'							=> '0px 0px 0px 0px',
			'description'					=> esc_html__('Your border width in pixels. Example: <strong>1px 1px 1px 1px</strong> (top, right, bottom, left).', 'besmart'),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart')
		));	
		
		// Paddings & Margins
		vc_add_param("vc_row", array(
			'type'              			=> 'wt_separator',
			'heading'           			=> esc_html__( '', 'besmart' ),
			'param_name'        			=> 'separator_2',
			'separator'             		=> 'Paddings and Margins',
			'description'       			=> esc_html__( '', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));
		vc_add_param("vc_row", array(
			'type'                  		=> 'qodux_range',
			'heading'               		=> esc_html__( 'Padding: Top', 'besmart' ),
			'param_name'            		=> 'padding_top',
			'value'                 		=> '0',
			'min'                   		=> '0',
			'max'                   		=> '250',
			'step'                  		=> '1',
			'unit'                  		=> 'px',
			'description'           		=> esc_html__( '', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));
		vc_add_param("vc_row", array(
			'type'                  		=> 'qodux_range',
			'heading'               		=> esc_html__( 'Padding: Bottom', 'besmart' ),
			'param_name'            		=> 'padding_bottom',
			'value'                 		=> '0',
			'min'                   		=> '0',
			'max'                   		=> '250',
			'step'                  		=> '1',
			'unit'                  		=> 'px',
			'description'           		=> esc_html__( '', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));
		vc_add_param("vc_row", array(
			'type'                  		=> 'qodux_range',
			'heading'               		=> esc_html__( 'Padding: Left', 'besmart' ),
			'param_name'            		=> 'padding_left',
			'value'                 		=> '0',
			'min'                   		=> '0',
			'max'                   		=> '250',
			'step'                  		=> '1',
			'unit'                  		=> 'px',
			'description'           		=> esc_html__( '', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));
		vc_add_param("vc_row", array(
			'type'                  		=> 'qodux_range',
			'heading'               		=> esc_html__( 'Padding: Right', 'besmart' ),
			'param_name'            		=> 'padding_right',
			'value'                 		=> '0',
			'min'                   		=> '0',
			'max'                   		=> '250',
			'step'                  		=> '1',
			'unit'                  		=> 'px',
			'description'           		=> esc_html__( '', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));
		vc_add_param("vc_row", array(
			'type'                  		=> 'qodux_range',
			'heading'               		=> esc_html__( 'Margin: Top', 'besmart' ),
			'param_name'            		=> 'margin_top',
			'value'                 		=> '0',
			'min'                   		=> '-250',
			'max'                   		=> '250',
			'step'                  		=> '1',
			'unit'                  		=> 'px',
			'description'           		=> esc_html__( '', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));
		vc_add_param("vc_row", array(
			'type'                  		=> 'qodux_range',
			'heading'               		=> esc_html__( 'Margin: Bottom', 'besmart' ),
			'param_name'            		=> 'margin_bottom',
			'value'                 		=> '0',
			'min'                   		=> '-250',
			'max'                   		=> '250',
			'step'                  		=> '1',
			'unit'                  		=> 'px',
			'description'           		=> esc_html__( '', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));
		
		// Animations	
		vc_add_param("vc_row", array(
			'type'              			=> 'wt_separator',
			'heading'           			=> esc_html__( '', 'besmart' ),
			'param_name'        			=> 'separator_3',
			'separator'             		=> 'Animations',
			'description'       			=> esc_html__( '', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));	
		vc_add_param("vc_row", array(
			"type"                          => "dropdown",
			"heading"                       => esc_html__("CSS WT Animation", "besmart"),
			"param_name"                    => "css_animation",
			"value" => array( esc_html__("No", "besmart") => '', esc_html__("Hinge", "besmart") => "hinge", esc_html__("Flash", "besmart") => "flash", esc_html__("Shake", "besmart") => "shake", esc_html__("Bounce", "besmart") => "bounce", esc_html__("Tada", "besmart") => "tada", esc_html__("Swing", "besmart") => "swing", esc_html__("Wobble", "besmart") => "wobble", esc_html__("Pulse", "besmart") => "pulse", esc_html__("Flip", "besmart") => "flip", esc_html__("FlipInX", "besmart") => "flipInX", esc_html__("FlipOutX", "besmart") => "flipOutX", esc_html__("FlipInY", "besmart") => "flipInY", esc_html__("FlipOutY", "besmart") => "flipOutY", esc_html__("FadeIn", "besmart") => "fadeIn", esc_html__("FadeInUp", "besmart") => "fadeInUp", esc_html__("FadeInDown", "besmart") => "fadeInDown", esc_html__("FadeInLeft", "besmart") => "fadeInLeft", esc_html__("FadeInRight", "besmart") => "fadeInRight", esc_html__("FadeInUpBig", "besmart") => "fadeInUpBig", esc_html__("FadeInDownBig", "besmart") => "fadeInDownBig", esc_html__("FadeInLeftBig", "besmart") => "fadeInLeftBig", esc_html__("FadeInRightBig", "besmart") => "fadeInRightBig", esc_html__("FadeOut", "besmart") => "fadeOut", esc_html__("FadeOutUp", "besmart") => "fadeOutUp", esc_html__("FadeOutDown", "besmart") => "fadeOutDown", esc_html__("FadeOutLeft", "besmart") => "fadeOutLeft", esc_html__("FadeOutRight", "besmart") => "fadeOutRight", esc_html__("fadeOutUpBig", "besmart") => "fadeOutUpBig", esc_html__("FadeOutDownBig", "besmart") => "fadeOutDownBig", esc_html__("FadeOutLeftBig", "besmart") => "fadeOutLeftBig", esc_html__("FadeOutRightBig", "besmart") => "fadeOutRightBig", esc_html__("BounceIn", "besmart") => "bounceIn", esc_html__("BounceInUp", "besmart") => "bounceInUp", esc_html__("BounceInDown", "besmart") => "bounceInDown", esc_html__("BounceInLeft", "besmart") => "bounceInLeft", esc_html__("BounceInRight", "besmart") => "bounceInRight", esc_html__("BounceOut", "besmart") => "bounceOut", esc_html__("BounceOutUp", "besmart") => "bounceOutUp", esc_html__("BounceOutDown", "besmart") => "bounceOutDown", esc_html__("BounceOutLeft", "besmart") => "bounceOutLeft", esc_html__("BounceOutRight", "besmart") => "bounceOutRight", esc_html__("RotateIn", "besmart") => "rotateIn", esc_html__("RotateInUpLeft", "besmart") => "rotateInUpLeft", esc_html__("RotateInDownLeft", "besmart") => "rotateInDownLeft", esc_html__("RotateInUpRight", "besmart") => "rotateInUpRight", esc_html__("RotateInDownRight", "besmart") => "rotateInDownRight", esc_html__("RotateOut", "besmart") => "rotateOut", esc_html__("RotateOutUpLeft", "besmart") => "rotateOutUpLeft", esc_html__("RotateOutDownLeft", "besmart") => "rotateOutDownLeft", esc_html__("RotateOutUpRight", "besmart") => "rotateOutUpRight", esc_html__("RotateOutDownRight", "besmart") => "rotateOutDownRight", esc_html__("RollIn", "besmart") => "rollIn", esc_html__("RollOut", "besmart") => "rollOut", esc_html__("LightSpeedIn", "besmart") => "lightSpeedIn", esc_html__("LightSpeedOut", "besmart") => "lightSpeedOut" ),
			'description' => esc_html__('Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'besmart'),
			'group' 	                    => esc_html__( 'WT_VC Extensions', 'besmart'),
		));
		vc_add_param("vc_row", array(
			"type"                          => "dropdown",
			"heading"                       => esc_html__("WT Animation Visible Type", "besmart"),
			"param_name"                    => "anim_type",
			"value"                         => array( esc_html__("Animate when element is visible", "besmart") => 'wt_animate_if_visible', esc_html__("Animate if element is almost visible", "besmart") => "wt_animate_if_almost_visible" ),
			"description"                   => esc_html__("Select when the type of animation should start for this element.", "besmart"),
			'group'                         => esc_html__('WT_VC Extensions', 'besmart')
		));		
		vc_add_param("vc_row", array(
			"type"                          => "textfield",
			"heading"                       => esc_html__("WT Animation Delay", "besmart"),
			"param_name"                    => "anim_delay",
			"description"                   => esc_html__("Here you can set a specific delay for the animation (miliseconds). Example: '100', '500', '1000'.", "besmart"),
			'group'                         => esc_html__('WT_VC Extensions', 'besmart')
		));	
		
		vc_add_param("vc_row", array(
			'type'                  		=> 'wt_loadfile',
			'heading'               		=> esc_html__( '', 'besmart' ),
			'param_name'            		=> 'el_file',
			'value'                 		=> '',
			'file_type'             		=> 'js',
			'file_path'             		=> 'wt-visual-composer-extend-element.min.js',
			'param_holder_class'            => 'wt_loadfile_field',
			'description'           		=> esc_html__( '', 'besmart' ),
			'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart'),
		));
	}
?>