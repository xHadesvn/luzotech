<?php

// File Security Check
if (!defined('ABSPATH')) die('-1');

/*
Register WhoaThemes shortcode.
*/

class WPBakeryShortCode_WT_social_networks extends WPBakeryShortCode {
	
	private $wt_sc;
	
	public function __construct($settings) {
        parent::__construct($settings);
		$this->wt_sc = new WT_VCSC_SHORTCODE;
	}
				
	protected function content($atts, $content = null) {
		
		// Get and extract shortcode attributes
		$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
		extract( $atts );
		
		$sc_class = 'wt_social_networks_sc';	
					
		$id = mt_rand(9999, 99999);
		if (trim($el_id) != false) {
			$el_id = esc_attr( trim($el_id) );
		} else {
			$el_id = $sc_class . '-' . $id;
		}		
		
		$icon_margin = (int)$icon_margin;	
				
		if ( $icon_margin != '' || $icon_margin == 0 ) {
			$icon_margin = ' style="margin: ' . $icon_margin . 'px;"';
		} else {
			$icon_margin = ''; 
		}
		
		$el_style = '';	
		
		if ( $icon_color != '' ) {
			$icon_color = 'color: ' . $icon_color . ';';
		}
						
		if ((empty($icon_background)) || ($icon_style == 'simple')) {
			$icon_background = '';
		} else {
			$icon_background = 'background: ' . $icon_background . ';';
		}
			
		if ( $icon_background != '' || $icon_color != '' ) {
			$el_style = ' style="'. $icon_color . $icon_background .'"';
		}
				
		$el_class = esc_attr( $this->getExtraClass($el_class) );
		$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $sc_class.$el_class.vc_shortcode_custom_css_class($css, ' '), $this->settings['base']);
		$css_class .= ' wt_align_'.$icon_align;		
		$css_class .= $this->wt_sc->getWTCSSAnimationClass($css_animation,$anim_type);
		$anim_data = $this->wt_sc->getWTCSSAnimationData($css_animation,$anim_delay);
								
		if($social_networks != ''){
			 
			$soc_output = '';	
			$icon_name  = '';
			$social_networks = array_map( 'trim', explode( ',', $social_networks ) );	
					
			if(is_array($social_networks) && !empty($social_networks)){
				$soc_output .= "\n\t\t\t" . '<ul class="wt_icon_'.$icon_size.' ' . $icon_type . ' ' . $icon_style . '">'; 
				
					foreach ( $social_networks as $index=>$icon ) {
						$icon_link = $icon.'_link';
						$icon_name = $icon;
						
						switch( $icon ) {
							case 'website'     : $icon_output = '<i class="entypo-link"></i>';      break;
							case 'email'       : $icon_output = '<i class="fa-envelope"></i>';      break;
							case 'facebook'    : $icon_output = '<i class="fa-'.$icon.'"></i>';     break;
							case 'twitter'     : $icon_output = '<i class="fa-'.$icon.'"></i>';     break;
							case 'pinterest'   : $icon_output = '<i class="fa-'.$icon.'"></i>';     break;
							case 'linkedin'    : $icon_output = '<i class="fa-'.$icon.'"></i>';     break;
							case 'google'      : $icon_output = '<i class="entypo-gplus"></i>';     break;
							case 'dribbble'    : $icon_output = '<i class="entypo-'.$icon.'"></i>'; break;
							case 'youtube'     : $icon_output = '<i class="fa-'.$icon.'"></i>';     break;
							case 'vimeo'       : $icon_output = '<i class="entypo-'.$icon.'"></i>'; break;
							case 'rss'         : $icon_output = '<i class="fa-'.$icon.'"></i>';     break;
							case 'github'      : $icon_output = '<i class="entypo-'.$icon.'"></i>'; break;
							case 'delicious'   : $icon_output = '<i class="fa-'.$icon.'"></i>';     break;
							case 'flickr'      : $icon_output = '<i class="entypo-'.$icon.'"></i>'; break;
							case 'lastfm'      : $icon_output = '<i class="entypo-'.$icon.'"></i>'; break;
							case 'tumblr'      : $icon_output = '<i class="entypo-'.$icon.'"></i>'; break;
							case 'deviantart'  : $icon_output = '<i class="fa-'.$icon.'"></i>';     break;
							case 'skype'       : $icon_output = '<i class="entypo-'.$icon.'"></i>'; break;
							case 'instagram'   : $icon_output = '<i class="entypo-'.$icon.'"></i>'; break;
							case 'stumbleupon' : $icon_output = '<i class="entypo-'.$icon.'"></i>'; break;
							case 'behance'     : $icon_output = '<i class="entypo-'.$icon.'"></i>'; break;
							case 'soundcloud'  : $icon_output = '<i class="entypo-'.$icon.'"></i>'; break;
						}
						
						if ($tooltip == false) {
							$tooltip = '';
						} else {
							$tooltip_placement == '' ? $tooltip_placement = 'top' : '';
							$tooltip = ' data-toggle="tooltip" data-placement="'.$tooltip_placement.'"';
						}
						
						$soc_output .= "\n\t\t\t\t" . '<li' . $icon_margin . '>';
							if ($$icon_link == false) { // if there is not set the social link, put '#' as a placeholder
									$soc_output .= "\n\t\t\t\t\t" . '<a'.$el_style.' href="#" class="'.$icon_name.'" title="'.$icon.'" rel="nofollow" target="_blank"'.$tooltip.'>'.$icon_output.'</a>'; 
							} else {
									$soc_output .= "\n\t\t\t\t\t" . '<a'.$el_style.' href="'.esc_url( $$icon_link ).'" class="'.$icon_name.'" title="'.$icon.'" rel="nofollow" target="_blank"'.$tooltip.'>'.$icon_output.'</a>'; 
							}
						$soc_output .= "\n\t\t\t\t" . '</li>';												
					}
				
				$soc_output .= "\n\t\t\t" . '</ul>'; 
			}
		}
		
		$output = '<div id="'.$el_id.'" class="'.$css_class.'"'.$anim_data.'>';
	        $output .= "\n\t" . $soc_output;
        $output .= '</div>';
		
        return $output;
    }
	
}
	
/*
Register WhoaThemes shortcode within Visual Composer interface.
*/

if (function_exists('vc_map')) {

	$add_wt_sc_func             = new WT_VCSC_SHORTCODE;
	$add_wt_extra_id            = $add_wt_sc_func->getWTExtraId();
	$add_wt_extra_class         = $add_wt_sc_func->getWTExtraClass();
	$add_wt_css_animation       = $add_wt_sc_func->getWTAnimations();
	$add_wt_css_animation_type  = $add_wt_sc_func->getWTAnimationsType();
	$add_wt_css_animation_delay = $add_wt_sc_func->getWTAnimationsDelay();
	
	vc_map( array(
		'name'          => esc_html__( 'WT Social Networks', 'besmart' ),
		'base'          => 'wt_social_networks',
		'icon'          => 'wt_vc_ico_social_networks',
		'class'         => 'wt_vc_sc_social_networks',
		'category'      => esc_html__( 'by WhoaThemes', 'besmart' ),
		'description'   => esc_html__( 'Place social networks links', 'besmart' ),
		'params'        => array(
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Icon type', 'besmart' ),
				'param_name'    => 'icon_type',
				'value' => array( 
					esc_html__( 'Type #1', 'besmart' )   => 'wt_icon_type_1',
					esc_html__( 'Type #2', 'besmart' )   => 'wt_icon_type_2', 
					esc_html__( 'Type #3', 'besmart' )   => 'wt_icon_type_3',
					esc_html__( 'Type #4', 'besmart' )   => 'wt_icon_type_4',
				),
				'description'   => esc_html__( 'Select social networks type.', 'besmart' )
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Icon style', 'besmart' ),
				'param_name'    => 'icon_style',
				'value' => array( 
					esc_html__( 'Simple', 'besmart' )    => 'wt_simple',
					esc_html__( 'Square', 'besmart' )    => 'wt_square', 
					esc_html__( 'Rounded', 'besmart' )   => 'wt_rounded',
					esc_html__( 'Circle', 'besmart' )    => 'wt_circle',
				),
				'description'   => esc_html__( 'Select social networks style.', 'besmart' )
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Icon background', 'besmart' ),
				'param_name'    => 'icon_background',
				'description'   => esc_html__( 'Select social networks background.', 'besmart' )
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Icon color', 'besmart' ),
				'param_name'    => 'icon_color',
				'description'   => esc_html__( 'Select social networks text color.', 'besmart' )
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Icons alignment', 'besmart' ),
				'param_name'    => 'icon_align',
				'value'         => array( esc_html__( 'Align left', 'besmart' ) => 'left', esc_html__( 'Align right', 'besmart' ) => 'right', esc_html__( 'Align center', 'besmart' ) => 'center'),
				'std'           => 'center',
				'description'   => esc_html__( 'Select icons alignment.', 'besmart' )
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Icon margin', 'besmart' ),
				'param_name'    => 'icon_margin',
				'value'         => '5',
				'description'   => esc_html__( 'Select icons margin. (in pixels)', 'besmart' )
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Icon size', 'besmart' ),
				'param_name'    => 'icon_size',
				'value' => array( 
					'26' => '26',
					'32' => '32', 
					'38' => '38',
					'40' => '40',
					'42' => '42',
					'44' => '44',
					'50' => '50',
				),
				'std'           => '32',
				'description'   => esc_html__( 'Select social networks size.', 'besmart' )
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Show tooltip title?', 'besmart' ),
				'param_name'    => 'tooltip',
				'value'         => array( esc_html__( 'Yes, please', 'besmart' ) => 'true' ),
				'description'   => esc_html__( 'If YES, it shows a tooltip with the social link information.', 'besmart' )
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Tooltip placement', 'besmart' ),
				'param_name'    => 'tooltip_placement',					
				'value'         => array( esc_html__( 'Top', 'besmart' ) => '', esc_html__( 'Bottom', 'besmart' ) => 'bottom', esc_html__( 'Left', 'besmart' ) => 'left', esc_html__( 'Right', 'besmart' ) => 'right'),
				'std'           => 'top',
				'dependency'    => array(
					'element'   => 'tooltip',
					'not_empty'  => true,
				),
				'description'   => esc_html__( 'Select tooltip placement.', 'besmart' )
			),
			array(
				'type'          => 'qodux_multidropdown',
				'heading'       => esc_html__( 'Social networks', 'besmart' ),
				'admin_label'   => true,
				'param_name'    => 'social_networks',
				'value'         => array(
					esc_html__( 'Website', 'besmart' )     => 'website',
					esc_html__( 'Email', 'besmart' )       => 'email', 
					esc_html__( 'Facebook', 'besmart' )    => 'facebook', 
					esc_html__( 'Twitter', 'besmart' )     => 'twitter',
					esc_html__( 'Pinterest', 'besmart' )   => 'pinterest', 
					esc_html__( 'LinkedIn', 'besmart' )    => 'linkedin', 
					esc_html__( 'Google +', 'besmart' )    => 'google',  
					esc_html__( 'Dribbble', 'besmart' )    => 'dribbble',   
					esc_html__( 'YouTube', 'besmart' )     => 'youtube',   
					esc_html__( 'Vimeo', 'besmart' )       => 'vimeo',   
					esc_html__( 'Rss', 'besmart' )         => 'rss', 
					esc_html__( 'Github', 'besmart' )      => 'github',
					esc_html__( 'Delicious', 'besmart' )   => 'delicious',
					esc_html__( 'Flickr', 'besmart' )      => 'flickr',
					esc_html__( 'Lastfm', 'besmart' )      => 'lastfm',
					esc_html__( 'Tumblr', 'besmart' )      => 'tumblr',
					esc_html__( 'Deviantart', 'besmart' )  => 'deviantart',
					esc_html__( 'Skype', 'besmart' )       => 'skype',
					esc_html__( 'Instagram', 'besmart' )   => 'instagram',
					esc_html__( 'StumbleUpon', 'besmart' ) => 'stumbleupon',
					esc_html__( 'Behance', 'besmart' )     => 'behance',
					esc_html__( 'SoundCloud', 'besmart' )  => 'soundcloud'
				),
				'description'   => esc_html__( 'Select custom social media icons and set the links from ', 'besmart' ) .'<strong>' . esc_html__( 'Profiles. ', 'besmart' ) . '</strong>' . esc_html__('Hold the \'Ctrl\' or \'Shift\' keys while clicking to select multiple items', 'besmart')
			),	
			array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'Website Link', 'besmart' ),
					'param_name'         => 'website_link',
					'description'        => esc_html__( 'Set website link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),		
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'Email Link', 'besmart' ),
					'param_name'         => 'email_link',
					'description'        => esc_html__( 'Set email link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'Facebook Link', 'besmart' ),
					'param_name'         => 'facebook_link',
					'description'        => esc_html__( 'Set facebook link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),		
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'Twitter Link', 'besmart' ),
					'param_name'         => 'twitter_link',
					'description'        => esc_html__( 'Set twitter link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),	
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'Pinterest Link', 'besmart' ),
					'param_name'         => 'pinterest_link',
					'description'        => esc_html__( 'Set pinterest link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'LinkedIn Link', 'besmart' ),
					'param_name'         => 'linkedin_link',
					'description'        => esc_html__( 'Set linkedin link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),	
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'Google + Link', 'besmart' ),
					'param_name'         => 'google_link',
					'description'        => esc_html__( 'Set google + link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),	
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'Dribbble Link', 'besmart' ),
					'param_name'         => 'dribbble_link',
					'description'        => esc_html__( 'Set dribbble link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),	
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'YouTube Link', 'besmart' ),
					'param_name'         => 'youtube_link',
					'description'        => esc_html__( 'Set youtube link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),	
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'Vimeo Link', 'besmart' ),
					'param_name'         => 'vimeo_link',
					'description'        => esc_html__( 'Set vimeo link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),	
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'Rss Link', 'besmart' ),
					'param_name'         => 'rss_link',
					'description'        => esc_html__( 'Set rss link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),	
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'Github Link', 'besmart' ),
					'param_name'         => 'github_link',
					'description'        => esc_html__( 'Set github link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),	
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'Delicious Link', 'besmart' ),
					'param_name'         => 'delicious_link',
					'description'        => esc_html__( 'Set delicious link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),	
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'Flickr Link', 'besmart' ),
					'param_name'         => 'flickr_link',
					'description'        => esc_html__( 'Set flickr link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'Lastfm Link', 'besmart' ),
					'param_name'         => 'lastfm_link',
					'description'        => esc_html__( 'Set lastfm link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),	
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'Tumblr Link', 'besmart' ),
					'param_name'         => 'tumblr_link',
					'description'        => esc_html__( 'Set tumblr link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),	
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'Deviantart Link', 'besmart' ),
					'param_name'         => 'deviantart_link',
					'description'        => esc_html__( 'Set deviantart link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),	
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'Skype Link', 'besmart' ),
					'param_name'         => 'skype_link',
					'description'        => esc_html__( 'Set skype link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'Instagram Link', 'besmart' ),
					'param_name'         => 'instagram_link',
					'description'        => esc_html__( 'Set instagram link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),	
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'StumbleUpon Link', 'besmart' ),
					'param_name'         => 'stumbleupon_link',
					'description'        => esc_html__( 'Set stumbleupon link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),	
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'Behance Link', 'besmart' ),
					'param_name'         => 'behance_link',
					'description'        => esc_html__( 'Set behance link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),	
				array(
					'type'               => 'textfield',
					'heading'            => esc_html__( 'SoundCloud Link', 'besmart' ),
					'param_name'         => 'soundcloud_link',
					'description'        => esc_html__( 'Set soundcloud link.', 'besmart' ),
					'group'         	 => esc_html__( 'Profiles', 'besmart' )
				),	
			
			$add_wt_extra_id,
			$add_wt_extra_class,
			$add_wt_css_animation,
			$add_wt_css_animation_type,
			$add_wt_css_animation_delay,
			
			array(
				'type'          => 'css_editor',
				'heading'       => esc_html__( 'Css', 'besmart' ),
				'param_name'    => 'css',
				'group'         => esc_html__( 'Design options', 'besmart' )
			)
		)
	));	
	
}