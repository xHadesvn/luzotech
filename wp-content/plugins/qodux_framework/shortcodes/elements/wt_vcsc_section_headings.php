<?php

// File Security Check
if (!defined('ABSPATH')) die('-1');

/*
Register WhoaThemes shortcode.
*/

class WPBakeryShortCode_WT_section_headings extends WPBakeryShortCode {
	
	private $wt_sc;
	
	public function __construct($settings) {
        parent::__construct($settings);
		$this->wt_sc = new WT_VCSC_SHORTCODE;
	}
			
	protected function content($atts, $content = null) {
		
		// Get and extract shortcode attributes
		$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
		extract( $atts );	
		
		$sc_class   = 'wt_section_heading intro_text';
									
		$el_class   = esc_attr( $this->getExtraClass($el_class) );		
		$css_class  = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $sc_class.$el_class.vc_shortcode_custom_css_class($css, ' '), $this->settings['base']);		
		$css_class .= $this->wt_sc->getWTCSSAnimationClass($css_animation,$anim_type);
		$anim_data  = $this->wt_sc->getWTCSSAnimationData($css_animation,$anim_delay);
		
		//if ($item_content != '') {
			$item_content = rawurldecode(base64_decode(strip_tags($item_content)));
		//}			
		//$content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content
		
		$output = '<div class="'.$css_class.'"'.$anim_data.'>';
	        $output .= "\n\t".do_shortcode($item_content);
        $output .= '</div>';
		
        return $output;
    }
	
}

/*
Register WhoaThemes shortcode within Visual Composer interface.
*/

if (function_exists('vc_map')) {

	$add_wt_sc_func             = new WT_VCSC_SHORTCODE;
	$add_wt_extra_class         = $add_wt_sc_func->getWTExtraClass();
	$add_wt_css_animation       = $add_wt_sc_func->getWTAnimations();
	$add_wt_css_animation_type  = $add_wt_sc_func->getWTAnimationsType();
	$add_wt_css_animation_delay = $add_wt_sc_func->getWTAnimationsDelay();
	
	vc_map( array(
		'name'          => esc_html__('WT Section Headings', 'wt_vcsc'),
		'base'          => 'wt_section_headings',
		'icon'          => 'wt_vc_ico_section_headings',
		'class'         => 'wt_vc_sc_section_headings',
		'wrapper_class' => 'clearfix',		
		'category'      => esc_html__('by WhoaThemes', 'wt_vcsc'),
		'description'   => esc_html__('Add headings for custom sections', 'wt_vcsc'),
		'params'        => array(	
			array(
				'type' => 'textarea_raw_html',
				'holder' => 'div',
				'heading' => esc_html__( 'Section Headings', 'wt_vcsc' ),
				'param_name' => 'item_content',
				'value' => base64_encode( '<h2>I an H2 text block</h2>
<div class="separator_wrapper"><i class="icon icon-star-two red"></i> </div>
<h3 class="h2">There are many variations of passages of Lorem Ipsum available, but the majority
have suffered alteration, by injected humour, or new randomised words.</h3>' ),
				'description' => __( 'Enter your HTML content for custom title.', 'js_composer' ),
			),
			
			$add_wt_extra_class,
			$add_wt_css_animation,
			$add_wt_css_animation_type,
			$add_wt_css_animation_delay,
			
			array(
				'type'          => 'css_editor',
				'heading'       => esc_html__('Css', 'wt_vcsc'),
				'param_name'    => 'css',
				'group'         => esc_html__('Design options', 'wt_vcsc')
			)
		)
	));
	
}