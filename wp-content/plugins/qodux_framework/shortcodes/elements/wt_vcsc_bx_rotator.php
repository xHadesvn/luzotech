<?php

// File Security Check
if (!defined('ABSPATH')) die('-1');

/*
Register WhoaThemes shortcode.
*/

class WPBakeryShortCode_WT_bx_rotator extends WPBakeryShortCode {
	
	private $wt_sc;
	
	public function __construct($settings) {
        parent::__construct($settings);
		$this->wt_sc = new WT_VCSC_SHORTCODE;
	}
						
	protected function content($atts, $content = null) {
		
		extract( shortcode_atts( array(
			'mode'           => 'horizontal',
			'effect'         => 'ease-in-out',
			"speed"          => '500',
			"pause"          => '3000',
			'autoplay'       => '',
			'controlnav'     => '',
			'pagernav'       => '',
			'slide_count'    => 1,
									
			'el_id'          => '',
			'el_class'       => '',
    		'css_animation'  => '',
    		'anim_type'      => '',
    		'anim_delay'     => '',			
			'css'            => ''		
		), $atts ) );
		
		$sc_class = 'wt_bx_rotator_sc';
						
		$id = mt_rand(9999, 99999);
		if (trim($el_id) != false) {
			$el_id = esc_attr( trim($el_id) );
		} else {
			$el_id = $sc_class . '-' . $id;
		}
		
		wp_print_scripts('wt-extend-bx-slider');
		wp_enqueue_style('wt-extend-bx-slider');
		
		$output = '';		
		
		$autoplay   !== "true" ? $autoplay = 'false' : '';
		$controlnav !== "true" ? $controlnav = 'false' : '';
		$pagernav   !== "true" ? $pagernav = 'false' : '';		
		
		$el_class   = esc_attr( $this->getExtraClass($el_class) );
		$css_class  = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $sc_class.$el_class.vc_shortcode_custom_css_class($css, ' '), $this->settings['base']);	
		$css_class .= $this->wt_sc->getWTCSSAnimationClass($css_animation,$anim_type);
		$anim_data  = $this->wt_sc->getWTCSSAnimationData($css_animation,$anim_delay);
		
		$speed = (int)$speed;
		$pause = (int)$pause;
		
		$output .= '<div id="'.$el_id.'" class="'.$css_class.'"'.$anim_data.'>';
				
		$output .= "\n\t\t\t".'<ul class="wt_bxslider" data-bx-mode="'.$mode.'" data-bx-effect="'.$effect.'" data-bx-speed="'.$speed.'" data-bx-pause="'.$pause.'" data-bx-autoPlay="'.$autoplay.'" data-bx-controlNav="'.$controlnav.'" data-bx-pagerNav="'.$pagernav.'">';
		
		for($i = 1; $i <= $slide_count; $i++) {
			$item_content = '';
								
			isset($atts["content_" . $i]) && $atts["content_" . $i] != "" ? $item_content = $atts["content_" . $i] : '';
			
			if ($item_content != '') {
				$item_content = rawurldecode(strip_tags($item_content));
			}	
							
			$output .= "\n\t\t\t\t".'<li class="item">';
													
				$output .= "\n\t\t\t\t\t" . do_shortcode($item_content);				
				
			$output .= "\t\t\t\t".'</li>';
			
		}
		
		$output .= "\n\t\t\t".'</ul>';
		$output .= "\n\t\t\t".'</div>';			
		
        return $output;
    }
	
}

/*
Register WhoaThemes shortcode within Visual Composer interface.
*/

if (function_exists('vc_map')) {

	$add_wt_sc_func             = new WT_VCSC_SHORTCODE;
	$add_wt_extra_id            = $add_wt_sc_func->getWTExtraId();
	$add_wt_extra_class         = $add_wt_sc_func->getWTExtraClass();
	$add_wt_css_animation       = $add_wt_sc_func->getWTAnimations();
	$add_wt_css_animation_type  = $add_wt_sc_func->getWTAnimationsType();
	$add_wt_css_animation_delay = $add_wt_sc_func->getWTAnimationsDelay();
	
	vc_map( array(
		'name'          => esc_html(__('WT Bx Rotator', 'besmart')),
		'base'          => 'wt_bx_rotator',
		'icon'          => 'wt_vc_ico_bx_rotator',
		'class'         => 'wt_vc_sc_bx_rotator',
		'category'      => esc_html(__('by WhoaThemes', 'besmart')),
		'description'   => esc_html(__('Bx content rotator', 'besmart')),
		'params'        => array(
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html(__('Mode', 'besmart')),
				'param_name'    => 'mode',
				'value'         => array( 
					esc_html(__('Horizontal', 'besmart')) => 'horizontal',
					esc_html(__("Vertical", "besmart"))   => 'vertical',
					esc_html(__('Fade', 'besmart'))       => 'fade'
				),
				'description'   => esc_html(__('Type of transition between slides.', 'besmart'))
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html(__('Effect', 'besmart')),
				'param_name'    => 'effect',
				'value'         => array( 
					esc_html(__("EaseInOut", "besmart")) => 'ease-in-out',
					esc_html(__('EaseOut', 'besmart'))   => 'ease-out',
					esc_html(__('Ease', 'besmart'))      => 'ease',
					esc_html(__('easeIn', 'besmart'))    => 'ease-in',
					esc_html(__('Linear', 'besmart'))    => 'linear'
				),
				'description'   => esc_html(__('Here you can set the transition effect when the items are changing.', 'besmart'))
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html(__('Speed', 'besmart')),
				'param_name'    => 'speed',
				'std'           => '500',
				'description'   => esc_html(__('Here you can set the slide transition duration (in miliseconds). Example: \'100\', \'500\', \'1000\'.', 'besmart'))
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html(__('Pause', 'besmart')),
				'param_name'    => 'pause',
				'std'           => '3000',
				'description'   => esc_html(__('The amount of time between each auto transition. (in miliseconds). Example: \'100\', \'500\', \'1000\'.', 'besmart'))
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html(__('Auto Start', 'besmart')),
				'param_name'    => 'autoplay',
				'value'         => array( esc_html(__( 'Yes, please', 'besmart') ) => 'true' ),
				'description'   => esc_html(__('If YES, slides will automatically transition.', 'besmart'))
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html(__('Control Navigation', 'besmart')),
				'param_name'    => 'controlnav',
				'value'         => array( esc_html(__( 'Yes, please', 'besmart') ) => 'true' ),
				'description'   => esc_html(__('If YES, the Control Navigation (next & prev buttons) is displayed.', 'besmart'))
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html(__('Pager (Navigation)', 'besmart')),
				'param_name'    => 'pagernav',
				'value'         => array( esc_html(__( 'Yes, please', 'besmart') ) => 'true' ),
				'description'   => esc_html(__('If YES, the Pager Navigation is displayed.', 'besmart'))
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html(__('Number Of Items.', 'besmart')),
				'param_name'    => 'slide_count',
				'value'         => array( 
					1  => '1', 
					2  => '2', 
					3  => '3', 
					4  => '4', 
					5  => '5', 
					6  => '6', 
					7  => '7', 
					8  => '8', 
					9  => '9',
					10 => '10', 
					11 => '11',
					12 => '12',
					13 => '13',
					14 => '14',
					15 => '15'
				),
				'description'   => esc_html(__('Specify the number of slide items.', 'besmart')) .' <strong>' . esc_html(__('Maximum allowed is \'15\' items', 'besmart')) .'</strong>.'
			),		
						
				array(
					'type'               => 'textarea_raw_html',
        			//'holder'             => 'div',
					'heading'            => sprintf( esc_html(__("Item Content %d",'besmart')),1),
					'param_name'         => sprintf("content_%d",1),
					'value' 			 => '<p>I am test text block. Click edit button to change this text.</p>',
					'param_holder_class' => 'wtcode border_box wt_dependency',
					'dependency'         => array(
						'element' => 'slide_count', 
						'value'   => array('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15')
					)
				),
					
				array(
					'type'               => 'textarea_raw_html',
					'heading'            => sprintf( esc_html(__("Item Content %d",'besmart')),2),
					'param_name'         => sprintf("content_%d",2),
        			'value'              => '',
					'param_holder_class' => 'wtcode border_box wt_dependency',
					'dependency'         => array(
						'element' => 'slide_count', 
						'value'   => array('2','3','4','5','6','7','8','9','10','11','12','13','14','15')
					)
				),
					
				array(
					'type'               => 'textarea_raw_html',
					'heading'            => sprintf( esc_html(__("Item Content %d",'besmart')),3),
					'param_name'         => sprintf("content_%d",3),
        			'value'              => '',
					'param_holder_class' => 'wtcode border_box wt_dependency',
					'dependency'         => array(
						'element' => 'slide_count', 
						'value'   => array('3','4','5','6','7','8','9','10','11','12','13','14','15')
					)
				),
					
				array(
					'type'               => 'textarea_raw_html',
					'heading'            => sprintf( esc_html(__("Item Content %d",'besmart')),4),
					'param_name'         => sprintf("content_%d",4),
        			'value'              => '',
					'param_holder_class' => 'wtcode border_box wt_dependency',
					'dependency'         => array(
						'element' => 'slide_count', 
						'value'   => array('4','5','6','7','8','9','10','11','12','13','14','15')
					)
				),
					
				array(
					'type'               => 'textarea_raw_html',
					'heading'            => sprintf( esc_html(__("Item Content %d",'besmart')),5),
					'param_name'         => sprintf("content_%d",5),
        			'value'              => '',
					'param_holder_class' => 'wtcode border_box wt_dependency',
					'dependency'         => array(
						'element' => 'slide_count', 
						'value'   => array('5','6','7','8','9','10','11','12','13','14','15')
					)
				),
					
				array(
					'type'               => 'textarea_raw_html',
					'heading'            => sprintf( esc_html(__("Item Content %d",'besmart')),6),
					'param_name'         => sprintf("content_%d",6),
        			'value'              => '',
					'param_holder_class' => 'wtcode border_box wt_dependency',
					'dependency'         => array(
						'element' => 'slide_count', 
						'value'   => array('6','7','8','9','10','11','12','13','14','15')
					)
				),
					
				array(
					'type'               => 'textarea_raw_html',
					'heading'            => sprintf( esc_html(__("Item Content %d",'besmart')),7),
					'param_name'         => sprintf("content_%d",7),
        			'value'              => '',
					'param_holder_class' => 'wtcode border_box wt_dependency',
					'dependency'         => array(
						'element' => 'slide_count', 
						'value'   => array('7','8','9','10','11','12','13','14','15')
					)
				),
					
				array(
					'type'               => 'textarea_raw_html',
					'heading'            => sprintf( esc_html(__("Item Content %d",'besmart')),8),
					'param_name'         => sprintf("content_%d",8),
        			'value'              => '',
					'param_holder_class' => 'wtcode border_box wt_dependency',
					'dependency'         => array(
						'element' => 'slide_count', 
						'value'   => array('8','9','10','11','12','13','14','15')
					)
				),
				
				array(
					'type'               => 'textarea_raw_html',
					'heading'            => sprintf( esc_html(__("Item Content %d",'besmart')),9),
					'param_name'         => sprintf("content_%d",9),
        			'value'              => '',
					'param_holder_class' => 'wtcode border_box wt_dependency',
					'dependency'         => array(
						'element' => 'slide_count', 
						'value'   => array('9','10','11','12','13','14','15')
					)
				),
					
				array(
					'type'               => 'textarea_raw_html',
					'heading'            => sprintf( esc_html(__("Item Content %d",'besmart')),10),
					'param_name'         => sprintf("content_%d",10),
        			'value'              => '',
					'param_holder_class' => 'wtcode border_box wt_dependency',
					'dependency'         => array(
						'element' => 'slide_count', 
						'value'   => array('10','11','12','13','14','15')
					)
				),
					
				array(
					'type'               => 'textarea_raw_html',
					'heading'            => sprintf( esc_html(__("Item Content %d",'besmart')),11),
					'param_name'         => sprintf("content_%d",11),
        			'value'              => '',
					'param_holder_class' => 'wtcode border_box wt_dependency',
					'dependency'         => array(
						'element' => 'slide_count', 
						'value'   => array('11','12','13','14','15')
					)
				),
					
				array(
					'type'               => 'textarea_raw_html',
					'heading'            => sprintf( esc_html(__("Item Content %d",'besmart')),12),
					'param_name'         => sprintf("content_%d",12),
        			'value'              => '',
					'param_holder_class' => 'wtcode border_box wt_dependency',
					'dependency'         => array(
						'element' => 'slide_count', 
						'value'   => array('12','13','14','15')
					)
				),
				
				array(
					'type'               => 'textarea_raw_html',
					'heading'            => sprintf( esc_html(__("Item Content %d",'besmart')),13),
					'param_name'         => sprintf("content_%d",13),
        			'value'              => '',
					'param_holder_class' => 'wtcode border_box wt_dependency',
					'dependency'         => array(
						'element' => 'slide_count', 
						'value'   => array('13','14','15')
					)
				),
					
				array(
					'type'               => 'textarea_raw_html',
					'heading'            => sprintf( esc_html(__("Item Content %d",'besmart')),14),
					'param_name'         => sprintf("content_%d",14),
        			'value'              => '',
					'param_holder_class' => 'wtcode border_box wt_dependency',
					'dependency'         => array(
						'element' => 'slide_count', 
						'value'   => array('14','15')
					)
				),
					
				array(
					'type'               => 'textarea_raw_html',
					'heading'            => sprintf( esc_html(__("Item Content %d",'besmart')),15),
					'param_name'         => sprintf("content_%d",15),
        			'value'              => '',
					'param_holder_class' => 'wtcode border_box wt_dependency',
					'dependency'         => array(
						'element' => 'slide_count', 
						'value'   => array('15')
					)
				),
			
			$add_wt_extra_id,
			$add_wt_extra_class,
			$add_wt_css_animation,
			$add_wt_css_animation_type,
			$add_wt_css_animation_delay,
			
			array(
				'type'          => 'css_editor',
				'heading'       => esc_html(__('Css', 'besmart')),
				'param_name'    => 'css',
				'group'         => esc_html(__('Design options', 'besmart'))
			)
		)
	));
	
}