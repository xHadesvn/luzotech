<?php

// File Security Check
if (!defined('ABSPATH')) die('-1');

/*
Register WhoaThemes shortcode.
*/

class WPBakeryShortCode_WT_timeline extends WPBakeryShortCode {
	
	private $wt_sc;
	
	public function __construct($settings) {
        parent::__construct($settings);
		$this->wt_sc = new WT_VCSC_SHORTCODE;
	}
				
	protected function content($atts, $content = null) {
		
		// Get and extract shortcode attributes
		$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
		extract( $atts );	
		
		$date      = esc_html( $date );	
		$alignment = esc_html( $alignment );
		$title     = esc_html( $title );
				
		$id = mt_rand(9999, 99999);
		if (trim($el_id) != false) {
			$el_id = ' id="' . esc_attr( trim($el_id) ) . '"';
		} else {
			// $el_id = $sc_class . '-' . $id;
			$el_id = '';
		}	
		
		/*if ($alignment == 'left') {
			$alignment = ' pull-left';
		} else {
			$alignment = ' pull-right';
		}	*/
		
		if ($alignment == 'left') {
			$alignmentCss = ' story-left';
		} else {
			$alignmentCss = ' story-right';
		}	
		
		$sc_class = 'wt_timeline_sc col-md-6 col-sm-12';	
						
		$el_class  = esc_attr( $this->getExtraClass($el_class) );		
		$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $sc_class.$el_class, $this->settings['base']);		
			
		$date_css_class    = $this->wt_sc->getWTCSSAnimationClass($date_css_animation,$date_anim_type);
		$content_css_class = $this->wt_sc->getWTCSSAnimationClass($content_css_animation,$content_anim_type);
		
		$date_anim_data    = $this->wt_sc->getWTCSSAnimationData($date_css_animation,$date_anim_delay);
		$content_anim_data = $this->wt_sc->getWTCSSAnimationData($content_css_animation,$content_anim_delay);	
		
		$content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content	
		
		$img_id = preg_replace('/[^\d]/', '', $image);
		
		if ( $border_color != '' && ($style == 'vc_box_border' || $style == 'vc_box_border_circle' || $style == 'vc_box_outline' || $style == 'vc_box_outline_circle') ) {
			$img = wt_wpb_getImageBySize(array( 'attach_id' => $img_id, 'thumb_size' => $img_size, 'class' => $style, 'style' => $el_style ));
		} else {
			$img = wpb_getImageBySize(array( 'attach_id' => $img_id, 'thumb_size' => $img_size, 'class' => $style ));
		}
				
		$img_output = ($style=='vc_box_shadow_3d') ? '<span class="vc_box_shadow_3d_wrap">' . $img['thumbnail'] . '</span>' : '<div class="story-image">' . $img['thumbnail'] . '</div>';	
		
		$date_out            = '';
		$timeline_item_start = '';
		$timeline_item_end   = '';
		$title_out           = '';
		$content_out         = '';
						
		if ($date) {
			$date_out .= '<div class="wt_timeline_year text-center'.$date_css_class.'"'.$date_anim_data.'>';
				$date_out .= '<span>' . $date . '</span>';
			$date_out .= '</div>';
		}
		
		if ($content || $title) {
			$timeline_item_start .= '<div class="wt_timeline_item story-block'. $alignmentCss .'">';
				$timeline_item_start .= '<div class="wt_timeline_item_content'.$content_css_class.'"'.$content_anim_data.'>';
				$timeline_item_end   .= '</div>'; 
			$timeline_item_end   .= '<span class="story-arrow"></span>';
			$timeline_item_end   .= '<span class="h-line"></span>';
			$timeline_item_end   .= '</div>'; 
		}		
		
		if ($title) {
			$title_out .= '<div class="wt_timeline_item_title clearfix">';
				$title_out .= '<h4>' . $title . '</h4>';
			$title_out .= '</div>';
		}		
		
		if ($content) {
			$content_out .= '<div class="wt_timeline_item_text">';
				$content_out .= $content;
			$content_out .= '</div>';
		} 	
			
		$output = '<div'.$el_id.' class="'.$css_class.'">';
			$output .= $date_out;
			$output .= $timeline_item_start;
				$output .= $title_out;
				$output .= $content_out;
				$output .= $img_output;	
			$output .= $timeline_item_end;
		$output .= '</div>';
		
        return $output;
								
    }
	
}
	
/*
Register WhoaThemes shortcode within Visual Composer interface.
*/

if (function_exists('vc_map')) {

	$add_wt_sc_func             = new WT_VCSC_SHORTCODE;
	$add_wt_extra_id            = $add_wt_sc_func->getWTExtraId();
	$add_wt_extra_class         = $add_wt_sc_func->getWTExtraClass();
	$add_wt_css_animation       = $add_wt_sc_func->getWTAnimations();
	$add_wt_css_animation_type  = $add_wt_sc_func->getWTAnimationsType();
	$add_wt_css_animation_delay = $add_wt_sc_func->getWTAnimationsDelay();
	
	vc_map( array(
		'name'          => esc_html__('WT Timeline', 'besmart'),
		'base'          => 'wt_timeline',
		'icon'          => 'wt_vc_ico_timeline',
		'class'         => 'wt_vc_sc_timeline',
		'category'      => esc_html__('by WhoaThemes', 'besmart'),
		'description'   => esc_html__('Build a custom timeline', 'besmart'),
		'params'        => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__('Date / Step', 'besmart'),
				'param_name'    => 'date',
				'description'   => esc_html__('Enter the date / step for the timeline element.', 'besmart')
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__('Alignment', 'besmart'),
				'param_name'    => 'alignment',
				'value'         => array(  
					esc_html__('Left', 'besmart')   => 'left',
					esc_html__('Right', 'besmart')  => 'right'
				),
				'description'   => esc_html__('Define timeline alignment: left / right.', 'besmart')
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__('Title', 'besmart'),
				'param_name'    => 'title',
				'description'   => esc_html__('Enter the title for the timeline element.', 'besmart')
			),
			array(
				'type'          => 'attach_image',
				'heading'       => esc_html(__('Image', 'besmart')),
				'param_name'    => 'image',
				'value'         => '',
				'description'   => esc_html(__('Select image from media library.', 'besmart'))
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html(__('Image size', 'besmart')),
				'param_name'    => 'img_size',
				'description'   => esc_html(__('Enter image size. Example: "thumbnail", "medium", "large", "full" or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size.', 'besmart'))
			),
			array(
				'type'          => 'textarea_html',
				'heading'       => esc_html__('Text', 'besmart'),
				'param_name'    => 'content',
				'value' 		=>  '<p>' . esc_html__( 'I am text block. Click edit button to change this text.', 'besmart' ). '</p>',
				'description'   => esc_html__('Add text for the timeline.', 'besmart')
			),
			
			$add_wt_extra_id,
			$add_wt_extra_class,			
			
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Date / Step CSS WT Animation", "besmart"),
				"param_name" => "date_css_animation",
				"value" => array( esc_html__("No", "besmart") => '', esc_html__("Hinge", "besmart") => "hinge", esc_html__("Flash", "besmart") => "flash", esc_html__("Shake", "besmart") => "shake", esc_html__("Bounce", "besmart") => "bounce", esc_html__("Tada", "besmart") => "tada", esc_html__("Swing", "besmart") => "swing", esc_html__("Wobble", "besmart") => "wobble", esc_html__("Pulse", "besmart") => "pulse", esc_html__("Flip", "besmart") => "flip", esc_html__("FlipInX", "besmart") => "flipInX", esc_html__("FlipOutX", "besmart") => "flipOutX", esc_html__("FlipInY", "besmart") => "flipInY", esc_html__("FlipOutY", "besmart") => "flipOutY", esc_html__("FadeIn", "besmart") => "fadeIn", esc_html__("FadeInUp", "besmart") => "fadeInUp", esc_html__("FadeInDown", "besmart") => "fadeInDown", esc_html__("FadeInLeft", "besmart") => "fadeInLeft", esc_html__("FadeInRight", "besmart") => "fadeInRight", esc_html__("FadeInUpBig", "besmart") => "fadeInUpBig", esc_html__("FadeInDownBig", "besmart") => "fadeInDownBig", esc_html__("FadeInLeftBig", "besmart") => "fadeInLeftBig", esc_html__("FadeInRightBig", "besmart") => "fadeInRightBig", esc_html__("FadeOut", "besmart") => "fadeOut", esc_html__("FadeOutUp", "besmart") => "fadeOutUp", esc_html__("FadeOutDown", "besmart") => "fadeOutDown", esc_html__("FadeOutLeft", "besmart") => "fadeOutLeft", esc_html__("FadeOutRight", "besmart") => "fadeOutRight", esc_html__("fadeOutUpBig", "besmart") => "fadeOutUpBig", esc_html__("FadeOutDownBig", "besmart") => "fadeOutDownBig", esc_html__("FadeOutLeftBig", "besmart") => "fadeOutLeftBig", esc_html__("FadeOutRightBig", "besmart") => "fadeOutRightBig", esc_html__("BounceIn", "besmart") => "bounceIn", esc_html__("BounceInUp", "besmart") => "bounceInUp", esc_html__("BounceInDown", "besmart") => "bounceInDown", esc_html__("BounceInLeft", "besmart") => "bounceInLeft", esc_html__("BounceInRight", "besmart") => "bounceInRight", esc_html__("BounceOut", "besmart") => "bounceOut", esc_html__("BounceOutUp", "besmart") => "bounceOutUp", esc_html__("BounceOutDown", "besmart") => "bounceOutDown", esc_html__("BounceOutLeft", "besmart") => "bounceOutLeft", esc_html__("BounceOutRight", "besmart") => "bounceOutRight", esc_html__("RotateIn", "besmart") => "rotateIn", esc_html__("RotateInUpLeft", "besmart") => "rotateInUpLeft", esc_html__("RotateInDownLeft", "besmart") => "rotateInDownLeft", esc_html__("RotateInUpRight", "besmart") => "rotateInUpRight", esc_html__("RotateInDownRight", "besmart") => "rotateInDownRight", esc_html__("RotateOut", "besmart") => "rotateOut", esc_html__("RotateOutUpLeft", "besmart") => "rotateOutUpLeft", esc_html__("RotateOutDownLeft", "besmart") => "rotateOutDownLeft", esc_html__("RotateOutUpRight", "besmart") => "rotateOutUpRight", esc_html__("RotateOutDownRight", "besmart") => "rotateOutDownRight", esc_html__("RollIn", "besmart") => "rollIn", esc_html__("RollOut", "besmart") => "rollOut", esc_html__("LightSpeedIn", "besmart") => "lightSpeedIn", esc_html__("LightSpeedOut", "besmart") => "lightSpeedOut" ),
				"description" => esc_html__("Select type of animation (for timeline date / step) if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.", "besmart"),
				'group' => esc_html__('Extra settings', 'besmart')
			),	
			array(
				"type" => "textfield",
				"heading" => esc_html__("Date / Step WT Animation Delay", "besmart"),
				"param_name" => "date_anim_delay",
				"description" => esc_html__("Here you can set a specific delay for the timeline date / step animation (miliseconds). Example: '100', '500', '1000'.", "besmart"),
				'group' => esc_html__('Extra settings', 'besmart')
			),
			array(
				"type"        => "dropdown",
				"heading"     => esc_html__("Date / Step WT Animation Visible Type", "besmart"),
				"param_name"  => "date_anim_type",
				"value"       => array( esc_html__("Animate when element is visible", "besmart") => 'wt_animate_if_visible', esc_html__("Animate if element is almost visible", "besmart") => "wt_animate_if_almost_visible" ),
				"description" => esc_html__("Select when the type of animation should start for timeline date / step element.", "besmart"),
				'group'       => esc_html__('Extra settings', 'besmart')
			),	
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Content CSS WT Animation", "besmart"),
				"param_name" => "content_css_animation",
				"value" => array( esc_html__("No", "besmart") => '', esc_html__("Hinge", "besmart") => "hinge", esc_html__("Flash", "besmart") => "flash", esc_html__("Shake", "besmart") => "shake", esc_html__("Bounce", "besmart") => "bounce", esc_html__("Tada", "besmart") => "tada", esc_html__("Swing", "besmart") => "swing", esc_html__("Wobble", "besmart") => "wobble", esc_html__("Pulse", "besmart") => "pulse", esc_html__("Flip", "besmart") => "flip", esc_html__("FlipInX", "besmart") => "flipInX", esc_html__("FlipOutX", "besmart") => "flipOutX", esc_html__("FlipInY", "besmart") => "flipInY", esc_html__("FlipOutY", "besmart") => "flipOutY", esc_html__("FadeIn", "besmart") => "fadeIn", esc_html__("FadeInUp", "besmart") => "fadeInUp", esc_html__("FadeInDown", "besmart") => "fadeInDown", esc_html__("FadeInLeft", "besmart") => "fadeInLeft", esc_html__("FadeInRight", "besmart") => "fadeInRight", esc_html__("FadeInUpBig", "besmart") => "fadeInUpBig", esc_html__("FadeInDownBig", "besmart") => "fadeInDownBig", esc_html__("FadeInLeftBig", "besmart") => "fadeInLeftBig", esc_html__("FadeInRightBig", "besmart") => "fadeInRightBig", esc_html__("FadeOut", "besmart") => "fadeOut", esc_html__("FadeOutUp", "besmart") => "fadeOutUp", esc_html__("FadeOutDown", "besmart") => "fadeOutDown", esc_html__("FadeOutLeft", "besmart") => "fadeOutLeft", esc_html__("FadeOutRight", "besmart") => "fadeOutRight", esc_html__("fadeOutUpBig", "besmart") => "fadeOutUpBig", esc_html__("FadeOutDownBig", "besmart") => "fadeOutDownBig", esc_html__("FadeOutLeftBig", "besmart") => "fadeOutLeftBig", esc_html__("FadeOutRightBig", "besmart") => "fadeOutRightBig", esc_html__("BounceIn", "besmart") => "bounceIn", esc_html__("BounceInUp", "besmart") => "bounceInUp", esc_html__("BounceInDown", "besmart") => "bounceInDown", esc_html__("BounceInLeft", "besmart") => "bounceInLeft", esc_html__("BounceInRight", "besmart") => "bounceInRight", esc_html__("BounceOut", "besmart") => "bounceOut", esc_html__("BounceOutUp", "besmart") => "bounceOutUp", esc_html__("BounceOutDown", "besmart") => "bounceOutDown", esc_html__("BounceOutLeft", "besmart") => "bounceOutLeft", esc_html__("BounceOutRight", "besmart") => "bounceOutRight", esc_html__("RotateIn", "besmart") => "rotateIn", esc_html__("RotateInUpLeft", "besmart") => "rotateInUpLeft", esc_html__("RotateInDownLeft", "besmart") => "rotateInDownLeft", esc_html__("RotateInUpRight", "besmart") => "rotateInUpRight", esc_html__("RotateInDownRight", "besmart") => "rotateInDownRight", esc_html__("RotateOut", "besmart") => "rotateOut", esc_html__("RotateOutUpLeft", "besmart") => "rotateOutUpLeft", esc_html__("RotateOutDownLeft", "besmart") => "rotateOutDownLeft", esc_html__("RotateOutUpRight", "besmart") => "rotateOutUpRight", esc_html__("RotateOutDownRight", "besmart") => "rotateOutDownRight", esc_html__("RollIn", "besmart") => "rollIn", esc_html__("RollOut", "besmart") => "rollOut", esc_html__("LightSpeedIn", "besmart") => "lightSpeedIn", esc_html__("LightSpeedOut", "besmart") => "lightSpeedOut" ),
				"description" => esc_html__("Select type of animation (for timeline content / text) if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.", "besmart"),
				'group' => esc_html__('Extra settings', 'besmart')
			),	
			array(
				"type" => "textfield",
				"heading" => esc_html__("Content WT Animation Delay", "besmart"),
				"param_name" => "content_anim_delay",
				"description" => esc_html__("Here you can set a specific delay for the timeline content / text animation (miliseconds). Example: '100', '500', '1000'.", "besmart"),
				'group' => esc_html__('Extra settings', 'besmart')
			),
			array(
				"type"        => "dropdown",
				"heading"     => esc_html__("Content WT Animation Visible Type", "besmart"),
				"param_name"  => "content_anim_type",
				"value"       => array( esc_html__("Animate when element is visible", "besmart") => 'wt_animate_if_visible', esc_html__("Animate if element is almost visible", "besmart") => "wt_animate_if_almost_visible" ),
				"std"         => "wt_animate_if_almost_visible",
				"description" => esc_html__("Select when the type of animation should start for timeline content element.", "besmart"),
				'group'       => esc_html__('Extra settings', 'besmart')
			),	
						
			array(
				'type'          => 'css_editor',
				'heading'       => esc_html__('Css', 'besmart'),
				'param_name'    => 'css',
				'group'         => esc_html__('Design options', 'besmart')
			)
		)
	));	
	
}