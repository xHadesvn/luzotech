<?php

$qodux_options = array(
	array(
		"class" => "nav-tab-wrapper",
		"default" => '',
		"options" => array(
			"blog_settings" => esc_html(__('Blog','besmart')),
			"single_post_settings" => esc_html(__('Single Post','besmart')),
			"meta_information_settings" => esc_html(__('Meta Informations','besmart')),
			"featured_entry_settings" => esc_html(__('Featured Entry','besmart')),
		),
		"type" => "qodux_navigation",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "blog_settings",
	),	
		array(
			"name" => esc_html(__("Blog Settings",'besmart')),
			"type" => "qodux_open"
		),
			array(
				"name" => esc_html(__("Layout",'besmart')),
				"desc" => "Here you can set the layout of the blog page.",
				"id" => "layout",
				"default" => 'right',
				"options" => array(
					"full" => esc_html(__('Full Width','besmart')),
					"right" => esc_html(__('Right Sidebar','besmart')),
					"left" => esc_html(__('Left Sidebar','besmart')),
				),
				"type" => "qodux_select",
			),
			array(
				"name" => esc_html(__("Featured Post Entry Type",'besmart')),
				"desc" => "The style in which the post entry will be displayed. This could be an image/slideshow/mp3/video (youtube, vimeo, daylimotion, metacafe, google, .flv, .f4v, .mp4)",
				"id" => "featured_image_type",
				"default" => 'full',
				"options" => array(
					"full" => esc_html(__('Full Width','besmart')),
					"left" => esc_html(__('Left Float','besmart')),
				),
				"type" => "qodux_select",
			),
			array(
				"name" => esc_html(__("Display Full Blog Posts",'besmart')),
				"desc" => esc_html(__("If the option is set to ON, the blog postswill be full displayed on the index page.",'besmart')),
				"id" => "display_full",
				"default" => false,
				"type" => "qodux_toggle"
			),
			array(
				"name" => esc_html(__("Exclude Categories",'besmart')),
				"desc" => esc_html(__("ff you don't want to display custom categories in the blog pages, you can set them here. Also you can exclude multiple categories.",'besmart')),
				"id" => "exclude_categorys",
				"default" => array(),
				"target" => "cat",
				"prompt" => esc_html(__("Choose category...",'besmart')),
				"chosen" => "true",
				"type" => "qodux_multiselect",
				//"type" => "multidropdown"
			),
			array(
				"name" => esc_html(__("Gap Between Posts",'besmart')),
				"desc" => "Here you can set the distance between the blog posts.",
				"id" => "posts_gap",
				"min" => "0",
				"max" => "200",
				"step" => "1",
				"unit" => 'px',
				"default" => "60",
				"type" => "qodux_range"
			),
			array(
				"type" => "qodux_close"
			),
		array(
			"type" => "qodux_reset"
		),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "single_post_settings",
	),		
		array(
			"name" => esc_html(__("Single Post Settings",'besmart')),
			"type" => "qodux_open"
		),
			array(
				"name" => esc_html(__("Layout",'besmart')),
				"desc" => "Here you can set the layout of the blog posts.",
				"id" => "single_layout",
				"default" => 'right',
				"options" => array(
					"full" => esc_html(__('Full Width','besmart')),
					"right" => esc_html(__('Right Sidebar','besmart')),
					"left" => esc_html(__('Left Sidebar','besmart')),
				),
				"type" => "qodux_select",
			),
			array(
				"name" => esc_html(__("Featured Post Entry",'besmart')),
				"desc" => esc_html(__("If the button is set to ON then the Featured Image/Slideshow/Mp3/Video will be displayed on the Single Blog post.",'besmart')),
				"id" => "featured_image",
				"default" => true,
				"type" => "qodux_toggle"
			),
			array(
				"name" => esc_html(__("Featured Post Entry Type",'besmart')),
				"desc" => "The style in which the post entry will be displayed on Single Blog post. This could be an image/slideshow/mp3/video (youtube, vimeo, daylimotion, metacafe, google, .flv, .f4v, .mp4). ",
				"id" => "single_featured_image_type",
				"default" => 'full',
				"options" => array(
					"full" => esc_html(__('Full Width','besmart')),
					"left" => esc_html(__('Left Float','besmart')),
				),
				"type" => "qodux_select",
			),
			array(
				"name" => esc_html(__("Featured Image for Lightbox",'besmart')),
				"desc" => esc_html(__("If the button is set to ON then the full image will be opened in the lightbox when you click on Featured Image of the Blog Single Post page.",'besmart')),
				"id" => "featured_image_lightbox",
				"default" => false,
				"type" => "qodux_toggle"
			),
			array(
				"name" => esc_html(__("About Author Box",'besmart')),
				"desc" => "If the button is set to ON then the About Author Box will be displayed in the Blog Single Post page.",
				"id" => "author",
				"default" => false,
				"type" => "qodux_toggle"
			),
			array(
				"name" => esc_html(__("Previous & Next Navigation",'besmart')),
				"desc" => "Display pagination for the posts.",
				"id" => "entry_navigation",
				"default" => true,
				"type" => "qodux_toggle"
			),
		array(
			"type" => "qodux_close"
		),
		array(
			"type" => "qodux_reset"
		),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "meta_information_settings",
	),
		array(
			"name" => esc_html(__("Meta Informations",'besmart')),
			"type" => "qodux_open"
		),
			array(
				"name" => esc_html(__("Date",'besmart')),
				"desc" => "Display date in Meta Informations box.",
				"id" => "meta_date",
				"default" => true,
				"type" => "qodux_toggle"
			),
			array(
				"name" => esc_html(__("Author",'besmart')),
				"desc" => "Display author in Meta Informations box.",
				"id" => "meta_author",
				"default" => false,
				"type" => "qodux_toggle"
			),
			array(
				"name" => esc_html(__("Author Link Type",'besmart')),
				"desc" => "Select the link type for the author ( website link which is set in the user profile page, author's posts page or none ).",
				"id" => "author_link_type",
				"default" => 'archive',
				"options" => array(
					"website" => esc_html(__('Website','besmart')),
					"archive" => esc_html(__('Author archive','besmart')),
					""        => esc_html(__('None','besmart')),
				),
				"type" => "qodux_select",
			),
			array(
				"name" => esc_html(__("Categories",'besmart')),
				"desc" => "Display categories in Meta Informations box.",
				"id" => "meta_category",
				"default" => true,
				"type" => "qodux_toggle"
			),
			array(
				"name" => esc_html(__("Tags",'besmart')),
				"desc" => "Display tags in Meta Informations box.",
				"id" => "meta_tags",
				"default" => false,
				"type" => "qodux_toggle"
			),
			array(
				"name" => esc_html(__("Comments",'besmart')),
				"desc" => "Display comments number in Meta Informations box.",
				"id" => "meta_comment",
				"default" => true,
				"type" => "qodux_toggle"
			),
		array(
			"type" => "qodux_close"
		),
		array(
			"type" => "qodux_reset"
		),
		array(
			"name" => esc_html(__("Meta Informations Single Post",'besmart')),
			"type" => "qodux_open"
		),
			array(
				"name" => esc_html(__("Date",'besmart')),
				"desc" => "Display date in Meta Informations box.",
				"id" => "single_meta_date",
				"default" => true,
				"type" => "qodux_toggle"
			),
			array(
				"name" => esc_html(__("Author",'besmart')),
				"desc" => "Display author in Meta Informations box.",
				"id" => "single_meta_author",
				"default" => true,
				"type" => "qodux_toggle"
			),
			array(
				"name" => esc_html(__("Categories",'besmart')),
				"desc" => "Display categories in Meta Informations box.",
				"id" => "single_meta_category",
				"default" => true,
				"type" => "qodux_toggle"
			),
			array(
				"name" => esc_html(__("Tags",'besmart')),
				"desc" => "Display tags in Meta Informations box.",
				"id" => "single_meta_tags",
				"default" => true,
				"type" => "qodux_toggle"
			),
			array(
				"name" => esc_html(__("Comments",'besmart')),
				"desc" => "Display comments number in Meta Informations box.",
				"id" => "single_meta_comment",
				"default" => true,
				"type" => "qodux_toggle"
			),
		array(
			"type" => "qodux_close"
		),
		array(
			"type" => "qodux_reset"
		),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "featured_entry_settings",
	),
		array(
			"name" => esc_html(__("Full Width Featured Post Entry",'besmart')),
			"type" => "qodux_open"
		),
			array(
				"name" => esc_html(__("Blog Adaptive Height",'besmart')),
				"desc" => esc_html(__("If the button is set to ON then the Featured Image height depends on the original image.",'besmart')),
				"id" => "adaptive_height",
				"default" => false,
				"type" => "qodux_toggle"
			),
			array(
				"name" => esc_html(__("Single Adaptive Height",'besmart')),
				"desc" => esc_html(__("If the button is set to ON then the Featured Image height depends on the original image.",'besmart')),
				"id" => "single_adaptive_height",
				"default" => false,
				"type" => "qodux_toggle"
			),
			array(
				"name" => esc_html(__("\"Full Layout\" - Featured Image Height",'besmart')),
				"desc" => esc_html(__("You can set the Featured Image Entry height for full width layouts. ( Adaptive height option above should be OFF ).  Default height is 550px.",'besmart')),
				"id" => "image_height",
				"min" => "1",
				"max" => "640",
				"step" => "1",
				"unit" => 'px',
				"default" => "550",
				"type" => "qodux_range"
			),
			array(
				"name" => esc_html(__("\"Full Layout\" - Featured Slide Height",'besmart')),
				"desc" => esc_html(__("You can set the Featured Slide Entry height for full width layouts. Default height is 550px.",'besmart')),
				"id" => "slide_height",
				"min" => "1",
				"max" => "640",
				"step" => "1",
				"unit" => 'px',
				"default" => "550",
				"type" => "qodux_range"
			),
			array(
				"name" => esc_html(__("\"Sidebar Layout\" - Featured Image Height",'besmart')),
				"desc" => esc_html(__("You can set the Featured Image Entry height for layouts with sidebar. ( Adaptive height option above should be OFF ). Default height is 250px.",'besmart')),
				"id" => "sidebar_image_height",
				"min" => "1",
				"max" => "640",
				"step" => "1",
				"unit" => 'px',
				"default" => "429",
				"type" => "qodux_range"
			),
			array(
				"name" => esc_html(__("\"Sidebar Layout\" - Featured Slide Height",'besmart')),
				"desc" => esc_html(__("You can set the Featured Slide Entry height for layouts with sidebar. Default height is 400px.",'besmart')),
				"id" => "sidebar_slide_height",
				"min" => "1",
				"max" => "640",
				"step" => "1",
				"unit" => 'px',
				"default" => "429",
				"type" => "qodux_range"
			),
		array(
			"type" => "qodux_close"
		),
		array(
			"type" => "qodux_reset"
		),
		array(
			"name" => esc_html(__("Left Float Featured Post Entry",'besmart')),
			"type" => "qodux_open"
		),
			array(
				"name" => esc_html(__("\"Full Layout\" - Width",'besmart')),
				"desc" => esc_html(__("You can set the width of the left floated entry wrapper for Full Layouts. Original image width is 720px because on smaller screens it will be displayed at full size. Default width is 460px.",'besmart')),
				"id" => "left_width",
				"min" => "1",
				"max" => "640",
				"step" => "1",
				"unit" => 'px',
				"default" => "460",
				"type" => "qodux_range"
			),
			array(
				"name" => esc_html(__("\"Full Layout\" - Featured Image Height",'besmart')),
				"desc" => esc_html(__("You can set the height of the left floated Image Entry for Full Layouts. This is the height for smaller screens where the image will be displayed in full size. On larger screens the image height will be resized to fit in left image wrapper. Default height is 405px.",'besmart')),
				"id" => "left_image_height",
				"min" => "1",
				"max" => "640",
				"step" => "1",
				"unit" => 'px',
				"default" => "405",
				"type" => "qodux_range"
			),
			array(
				"name" => esc_html(__("\"Full Layout\" - Featured Slide Height",'besmart')),
				"desc" => esc_html(__("You can set the height of the left floated Slide Entry for Full Layouts. This is the height for smaller screens where the image will be displayed in full size. On larger screens the image height will be resized to fit in left image wrapper. Default height is 405px.",'besmart')),
				"id" => "left_slide_height",
				"min" => "1",
				"max" => "640",
				"step" => "1",
				"unit" => 'px',
				"default" => "405",
				"type" => "qodux_range"
			),
			array(
				"name" => esc_html(__("\"Sidebar Layout\" - Width",'besmart')),
				"desc" => esc_html(__("You can set the width of the left floated entry wrapper for Layouts With Sidebar. Original image width is 720px because on smaller screens it will be displayed at full size. Default width is 380px.",'besmart')),
				"id" => "sidebar_left_width",
				"min" => "1",
				"max" => "460",
				"step" => "1",
				"unit" => 'px',
				"default" => "380",
				"type" => "qodux_range"
			),
			array(
				"name" => esc_html(__("\"Sidebar Layout\" - Featured Image Height",'besmart')),
				"desc" => esc_html(__("You can set the height of the left floated Image Entry for Layouts With Sidebar. This is the height for smaller screens where the image will be displayed in full size. On larger screens the image height will be resized to fit in left image wrapper. Default height is 405px.",'besmart')),
				"id" => "sidebar_left_image_height",
				"min" => "1",
				"max" => "460",
				"step" => "1",
				"unit" => 'px',
				"default" => "405",
				"type" => "qodux_range"
			),
			array(
				"name" => esc_html(__("\"Sidebar Layout\" - Featured Slide Height",'besmart')),
				"desc" => esc_html(__("You can set the height of the left floated Slide Entry for Layouts With Sidebar. This is the height for smaller screens where the image will be displayed in full size. On larger screens the image height will be resized to fit in left image wrapper.  Default height is 405px.",'besmart')),
				"id" => "sidebar_left_slide_height",
				"min" => "1",
				"max" => "460",
				"step" => "1",
				"unit" => 'px',
				"default" => "405",
				"type" => "qodux_range"
			),
		array(
			"type" => "qodux_close"
		),
		array(
			"type" => "qodux_reset"
		),
	array(
		"type" => "qodux_group_end",
	),
);
return array(
	'auto' => true,
	'name' => 'blog',
	'options' => $qodux_options
);