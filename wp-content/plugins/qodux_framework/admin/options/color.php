<?php
$qodux_options = array(
	array(
		"class" => "nav-tab-wrapper",
		"default" => '',
		"options" => array(
			"page_colors" => esc_html(__('Page Element Colors','besmart')),
			"social_colors" => esc_html(__('Social Icons Colors','besmart')),
		),
		"type" => "qodux_navigation",
	),	
	array(
		"type" => "qodux_group_start",
		"group_id" => "page_colors",
	),
		array(
			"name" => esc_html(__("Color Setting",'besmart')),
			"type" => "qodux_open"
		),
		array(
			"name" => esc_html(__("Page Text Color",'besmart')),
			"id" => "page_content",
			"default" => "",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Page Header Color",'besmart')),
			"id" => "content_header",
			"default" => "",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Page H1 Color",'besmart')),
			"id" => "content_h1",
			"default" => "",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Page H2 Color",'besmart')),
			"id" => "content_h2",
			"default" => "",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Page H3 Color",'besmart')),
			"id" => "content_h3",
			"default" => "",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Page H4 Color",'besmart')),
			"id" => "content_h4",
			"default" => "",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Page H5 Color",'besmart')),
			"id" => "content_h5",
			"default" => "",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Page H6 Color",'besmart')),
			"id" => "content_h6",
			"default" => "",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Logo Text Color",'besmart')),
			"id" => "logo_color",
			"default" => "",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Logo Description Text Color",'besmart')),
			"id" => "logo_color_desc",
			"default" => "",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Top Level Menu Color",'besmart')),
			"id" => "menu_top",
			"default" => "",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Top Level Menu Hover Color",'besmart')),
			"id" => "menu_top_hover",
			"default" => "",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Top Level Current Menu Color",'besmart')),
			"id" => "menu_top_current",
			"default" => "",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Sub Level Menu Color",'besmart')),
			"id" => "menu_sub",
			"default" => "",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Sub Level Menu Hover Color",'besmart')),
			"id" => "menu_sub_hover",
			"default" => "",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Footer Text Color",'besmart')),
			"id" => "footer_text",
			"default" => "",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Footer Widget Title Color",'besmart')),
			"id" => "footer_title",
			"default" => "",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Copyright Text Color",'besmart')),
			"id" => "copyright",
			"default" => "",
			"type" => "qodux_color"
		),
		array(
			"type" => "qodux_close"
		),
		array(
			"type" => "qodux_reset"
		),
	array(
		"type" => "qodux_group_end",
	),	
	array(
		"type" => "qodux_group_start",
		"group_id" => "social_colors",
	),
		array(
			"name" => esc_html(__("Social Icons Color Settings",'besmart')),
			"type" => "qodux_open"
		),
		array(
			"name" => esc_html(__("Aim",'besmart')),
			"id" => "aim_color",
			"default" => "#452806",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Apple",'besmart')),
			"id" => "apple_color",
			"default" => "#231f20",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Behance",'besmart')),
			"id" => "behance_color",
			"default" => "#1378fe",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Blogger",'besmart')),
			"id" => "blogger_color",
			"default" => "#fe6601",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Delicious",'besmart')),
			"id" => "delicious_color",
			"default" => "#3274d2",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Deviantart",'besmart')),
			"id" => "deviantart_color",
			"default" => "#c8da2e",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Digg",'besmart')),
			"id" => "digg_color",
			"default" => "#005f95",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Dribble",'besmart')),
			"id" => "dribbble_color",
			"default" => "#ea4b8b",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Dropbox",'besmart')),
			"id" => "dropbox_color",
			"default" => "#007ee5",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Email",'besmart')),
			"id" => "email_color",
			"default" => "#262626",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Ember",'besmart')),
			"id" => "ember_color",
			"default" => "#e11a3b",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Facebook",'besmart')),
			"id" => "facebook_color",
			"default" => "#3C5A9A",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Flickr",'besmart')),
			"id" => "flickr_color",
			"default" => "#0062dd",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Forrst",'besmart')),
			"id" => "forrst_color",
			"default" => "#166021",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Google",'besmart')),
			"id" => "google_color",
			"default" => "#4a7af6",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Google Plus",'besmart')),
			"id" => "googleplus_color",
			"default" => "#da2713",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Github",'besmart')),
			"id" => "github_color",
			"default" => "#569e3d",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Html5",'besmart')),
			"id" => "html5_color",
			"default" => "#e54d26",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Instagram",'besmart')),
			"id" => "instagram_color",
			"default" => "#517fa4",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Last Fm",'besmart')),
			"id" => "lastfm_color",
			"default" => "#c30d19",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("LinkedIn",'besmart')),
			"id" => "linkedin_color",
			"default" => "#006599",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Metacafe",'besmart')),
			"id" => "metacafe_color",
			"default" => "#f88326",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Netvibes",'besmart')),
			"id" => "netvibes_color",
			"default" => "#15ae15",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Paypal",'besmart')),
			"id" => "paypal_color",
			"default" => "#2c5f8c",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Picasa",'besmart')),
			"id" => "picasa_color",
			"default" => "#b163c9",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Pinterest",'besmart')),
			"id" => "pinterest_color",
			"default" => "#cb2028",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Reddit",'besmart')),
			"id" => "reddit_color",
			"default" => "#6bbffb",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Rss",'besmart')),
			"id" => "rss_color",
			"default" => "#ff6600",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Skype",'besmart')),
			"id" => "skype_color",
			"default" => "#00aff0",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("StumbleUpon",'besmart')),
			"id" => "stumbleupon_color",
			"default" => "#ea4b24",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Technorati",'besmart')),
			"id" => "technorati_color",
			"default" => "#00c400",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Tumblr",'besmart')),
			"id" => "tumblr_color",
			"default" => "#2c4661",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Twitter",'besmart')),
			"id" => "twitter_color",
			"default" => "#00acee",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Vimeo",'besmart')),
			"id" => "vimeo_color",
			"default" => "#17aacc",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Wordpress",'besmart')),
			"id" => "wordpress_color",
			"default" => "#207499",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Yahoo",'besmart')),
			"id" => "yahoo_color",
			"default" => "#65106b",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Yelp",'besmart')),
			"id" => "yelp_color",
			"default" => "#c51102",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Youtube",'besmart')),
			"id" => "youtube_color",
			"default" => "#d20200",
			"type" => "qodux_color"
		),
		array(
			"type" => "qodux_close"
		),
		array(
			"type" => "qodux_reset"
		),
	array(
		"type" => "qodux_group_end",
	),	
);
return array(
	'auto' => true,
	'name' => 'color',
	'options' => $qodux_options
);