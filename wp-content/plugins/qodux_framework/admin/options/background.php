<?php
$qodux_options = array(
	array(
		"class" => "nav-tab-wrapper",
		"default" => '',
		"options" => array(
			"page" => esc_html__('Homepage','besmart'),
			"header" => esc_html__('Header','besmart'),
			"footer" => esc_html__('Footer','besmart'),
		),
		"type" => "qodux_navigation",
	),
	
	array(
		"type" => "qodux_group_start",
		"group_id" => "page",
	),
		array(
			"name" => esc_html__("Background Type",'besmart'),
			"type" => "qodux_open",
		),
			array(
				"name" => esc_html__("",'besmart'),
				"one_col" => "true",
				"id" => "background_type",
				"default" => 'image_bg',
				"options" => array(
					"layerSlider" => esc_html__('Layer Slider Background','besmart'),
					"pattern" => esc_html__('Pattern Background','besmart'),
					"surface" => esc_html__('Surface Shader Background','besmart'),
					// "parallax" => esc_html__('Parallax Image Background','besmart'),
					"image_bg" => esc_html__('Image Background','besmart'),
					"slideshow" => esc_html__('Supersized Slideshow','besmart'),
					"video" => esc_html__('Video','besmart'),
				),
				"chosen" => "true",
				"type" => "qodux_select",
			),	
			
		array(
			"type" => "qodux_close"
		),
		array(
			"type" => "qodux_reset"
		),
			array(
				"open_class" => "layerSwitch",
				"type" => "qodux_open_group",
			),		
				array(
					"type" => "qodux_group_start",
					"group_id" => "layer_bg",
				),
					array(
						"name" => esc_html__("Layer Slider Background",'besmart'),
						"type" => "qodux_open"
					),		
						array(	
							"name" => esc_html__('Layer Slider', 'besmart'),
							"desc" => esc_html__("Select one of the Layer Sliders. The \"Layer Slider\" plugin should be installed and the sliders should be created / imported first.",'besmart'),
							"id" => "layerS_slideshow",
							"prompt" => esc_html__("Choose Layer Slider...",'besmart'),
							"type" => "qodux_selectLayerS"
					),	
					array(
						"type" => "qodux_close"
					),
					array(
						"type" => "qodux_reset"
					),
				array(
					"type" => "qodux_group_end",
				),
			array(
				"type" => "qodux_close_group"
			),
			array(
				"open_class" => "patternSwitch",
				"type" => "qodux_open_group",
			),		
				array(
					"type" => "qodux_group_start",
					"group_id" => "pattern_bg",
				),
					array(
						"name" => esc_html__("Pattern Background",'besmart'),
						"type" => "qodux_open"
					),		
						array(
							"name" => esc_html__("Pattern Background Image",'besmart'),
							"desc" => esc_html__("You can paste the full URL of the image including ",'besmart') . "<code>http://</code>" . esc_html__(", to be used as a background image, or you can simply upload it using the button.",'besmart'),
							"id" => "pattern_bg",
							"default" => "",
							"type" => "qodux_upload"
						),
						array(
							"name" => esc_html__("Pattern Background Position",'besmart'),
							"desc" => "Choose the background image position.",
							"id" => "pattern_position_x",
							"default" => 'center',
							"options" => array(
								"left" => esc_html__('Left','besmart'),
								"center" => esc_html__('Center','besmart'),
								"right" => esc_html__('Right','besmart'),
							),
							"type" => "qodux_select",
						),
						array(
							"name" => esc_html__("Pattern Background Repeat",'besmart'),
							"desc" => "Choose the background image repeat style.",
							"id" => "pattern_repeat",
							"default" => 'no-repeat',
							"options" => array(
								"no-repeat" => esc_html__('No Repeat','besmart'),
								"repeat" => esc_html__('Repeat','besmart'),
								"repeat-x" => esc_html__('Repeat Horizontally','besmart'),
								"repeat-y" => esc_html__('Repeat Vertically','besmart'),
							),
							"type" => "qodux_select",
						),
						array(
							"name" => esc_html__("Pattern Background Color",'besmart'),
							"desc" => esc_html__("Here you can choose a specific page background color. Set it to transparent in order to disable this.",'besmart'),
							"id" => "pattern_bg_color",
							"default" => "",
							"type" => "qodux_color"		
						),
					array(
						"type" => "qodux_close"
					),
					array(
						"type" => "qodux_reset"
					),
				array(
					"type" => "qodux_group_end",
				),
			array(
				"type" => "qodux_close_group"
			),	array(
				"open_class" => "surfaceSwitch",
				"type" => "qodux_open_group",
			),		
				array(
					"type" => "qodux_group_start",
					"group_id" => "surface_bg",
				),
					array(
						"name" => esc_html__("Surface Shader Settings",'besmart'),
						"type" => "qodux_open"
					),		
						array(
							"name" => esc_html__("Mesh ambient",'besmart'),
							"id" => "mesh_ambient",
							"default" => "#3694d2",
							"format" => "hex",
							"type" => "qodux_color"
						),
						array(
							"name" => esc_html__("Mesh diffuse",'besmart'),
							"id" => "mesh_diffuse",
							"default" => "#2980b9",
							"type" => "qodux_color"
						),
						
						array(
							"name" => esc_html__("Light ambient",'besmart'),
							"id" => "light_ambient",
							"default" => "#2c3e50",
							"type" => "qodux_color"
						),
						array(
							"name" => esc_html__("Light ambient",'besmart'),
							"id" => "light_diffuse",
							"default" => "#34495e",
							"type" => "qodux_color"
						),
					array(
						"type" => "qodux_close"
					),
					array(
						"type" => "qodux_reset"
					),
				array(
					"type" => "qodux_group_end",
				),
			array(
				"type" => "qodux_close_group"
			),
			array(
				"open_class" => "imageSwitch",
				"type" => "qodux_open_group",
			),		
				array(
					"type" => "qodux_group_start",
					"group_id" => "image_bg",
				),
					array(
						"name" => esc_html__("Image Background",'besmart'),
						"type" => "qodux_open"
					),		
						array(
							"name" => esc_html__("Image Image",'besmart'),
							"desc" => esc_html__("You can paste the full URL of the image including ",'besmart') . "<code>http://</code>" . esc_html__(", to be used as a background image, or you can simply upload it using the button.",'besmart'),
							"id" => "image_bg",
							"default" => "",
							"type" => "qodux_upload"
						),
						array(
							"name" => esc_html__("Background Image 'X' Position",'besmart'),
							"desc" => "Choose the background image 'X' position.",
							"id" => "image_position_x",
							"default" => 'center',
							"options" => array(
								"left" => esc_html__('Left','besmart'),
								"center" => esc_html__('Center','besmart'),
								"right" => esc_html__('Right','besmart'),
							),
							"type" => "qodux_select",
						),
						array(
							"name" => esc_html__("Background Image 'Y' Position",'besmart'),
							"desc" => "Choose the background image 'Y' position.",
							"id" => "image_position_y",
							"default" => 'top',
							"options" => array(
								"top" => esc_html__('Top','besmart'),
								"center" => esc_html__('Center','besmart'),
								"bottom" => esc_html__('Bottom','besmart'),
							),
							"type" => "qodux_select",
						),
					array(
						"type" => "qodux_close"
					),
					array(
						"type" => "qodux_reset"
					),
				array(
					"type" => "qodux_group_end",
				),
			array(
				"type" => "qodux_close_group"
			),
			array(
				"open_class" => "parallaxSwitch",
				"type" => "qodux_open_group",
			),		
				array(
					"type" => "qodux_group_start",
					"group_id" => "parallax_bg",
				),
					array(
						"name" => esc_html__("Parallax Background",'besmart'),
						"type" => "qodux_open"
					),		
						array(
							"name" => esc_html__("Parallax Image",'besmart'),
							"desc" => esc_html__("You can paste the full URL of the image including ",'besmart') . "<code>http://</code>" . esc_html__(", to be used as a background image, or you can simply upload it using the button.",'besmart'),
							"id" => "parallax_bg",
							"default" => "",
							"type" => "qodux_upload"
						),
					array(
						"type" => "qodux_close"
					),
					array(
						"type" => "qodux_reset"
					),
				array(
					"type" => "qodux_group_end",
				),
			array(
				"type" => "qodux_close_group"
			),
			array(
				"open_class" => "slideshowSwitch",
				"type" => "qodux_open_group",
			),		
				array(
					"type" => "qodux_group_start",
					"group_id" => "slideshow_bg",
				),
					array(
						"name" => esc_html__("Slideshow Background",'besmart'),
						"type" => "qodux_open"
					),		
						array(
							"name" => esc_html__("Slideshow Background Image 1",'besmart'),
							"desc" => esc_html__("You can paste the full URL of the image including ",'besmart') . "<code>http://</code>" . esc_html__(", to be used as a background image, or you can simply upload it using the button.",'besmart'),
							"id" => "slide_bg_1",
							"default" => "",
							"type" => "qodux_upload"
						),
						array(
							"name" => esc_html__("Slideshow Background Image 2",'besmart'),
							"desc" => esc_html__("You can paste the full URL of the image including ",'besmart') . "<code>http://</code>" . esc_html__(", to be used as a background image, or you can simply upload it using the button.",'besmart'),
							"id" => "slide_bg_2",
							"default" => "",
							"type" => "qodux_upload"
						),
						array(
							"name" => esc_html__("Slideshow Background Image 3",'besmart'),
							"desc" => esc_html__("You can paste the full URL of the image including ",'besmart') . "<code>http://</code>" . esc_html__(", to be used as a background image, or you can simply upload it using the button.",'besmart'),
							"id" => "slide_bg_3",
							"default" => "",
							"type" => "qodux_upload"
						),
						array(
							"name" => esc_html__("Slideshow Background Image 4",'besmart'),
							"desc" => esc_html__("You can paste the full URL of the image including ",'besmart') . "<code>http://</code>" . esc_html__(", to be used as a background image, or you can simply upload it using the button.",'besmart'),
							"id" => "slide_bg_4",
							"default" => "",
							"type" => "qodux_upload"
						),
						array(
							"name" => esc_html__("Slideshow Background Image 5",'besmart'),
							"desc" => esc_html__("You can paste the full URL of the image including ",'besmart') . "<code>http://</code>" . esc_html__(", to be used as a background image, or you can simply upload it using the button.",'besmart'),
							"id" => "slide_bg_5",
							"default" => "",
							"type" => "qodux_upload"
						),
					array(
						"type" => "qodux_close"
					),
					array(
						"type" => "qodux_reset"
					),
				array(
					"type" => "qodux_group_end",
				),
			array(
				"type" => "qodux_close_group"
			),	
			array(
				"open_class" => "videoSwitch",
				"type" => "qodux_open_group",
			),		
				array(
					"type" => "qodux_group_start",
					"group_id" => "video_bg",
				),
					array(
						"name" => esc_html__("Video Background",'besmart'),
						"type" => "qodux_open"
					),		
						array(
							"name" => esc_html__("Video Link",'besmart'),
							"desc" => esc_html__("You need to paste the full URL including ",'besmart') . "<code>http://</code>" . esc_html__(", of the video to be used as a background video. Only 'YouTube' accepted.",'besmart'),
							"id" => "video_link",
							"default" => "",
							"type" => "qodux_text"
						),
						array(
							"name" => esc_html__("Background Mobile",'besmart'),
							"desc" => esc_html__("You can paste the full URL of the image including ",'besmart') . "<code>http://</code>" . esc_html__(", to be used as a background image, or you can simply upload it using the button.",'besmart'),
							"id" => "video_mobile_bg",
							"default" => "",
							"type" => "qodux_upload"
						),
					array(
						"type" => "qodux_close"
					),
					array(
						"type" => "qodux_reset"
					),
				array(
					"type" => "qodux_group_end",
				),
			array(
				"type" => "qodux_close_group"
			),						
		array(
			"type" => "qodux_group_end",
		),
		
	array(
		"type" => "qodux_group_start",
		"group_id" => "header",
	),
		array(
			"name" => esc_html__("Header Background",'besmart'),
			"type" => "qodux_open"
		),
		array(
			"name" => esc_html__("Header Background Image",'besmart'),
			"desc" => esc_html__("You can paste the full URL of the image including ",'besmart') . "<code>http://</code>" . esc_html__(", to be used as a background image, or you can simply upload it using the button.",'besmart'),
			"id" => "header_bg",
			"default" => "",
			"type" => "qodux_upload"
		),
		array(
			"name" => esc_html__("Header Background Position",'besmart'),
			"desc" => "Choose the background image position.",
			"id" => "header_position_x",
			"default" => 'center',
			"options" => array(
				"left" => esc_html__('Left','besmart'),
				"center" => esc_html__('Center','besmart'),
				"right" => esc_html__('Right','besmart'),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html__("Header Background Repeat",'besmart'),
			"desc" => "Choose the background image repeat style.",
			"id" => "header_repeat",
			"default" => 'no-repeat',
			"options" => array(
				"no-repeat" => esc_html__('No Repeat','besmart'),
				"repeat" => esc_html__('Repeat','besmart'),
				"repeat-x" => esc_html__('Repeat Horizontally','besmart'),
				"repeat-y" => esc_html__('Repeat Vertically','besmart'),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html__("Header Background Color",'besmart'),
			"desc" => esc_html__("Here you can choose a specific page background color. Set it to transparent in order to disable this.",'besmart'),
			"id" => "header_bg_color",
			"default" => "",
			"type" => "qodux_color"		
		),
		array(
			"type" => "qodux_close"
		),
		array(
			"type" => "qodux_reset"
		),
	array(
		"type" => "qodux_group_end",
	),	
	
	array(
		"type" => "qodux_group_start",
		"group_id" => "footer",
	),
	
		array(
			"name" => esc_html__("Footer Top Background",'besmart'),
			"type" => "qodux_open"
		),
		array(
			"name" => esc_html__("Custom Footer Top Image",'besmart'),
			"desc" => esc_html__("You can paste the full URL of the image including ",'besmart') . "<code>http://</code>" . esc_html__(", to be used as a background image, or you can simply upload it using the button.",'besmart'),
			"id" => "footer_top_bg",
			"default" => "",
			"type" => "qodux_upload"
		),
		array(
			"name" => esc_html__("Footer Top Position",'besmart'),
			"desc" => "Choose the background image position.",
			"id" => "footer_top_position_x",
			"default" => 'center',
			"options" => array(
				"left" => esc_html__('Left','besmart'),
				"center" => esc_html__('Center','besmart'),
				"right" => esc_html__('Right','besmart'),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html__("Footer Top Repeat",'besmart'),
			"desc" => "Choose the background image repeat style.",
			"id" => "footer_top_repeat",
			"default" => 'no-repeat',
			"options" => array(
				"no-repeat" => esc_html__('No Repeat','besmart'),
				"repeat" => esc_html__('Repeat','besmart'),
				"repeat-x" => esc_html__('Repeat Horizontally','besmart'),
				"repeat-y" => esc_html__('Repeat Vertically','besmart'),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html__("Footer Top Background Color",'besmart'),
			"desc" => esc_html__("If you specify a color below, this option will override the global configuration. Set it to transparent in order to disable this.",'besmart'),
			"id" => "footer_top_color",
			"default" => "",
			"type" => "qodux_color"		
		),

		array(
			"type" => "qodux_close"
		),
		array(
			"name" => esc_html__("Footer Background",'besmart'),
			"type" => "qodux_open"
		),
		array(
			"name" => esc_html__("Custom Footer Image",'besmart'),
			"desc" => esc_html__("You can paste the full URL of the image including ",'besmart') . "<code>http://</code>" . esc_html__(", to be used as a background image, or you can simply upload it using the button.",'besmart'),
			"id" => "footer_bg",
			"default" => "",
			"type" => "qodux_upload"
		),
		array(
			"name" => esc_html__("Footer Position",'besmart'),
			"desc" => "Choose the background image position.",
			"id" => "footer_position_x",
			"default" => 'center',
			"options" => array(
				"left" => esc_html__('Left','besmart'),
				"center" => esc_html__('Center','besmart'),
				"right" => esc_html__('Right','besmart'),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html__("Footer Repeat",'besmart'),
			"desc" => "Choose the background image repeat style.",
			"id" => "footer_repeat",
			"default" => 'no-repeat',
			"options" => array(
				"no-repeat" => esc_html__('No Repeat','besmart'),
				"repeat" => esc_html__('Repeat','besmart'),
				"repeat-x" => esc_html__('Repeat Horizontally','besmart'),
				"repeat-y" => esc_html__('Repeat Vertically','besmart'),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html__("Footer Background Color",'besmart'),
			"desc" => esc_html__("If you specify a color below, this option will override the global configuration. Set it to transparent in order to disable this.",'besmart'),
			"id" => "footer_color",
			"default" => "",
			"type" => "qodux_color"		
		),
		
		array(
			"type" => "qodux_close"
		),
		array(
			"name" => esc_html__("Footer Bottom Background",'besmart'),
			"type" => "qodux_open"
		),
		array(
			"name" => esc_html__("Custom Footer Bottom Image",'besmart'),
			"desc" => esc_html__("You can paste the full URL of the image including ",'besmart') . "<code>http://</code>" . esc_html__(", to be used as a background image, or you can simply upload it using the button.",'besmart'),
			"id" => "footer_bottom_bg",
			"default" => "",
			"type" => "qodux_upload"
		),
		array(
			"name" => esc_html__("Footer Bottom Position",'besmart'),
			"desc" => "Choose the background image position.",
			"id" => "footer_bottom_position_x",
			"default" => 'center',
			"options" => array(
				"left" => esc_html__('Left','besmart'),
				"center" => esc_html__('Center','besmart'),
				"right" => esc_html__('Right','besmart'),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html__("Footer Bottom Repeat",'besmart'),
			"desc" => "Choose the background image repeat style.",
			"id" => "footer_bottom_repeat",
			"default" => 'no-repeat',
			"options" => array(
				"no-repeat" => esc_html__('No Repeat','besmart'),
				"repeat" => esc_html__('Repeat','besmart'),
				"repeat-x" => esc_html__('Repeat Horizontally','besmart'),
				"repeat-y" => esc_html__('Repeat Vertically','besmart'),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html__("Footer Bottom Background Color",'besmart'),
			"desc" => esc_html__("If you specify a color below, this option will override the global configuration. Set it to transparent in order to disable this.",'besmart'),
			"id" => "footer_bottom_color",
			"default" => "",
			"type" => "qodux_color"		
		),
		array(
			"type" => "qodux_close"
		),
		array(
			"type" => "qodux_reset"
		),
	array(
		"type" => "qodux_group_end",
	),	
);
return array(
	'auto' => true,
	'name' => 'background',
	'options' => $qodux_options
);