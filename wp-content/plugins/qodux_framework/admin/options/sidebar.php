<?php
if (! function_exists("wt_add_sidebar_option")) {
	function wt_add_sidebar_option($value, $default) {
		if(!empty($default)){
			$sidebars = explode(',',$default);
		}else{
			$sidebars = array();
		}
		
		echo '<input type="text" id="add_sidebar" name="add_sidebar" pattern="([a-zA-Z\x7f-\xff][ a-zA-Z0-9_\x7f-\xff]*){0,1}" data-message="'.esc_html(__('Please input a valid name which starts with a letter, followed by letters, numbers, spaces, or underscores.','besmart')).'" maxlength="20" /><span class="validator-error"></span>';
		if(!empty($sidebars)){
			echo '<div class="sidebar-title">'.esc_html(__('Below are the Custom Sidebars you\'ve generated','besmart')).'</div>';
			foreach($sidebars as $sidebar){
				echo '<div class="sidebar-item"><span>'.$sidebar.'</span><input type="hidden" class="sidebar-item-value" value="'.$sidebar.'"/><input type="button" class="button" value="'.esc_html(__('Delete','besmart')).'"/></div>';
			}
		}
		echo '<input type="hidden" value="' . $default . '" name="' . $value['id'] . '" id="sidebars"/>';
	}
}
$qodux_options = array(
	array(
		"name" => esc_html(__("Sidebar",'besmart')),
		"type" => "besmart_title"
	),
	array(
		"name" => esc_html(__("Sidebar",'besmart')),
		"type" => "qodux_open"
	),
		array(
			"name" => esc_html(__("Generate Custom Sidebar",'besmart')),
			"desc" => esc_html(__("Enter the sidebar name you'd like to create.",'besmart')),
			"id" => "sidebars",
			"function" => "wt_add_sidebar_option",
			"default" => "",
			"type" => "besmart_custom"
		),
	array(
		"type" => "qodux_close"
	),
	array(
		"type" => "qodux_reset"
	),
);
return array(
	'auto' => true,
	'name' => 'sidebar',
	'options' => $qodux_options
);