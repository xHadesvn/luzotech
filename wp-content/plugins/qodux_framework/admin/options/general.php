<?php
$qodux_options = array(
	array(
		"class" => "nav-tab-wrapper",
		"default" => '',
		"options" => array(
			"general_settings" => esc_html(__('General','besmart')),
			"homepage_settings" => esc_html(__('Homepage','besmart')),
			"custom_favicons" => esc_html(__('Custom Favicons','besmart')),
			"custom_stylesheet" => esc_html(__('Custom Css','besmart')),
		),
		"type" => "qodux_navigation",
	),	
	array(
		"type" => "qodux_group_start",
		"group_id" => "general_settings",
	),
		array(
			"name" => esc_html(__("General Settings",'besmart')),
			"type" => "qodux_open"
		),		
		array(
			"name" => esc_html(__("Enable Responsive",'besmart')),
			"desc" => sprintf( esc_html(__('Set ON to enable responsive mode.','besmart'))),
			"id" => "enable_responsive",
			"default" => true,
			"type" => "qodux_toggle"
		),	
		array(
			"name" => esc_html(__("Custom Logo",'besmart')),
			"desc" => esc_html(__( "Enter the full URL of your logo image: e.g http://www.site.com/logo.png",'besmart')),
			"id" => "logo",
			"default" =>  "",
			"type" => "qodux_upload",
			"crop" => "false"
		),
		array(
			"name" => esc_html(__("Custom Logo High-DPI (retina) ",'besmart')),
			"desc" => esc_html(__( "Enter the full URL of your logo image: e.g http://www.site.com/logo@2x.png",'besmart')),
			"id" => "logo_retina",
			"default" =>  "",
			"type" => "qodux_upload",
			"crop" => "false"
		),
		array(
			"name" => esc_html(__("Display Site Description",'besmart')),
			"desc" => sprintf( esc_html(__('This enables site description. Works only with plain text logo.','besmart')),get_option('siteurl')),
			"id" => "display_site_desc",
			"default" => false,
			"type" => "qodux_toggle"
		),
		array(
			"name" => esc_html(__("Display Menu on Frontpage",'besmart')),
			"desc"=> esc_html(__("This option disables your website's navigation",'besmart')),
			"id" => "show_menu",
			"default" => true,
			"type" => "qodux_toggle"
		),	
		array(
			"name" => esc_html(__("Website style: onepage or multipage",'besmart')),
			"desc" => "Here you can set what type of style do you want: onepage or multipage.",
			"id" => "qodux_style",
			"default" => 'onepage',
			"options" => array(
				"onepage" => esc_html(__('Onepage style','besmart')),
				"multipage" => esc_html(__('Multipage style','besmart')),
			),
			"type" => "qodux_select",
		),	
		array(
			"name" => esc_html__("Custom Skin",'besmart'),
			"desc" => esc_html__("Create your own skin. This option creates skins which affects only colors, background colors and border colors. Unfortunatelly for images/background images doesn't work. So you need to edit the images with your own color skin and paste them in 'img' folder from theme root with the same names as the older ones. You can keep the older ones under different names.", 'besmart') . "<br><code>" . esc_html__("Please use the HEX format here. Ex: \"#000000\"", 'besmart') . "</code>",
			"id" => "custom_skin",
			"default" => "",
			"format" => "hex",
			"type" => "qodux_color"
		),
		array(
			"name" => esc_html(__("Disable Breadcrumbs",'besmart')),
			"desc" => esc_html(__("This option disables your website's breadcrumb navigation.",'besmart')),
			"id" => "disable_breadcrumb",
			"default" => true,
			"type" => "qodux_toggle"
		),		
		array(
			"name" => esc_html(__("Sticky Header",'besmart')),
			"desc" => esc_html(__("This option enables the sticky header when scrolling down.",'besmart')),
			"id" => "sticky_header",
			"default" => true,
			"type" => "qodux_toggle"
		),			
		array(
			"name" => esc_html(__("Disable Sticky Header On Smaller Screens",'besmart')),
			"desc" => esc_html(__("This option disables sticky header on smaller screens.",'besmart')),
			"id" => "no_sticky_on_ss",
			"default" => false,
			"type" => "qodux_toggle"
		),
		array(
			"name" => esc_html(__("Show Responsive Navigation under:",'besmart')),
			"desc" => "Here you can set when (which window size) the responsive navigation should be displayed.",
			"id" => "responsive_nav",
			"default" => '767',
			"options" => array(
				"991" => esc_html(__('< 991 px','besmart')),
				"767" => esc_html(__('< 767 px','besmart')),
				//"480" => esc_html(__('< 480 px','besmart')),
			),
			"type" => "qodux_select",
		),	
		array(
			"name" => esc_html(__("Smooth Scrolling",'besmart')),
			"desc" => sprintf( esc_html(__('Set ON to enable smooth scroll (a Google Chrome extension for smooth scrolling with the mouse wheel and keyboard buttons). This disables the above Nice Scroll option.', 'besmart' ))),
			"id" => "smooth_scroll",
			"default" => false,
			"type" => "qodux_toggle"
		),		
		array(
			"name" => esc_html(__("Page Loader Animation",'besmart')),
			"desc" => esc_html(__("This option enables the page loader animation.",'besmart')),
			"id" => "page_loader",
			"default" => false,
			"type" => "qodux_toggle"
		),
		array(
			"name" => esc_html(__("Scroll to Top",'besmart')),
			"desc" => esc_html(__("This option enables a scroll to top button at the right bottom corner of site pages.",'besmart')),
			"id" => "scroll_to_top",
			"default" => false,
			"type" => "qodux_toggle"
		),
		array(
			"name" => esc_html(__("WooCommerce",'besmart')),
			"desc"=> esc_html(__('Set ON if you want to use woocommerce.','besmart')),
			"id" => "woocommerce",
			"default" => false,
			"type" => "qodux_toggle"
		),
		array(
			"name" => esc_html(__("Shop page layout",'besmart')),
			"desc" => esc_html(__("Select which layout do you want for your Shop page.",'besmart')),
			"id" => "woo_layout",
			"default" => 'right',
			"options" => array(
				"full" => esc_html(__('Full Layout','besmart')),
				"right" => esc_html(__('Right Sidebar','besmart')),
				"left" => esc_html(__('Left Sidebar','besmart')),
			),
			"chosen" => "true",
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Enable Animations",'besmart')),
			"desc" => esc_html(__("This option enables site animations.",'besmart')),
			"id" => "enable_animation",
			"default" => false,
			"type" => "qodux_toggle"
		),

		array(
			"name" => esc_html(__("High-DPI (retina) images",'besmart')),
			"desc" => esc_html(__("This option allows you to use High-DPI (retina) images.",'besmart')),
			"id" => "enable_retina",
			"default" => false,
			"type" => "qodux_toggle"
		),
		array(
			"type" => "qodux_close"
		),	
		array(
			"type" => "qodux_reset"
		),
	array(
		"type" => "qodux_group_end",
	),	
	array(
		"type" => "qodux_group_start",
		"group_id" => "homepage_settings",
	),	
		array(
			"name" => esc_html(__("Homepage Settings",'besmart')),
			"type" => "qodux_open"
		),
		array(
			"name" => esc_html(__("Home Page",'besmart')),
			"desc" => esc_html(__("The selected page here will be displayed in the homepage.",'besmart')),
			"id" => "home_page",
			"page" => 0,
			"default" => 0,
			"prompt" => esc_html(__("None",'besmart')),
			"chosen" => "true",
			"type" => "qodux_select",
			),					
		array(
			"name" => esc_html(__("Home Section Overlay Type",'besmart')),
			"desc" => esc_html(__("Select an overlay type to use into home section.",'besmart')),
			"id" => "overlay_type",
			"default" => 'dark',
			"options" => array(
				"none" => esc_html(__('None','besmart')),
				"pattern" => esc_html(__('Pattern','besmart')),
				"color" => esc_html(__('Color','besmart')),
				"dark" => esc_html(__('Dark','besmart')),
			),
			"chosen" => "true",
			"type" => "qodux_select",
		),
		array(
			"type" => "qodux_close"
		),
		array(
			"type" => "qodux_reset"
		),
	array(
		"type" => "qodux_group_end",
	),	
	array(
		"type" => "qodux_group_start",
		"group_id" => "custom_favicons",
	),	
		array(
			"name" => esc_html(__("Favicons",'besmart')),
			"type" => "qodux_open"
		),					
			array(	
				"name" => esc_html(__("Favicon", 'besmart')),
				"desc" => esc_html(__("Enter the full URL of your favicon e.g. http://www.site.com/favicon.ico", 'besmart')),
				"id" => "favicon",
				"default" => '',
				"type" => "qodux_upload",
				"crop" => "false"
			),			
			array(	
				"name" => esc_html(__("Apple Touch Icon 57x57", 'besmart')),
				"desc" => esc_html(__("Enter the full URL of your favicon e.g. http://www.site.com/favicon_57.png", 'besmart')),
				"id" => "favicon_57",
				"default" => '',
				"type" => "qodux_upload",
				"crop" => "false"
			),		
			array(	
				"name" => esc_html(__("Apple Touch Icon 72x72", 'besmart')),
				"desc" => esc_html(__("Enter the full URL of your favicon e.g. http://www.site.com/favicon_72.png", 'besmart')),
				"id" => "favicon_72",
				"default" => '',
				"type" => "qodux_upload",
				"crop" => "false"
			),		
			array(	
				"name" => esc_html(__("Apple Touch Icon 114x114", 'besmart')),
				"desc" => esc_html(__("Enter the full URL of your favicon e.g. http://www.site.com/favicon_114.png", 'besmart')),
				"id" => "favicon_114",
				"default" => '',
				"type" => "qodux_upload",
				"crop" => "false"
			),	
			array(	
				"name" => esc_html(__("Apple Touch Icon 144x144", 'besmart')),
				"desc" => esc_html(__("Enter the full URL of your favicon e.g. http://www.site.com/favicon_144.png", 'besmart')),
				"id" => "favicon_144",
				"default" => '',
				"type" => "qodux_upload",
				"crop" => "false"
			),
		array(
			"type" => "qodux_close"
		),
		array(
			"type" => "qodux_reset"
		),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "custom_stylesheet",
	),	
		array(
			"name" => esc_html(__("Custom Css",'besmart')),
			"type" => "qodux_open"
		),			
			array(	
				"name" => esc_html(__("Custom Css", 'besmart')),
				"id" => "custom_css",
				"default" => "",
				"elastic" => "true",
				"type" => "qodux_textarea"
			),
		array(
			"type" => "qodux_close"
		),
		array(
			"type" => "qodux_reset"
		),
	array(
		"type" => "qodux_group_end",
	),	
);
return array(
	'auto' => true,
	'name' => 'general',
	'options' => $qodux_options
);