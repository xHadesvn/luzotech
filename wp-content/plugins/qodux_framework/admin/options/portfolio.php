<?php
$qodux_options = array(
	array(
		"class" => "nav-tab-wrapper",
		"default" => '',
		"options" => array(
			"single_portfolio_settings" => esc_html(__('Single Portfolio','besmart')),
			"featured_entry_settings" => esc_html(__('Featured Entry','besmart')),
		),
		"type" => "qodux_navigation",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "single_portfolio_settings",
	),
		array(
			"name" => esc_html(__("Single Portfolio Item",'besmart')),
			"type" => "qodux_open"
		),
			array(
				"name" => esc_html(__("Layout",'besmart')),
				"desc" => "Choose the layout of the Single Portfolio Item.",
				"id" => "layout",
				"default" => 'right',
				"options" => array(
					"full" => esc_html(__('Full Width','besmart')),
					"right" => esc_html(__('Right Sidebar','besmart')),
					"left" => esc_html(__('Left Sidebar','besmart')),
				),
				"type" => "qodux_select",
			),
			array(
				"name" => esc_html(__("Previous & Next Navigation",'besmart')),
				"desc" => "Displays the Previous & Next Navigation.",
				"id" => "single_navigation",
				"default" => false,
				"type" => "qodux_toggle"
			),
			array(
				"name" => esc_html(__("Previous & Next Navigation Order",'besmart')),
				"desc" => "The style you want to order Previous & Next Navigation.",
				"id" => "single_navigation_order",
				"default" => 'post_data',
				"options" => array(
					"post_data" => esc_html(__('Post Data','besmart')),
					"menu_order" => esc_html(__('Menu Order','besmart')),
				),
				"type" => "qodux_select",
			),
			array(
				"name" => esc_html(__("Document Type Navigation",'besmart')),
				"desc" => "If the button is set to ON then Previous & Next Navigation will be applied just for Portfolio Document Type.",
				"id" => "single_doc_navigation",
				"default" => true,
				"type" => "qodux_toggle"
			),			
			array(
				"name" => esc_html(__("Enable Comments",'besmart')),
				"desc" => "If the button is set to ON then you enable comments on the Single Portfolio Item.",
				"id" => "enable_comment",
				"default" => false,
				"type" => "qodux_toggle"
			),
		array(
			"type" => "qodux_close"
		),
		array(
			"type" => "qodux_reset"
		),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "featured_entry_settings",
	),
		array(
			"name" => esc_html(__("Featured Entry",'besmart')),
			"type" => "qodux_open"
		),
			array(
				"name" => esc_html(__("Featured Image",'besmart')),
				"desc" => esc_html(__("If the button is set to ON then the Featured Image will be diplayed in Portfolio Item page.",'besmart')),
				"id" => "featured_image",
				"default" => true,
				"type" => "qodux_toggle"
			),
			array(
				"name" => esc_html(__("Featured Image for Lightbox",'besmart')),
				"desc" => esc_html(__("If the button is set to ON then when you click on the Featured Image from Portfolio Item page, the full image will be opened in a lightbox.",'besmart')),
				"id" => "featured_image_lightbox",
				"default" => false,
				"type" => "qodux_toggle"
			),
			array(
				"name" => esc_html(__("Adaptive Height",'besmart')),
				"desc" => esc_html(__("If the button is set to ON then the Featured Image height depends on the original image.",'besmart')),
				"id" => "adaptive_height",
				"default" => false,
				"type" => "qodux_toggle"
			),
			array(
				"name" => esc_html(__("Fixed Height",'besmart')),
				"desc" => esc_html(__("You can set a fixed height for the Featured Image only if the option above is OFF.",'besmart')),
				"id" => "fixed_height",
				"min" => "1",
				"max" => "1000",
				"step" => "1",
				"unit" => 'px',
				"default" => "530",
				"type" => "qodux_range"
			),
		array(
			"type" => "qodux_close"
		),
		array(
			"type" => "qodux_reset"
		),
	array(
		"type" => "qodux_group_end",
	),
);
return array(
	'auto' => true,
	'name' => 'portfolio',
	'options' => $qodux_options
);