<?php
if (! function_exists("qodux_footer_column_option")) {
	function qodux_footer_column_option($value, $default) {
		wp_enqueue_script( 'footer-column');
		echo '<div class="theme-footer-columns">';
		echo '<a href="#" rel="1"><img src="' . get_template_directory_uri() . '/framework/admin/assets/images/footer_column/1.png" /></a>';
		echo '<a href="#" rel="2"><img src="' . get_template_directory_uri() . '/framework/admin/assets/images/footer_column/2.png" /></a>';
		echo '<a href="#" rel="3"><img src="' . get_template_directory_uri() . '/framework/admin/assets/images/footer_column/3.png" /></a>';
		echo '<a href="#" rel="4"><img src="' . get_template_directory_uri() . '/framework/admin/assets/images/footer_column/4.png" /></a>';
		echo '<a href="#" rel="6"><img src="' . get_template_directory_uri() . '/framework/admin/assets/images/footer_column/5.png" /></a>';
		echo '<a href="#" rel="col-9-3">
			<img src="' . get_template_directory_uri() . '/framework/admin/assets/images/footer_column/three_fourth_one_fourth.png" /></a>';
		echo '<a href="#" rel="col-3-9">
			<img src="' . get_template_directory_uri() . '/framework/admin/assets/images/footer_column/one_fourth_three_fourth.png" /></a>';
		echo '<a href="#" rel="col-2-5-5">
			<img src="' . get_template_directory_uri() . '/framework/admin/assets/images/footer_column/one_fifth_two_fifth_two_fifth.png" /></a>';
		echo '<a href="#" rel="col-3-3-6">
			<img src="' . get_template_directory_uri() . '/framework/admin/assets/images/footer_column/one_fourth_one_fourth_one_half.png" /></a>';
		echo '<a href="#" rel="col-6-3-3">
			<img src="' . get_template_directory_uri() . '/framework/admin/assets/images/footer_column/one_half_one_fourth_one_fourth.png" /></a>';
		echo '<a href="#" rel="col-3-6-3">
			<img src="' . get_template_directory_uri() . '/framework/admin/assets/images/footer_column/one_fourth_one_half_one_fourth.png" /></a>';
		echo '<a href="#" rel="col-4-8">
			<img src="' . get_template_directory_uri() . '/framework/admin/assets/images/footer_column/one_third_two_third.png" /></a>';
		echo '<a href="#" rel="col-8-4">
			<img src="' . get_template_directory_uri() . '/framework/admin/assets/images/footer_column/three_fifth_two_fifth.png" /></a>';
		echo '<a href="#" rel="col-5-7">
			<img src="' . get_template_directory_uri() . '/framework/admin/assets/images/footer_column/two_fifth_three_fifth.png" /></a>';
		echo '<a href="#" rel="col-5-5-2">
			<img src="' . get_template_directory_uri() . '/framework/admin/assets/images/footer_column/two_fifth_two_fifth_one_fifth.png" /></a>';
		echo '<input type="hidden" value="' . $default . '" name="' . $value['id'] . '" id="' . $value['id'] . '"/>';
		echo '</div>';
	}
}
$qodux_options = array(
	array(
		"name" => esc_html(__("Footer",'besmart')),
		"type" => "besmart_title"
	),
	array(
		"name" => esc_html(__("Footer",'besmart')),
		"type" => "qodux_open"
	),
		array(
			"name" => esc_html(__("Footer Top",'besmart')),
			"desc" => esc_html(__("If the button is set to OFF then the footer top area won't be displayed.",'besmart')),
			"id" => "footer_top",
			"default" => false,
			"type" => "qodux_toggle"
		),
		array(
			"name" => esc_html(__("Footer",'besmart')),
			"desc" => esc_html(__("If the button is set to OFF then the footer area won't be displayed.",'besmart')),
			"id" => "footer",
			"default" => true,
			"type" => "qodux_toggle"
		),
		array(
			"name" => esc_html(__("Footer Bottom",'besmart')),
			"desc" => esc_html(__("If the button is set to OFF then the sub footer area won't be displayed.",'besmart')),
			"id" => "sub_footer",
			"default" => false,
			"type" => "qodux_toggle"
		),
		array(
			"name" => esc_html(__("Footer Column Layout",'besmart')),
			"desc" => esc_html(__("Choose the footer column layout.",'besmart')),
			"id" => "column",
			"function" => "qodux_footer_column_option",
			"default" => "3",
			"type" => "besmart_custom"
		),
		array(
			"name" => esc_html(__("Copyright Text",'besmart')),
			"desc" => esc_html(__("Here you can enter the copyright text which is displayed in the sub footer.",'besmart')),
			"id" => "copyright",
			"default" => "Copyright &copy; 2015 Spectrum.",
			"rows" => 4,
			"type" => "qodux_textarea"
		),
	array(
		"type" => "qodux_close"
	),
	array(
		"type" => "qodux_reset"
	),
);
return array(
	'auto' => true,
	'name' => 'footer',
	'options' => $qodux_options
);