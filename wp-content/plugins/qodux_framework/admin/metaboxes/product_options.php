<?php
$config = array(
	'title' => sprintf( esc_html(__('%s Product Hover','besmart')),QODUX_FRAME_NAME),
	'id' => 'wt_product_hover',
	'pages' => array('product'),
	'callback' => '',
	'context' => 'side',
	'priority' => 'low',
);
$options = array(
	array(
		"name" => esc_html(__("Hover effect on Overview Pages",'besmart')),
		"desc" => "Display a hover effect on overview pages and replace the default featured thumbnail with the first image of the gallery?",
		"id" => "_product_hover",
		"default" => 'no_hover_active',
		"options" => array(
			"hover_active" => esc_html(__('Yes - show first gallery image on hover','besmart')),
			"no_hover_active" => esc_html(__('No hover effect','besmart')),
		),
		"type" => "qodux_select",
	),
);
new qodux_metaboxes($config,$options);