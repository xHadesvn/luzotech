<?php
$config = array(
	'title' => sprintf( esc_html(__('%s Featured Video','besmart')),QODUX_FRAME_NAME),
	'id' => 'featured_video',
	'pages' => array('page', 'post', 'portfolio'),
	'callback' => '',
	'context' => 'side',
	'priority' => 'default',
);
$options = array(
	array(
		"name" => esc_html(__("Paste video link below:",'besmart')),
		"desc" => esc_html(__("Accepted videos: YouTube, Vimeo, Daylimotion, Metacafe",'besmart')),
		"id" => "_featured_video",
		"class" => "large_width featured_video",
		"default" => "",
		"type" => "qodux_featured_video"
	),	
);
new qodux_metaboxes($config,$options);