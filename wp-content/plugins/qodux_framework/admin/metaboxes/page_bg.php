<?php
$config = array(
	'title' => sprintf( esc_html(__('Page Bg Options','besmart')),QODUX_FRAME_NAME),
	'id' => 'page_bg',
	'pages' => array('page','post'),
	'callback' => '',
	'context' => 'normal',
	'priority' => 'low',
);
$options = array(
	array(
		"class" => "nav-tab-wrapper",
		"default" => '',
		"options" => array(
			"header" => esc_html(__('Header','besmart')),
			"breadcrumbs" => esc_html(__('Breadcrumbs','besmart')),
			"page" => esc_html(__('Page','besmart')),
			"footer" => esc_html(__('Footer','besmart')),
		),
		"type" => "qodux_navigation",
	),	
	array(
		"type" => "qodux_option_group_start",
		"group_id" => "page",
	),
		array(
			"name" => esc_html(__("Page Background Image",'besmart')),
			"desc" => esc_html(__("You can paste the full URL of the image (including ",'besmart')) . "<code>http://</code>" . esc_html(__("), to be used as a background image, or you can simply upload it using the button.",'besmart')),
			"id" => "_page_bg",
			"default" => "",
			"type" => "qodux_upload"
		),
		array(
			"name" => esc_html(__("Page Background Position",'besmart')),
			"desc" => "Choose the background image position.",
			"id" => "_page_position_x",
			"default" => 'center',
			"options" => array(
				"left" => esc_html(__('Left','besmart')),
				"center" => esc_html(__('Center','besmart')),
				"right" => esc_html(__('Right','besmart')),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Page Background Repeat",'besmart')),
			"desc" => "Choose the background image repeat style.",
			"id" => "_page_repeat",
			"default" => 'no-repeat',
			"options" => array(
				"no-repeat" => esc_html(__('No Repeat','besmart')),
				"repeat" => esc_html(__('Repeat','besmart')),
				"repeat-x" => esc_html(__('Repeat Horizontally','besmart')),
				"repeat-y" => esc_html(__('Repeat Vertically','besmart')),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Page Background Color",'besmart')),
			"desc" => esc_html(__("Here you can choose a specific page background color. Set it to transparent in order to disable this.",'besmart')),
			"id" => "_page_bg_color",
			"default" => "",
			"type" => "qodux_color"		
		),
	array(
		"type" => "qodux_group_end",
	),
	
	array(
		"type" => "qodux_option_group_start",
		"group_id" => "header",
	),
		array(
			"name" => esc_html(__("Header Background Image",'besmart')),
			"desc" => esc_html(__("You can paste the full URL of the image (including ",'besmart')) . "<code>http://</code>" . esc_html(__("), to be used as a background image, or you can simply upload it using the button.",'besmart')),
			"id" => "_header_bg",
			"default" => "",
			"type" => "qodux_upload"
		),
		array(
			"name" => esc_html(__("Header Background Position",'besmart')),
			"desc" => "Choose the background image position.",
			"id" => "_header_position_x",
			"default" => 'center',
			"options" => array(
				"left" => esc_html(__('Left','besmart')),
				"center" => esc_html(__('Center','besmart')),
				"right" => esc_html(__('Right','besmart')),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Header Background Repeat",'besmart')),
			"desc" => "Choose the background image repeat style.",
			"id" => "_header_repeat",
			"default" => 'no-repeat',
			"options" => array(
				"no-repeat" => esc_html(__('No Repeat','besmart')),
				"repeat" => esc_html(__('Repeat','besmart')),
				"repeat-x" => esc_html(__('Repeat Horizontally','besmart')),
				"repeat-y" => esc_html(__('Repeat Vertically','besmart')),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Header Background Color",'besmart')),
			"desc" => esc_html(__("Here you can choose a specific page background color. Set it to transparent in order to disable this.",'besmart')),
			"id" => "_header_bg_color",
			"default" => "",
			"type" => "qodux_color"		
		),
		
	array(
		"type" => "qodux_group_end",
	),
	
	array(
		"type" => "qodux_option_group_start",
		"group_id" => "breadcrumbs",
	),	
		array(
			"name" => esc_html(__("Breadcrumbs Background Image",'besmart')),
			"desc" => esc_html(__("You can paste the full URL of the image (including ",'besmart')) . "<code>http://</code>" . esc_html(__("), to be used as a background image, or you can simply upload it using the button.",'besmart')),
			"id" => "_breadcrumbs_bg",
			"default" => "",
			"type" => "qodux_upload"
		),
		array(
			"name" => esc_html(__("Breadcrumbs Background Position",'besmart')),
			"desc" => "Choose the background image position.",
			"id" => "_breadcrumbs_position_x",
			"default" => 'center',
			"options" => array(
				"left" => esc_html(__('Left','besmart')),
				"center" => esc_html(__('Center','besmart')),
				"right" => esc_html(__('Right','besmart')),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Breadcrumbs Background Repeat",'besmart')),
			"desc" => "Choose the background image repeat style.",
			"id" => "_breadcrumbs_repeat",
			"default" => 'no-repeat',
			"options" => array(
				"no-repeat" => esc_html(__('No Repeat','besmart')),
				"repeat" => esc_html(__('Repeat','besmart')),
				"repeat-x" => esc_html(__('Repeat Horizontally','besmart')),
				"repeat-y" => esc_html(__('Repeat Vertically','besmart')),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Breadcrumbs Background Color",'besmart')),
			"desc" => esc_html(__("Here you can choose a specific intro background color. Set it to transparent in order to disable this.",'besmart')),
			"id" => "_breadcrumbs_bg_color",
			"default" => "",
			"type" => "qodux_color"		
		),
		array(
			"name" => esc_html(__("Breadcrumbs Text Color",'besmart')),
			"desc" => esc_html(__("Here you can choose a specific text color.",'besmart')),
			"id" => "_breadcrumbs_text_color",
			"default" => "",
			"type" => "qodux_color"		
		),
	array(
		"type" => "qodux_group_end",
	),
	
	array(
		"type" => "qodux_option_group_start",
		"group_id" => "intro",
	),	
		array(
			"name" => esc_html(__("Intro Background Image",'besmart')),
			"desc" => esc_html(__("You can paste the full URL of the image (including ",'besmart')) . "<code>http://</code>" . esc_html(__("), to be used as a background image, or you can simply upload it using the button.",'besmart')),
			"id" => "_intro_bg",
			"default" => "",
			"type" => "qodux_upload"
		),
		array(
			"name" => esc_html(__("Intro Background Position",'besmart')),
			"desc" => "Choose the background image position.",
			"id" => "_intro_position_x",
			"default" => 'center',
			"options" => array(
				"left" => esc_html(__('Left','besmart')),
				"center" => esc_html(__('Center','besmart')),
				"right" => esc_html(__('Right','besmart')),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Intro Background Repeat",'besmart')),
			"desc" => "Choose the background image repeat style.",
			"id" => "_intro_repeat",
			"default" => 'no-repeat',
			"options" => array(
				"no-repeat" => esc_html(__('No Repeat','besmart')),
				"repeat" => esc_html(__('Repeat','besmart')),
				"repeat-x" => esc_html(__('Repeat Horizontally','besmart')),
				"repeat-y" => esc_html(__('Repeat Vertically','besmart')),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Intro Background Color",'besmart')),
			"desc" => esc_html(__("Here you can choose a specific intro background color. Set it to transparent in order to disable this.",'besmart')),
			"id" => "_intro_bg_color",
			"default" => "",
			"type" => "qodux_color"		
		),
		array(
			"name" => esc_html(__("Intro Text Color",'besmart')),
			"desc" => esc_html(__("Here you can choose a specific intro text color.",'besmart')),
			"id" => "_intro_text_color",
			"default" => "",
			"type" => "qodux_color"		
		),
	array(
		"type" => "qodux_group_end",
	),
	
	array(
		"type" => "qodux_option_group_start",
		"group_id" => "container",
	),	
		array(
			"name" => esc_html(__("Container Background Image",'besmart')),
			"desc" => esc_html(__("You can paste the full URL of the image (including ",'besmart')) . "<code>http://</code>" . esc_html(__("), to be used as a background image, or you can simply upload it using the button.",'besmart')),
			"id" => "_container_bg",
			"default" => "",
			"type" => "qodux_upload"
		),
		array(
			"name" => esc_html(__("Container Background Position",'besmart')),
			"desc" => "Choose the background image position.",
			"id" => "_container_position_x",
			"default" => 'center',
			"options" => array(
				"left" => esc_html(__('Left','besmart')),
				"center" => esc_html(__('Center','besmart')),
				"right" => esc_html(__('Right','besmart')),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Container Background Repeat",'besmart')),
			"desc" => "Choose the background image repeat style.",
			"id" => "_container_repeat",
			"default" => 'no-repeat',
			"options" => array(
				"no-repeat" => esc_html(__('No Repeat','besmart')),
				"repeat" => esc_html(__('Repeat','besmart')),
				"repeat-x" => esc_html(__('Repeat Horizontally','besmart')),
				"repeat-y" => esc_html(__('Repeat Vertically','besmart')),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Container Background Color",'besmart')),
			"desc" => esc_html(__("Here you can choose a specific intro background color. Set it to transparent in order to disable this.",'besmart')),
			"id" => "_container_bg_color",
			"default" => "",
			"type" => "qodux_color"		
		),
	array(
		"type" => "qodux_group_end",
	),
	
	array(
		"type" => "qodux_option_group_start",
		"group_id" => "content",
	),	
		array(
			"name" => esc_html(__("Content Background Image",'besmart')),
			"desc" => esc_html(__("You can paste the full URL of the image (including ",'besmart')) . "<code>http://</code>" . esc_html(__("), to be used as a background image, or you can simply upload it using the button.",'besmart')),
			"id" => "_content_bg",
			"default" => "",
			"type" => "qodux_upload"
		),
		array(
			"name" => esc_html(__("Content Background Position",'besmart')),
			"desc" => "Choose the background image position.",
			"id" => "_content_position_x",
			"default" => 'center',
			"options" => array(
				"left" => esc_html(__('Left','besmart')),
				"center" => esc_html(__('Center','besmart')),
				"right" => esc_html(__('Right','besmart')),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Content Background Repeat",'besmart')),
			"desc" => "Choose the background image repeat style.",
			"id" => "_content_repeat",
			"default" => 'no-repeat',
			"options" => array(
				"no-repeat" => esc_html(__('No Repeat','besmart')),
				"repeat" => esc_html(__('Repeat','besmart')),
				"repeat-x" => esc_html(__('Repeat Horizontally','besmart')),
				"repeat-y" => esc_html(__('Repeat Vertically','besmart')),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Content Background Color",'besmart')),
			"desc" => esc_html(__("Here you can choose a specific intro background color. Set it to transparent in order to disable this.",'besmart')),
			"id" => "_content_bg_color",
			"default" => "",
			"type" => "qodux_color"		
		),
	array(
		"type" => "qodux_group_end",
	),
	
	array(
		"type" => "qodux_option_group_start",
		"group_id" => "footer",
	),
		array(
			"name" => esc_html(__("Footer Top",'besmart')),
			"type" => "qodux_title",
		),
		array(
			"name" => esc_html(__("Footer Top Background Image",'besmart')),
			"desc" => esc_html(__("You can paste the full URL of the image (including ",'besmart')) . "<code>http://</code>" . esc_html(__("), to be used as a background image, or you can simply upload it using the button.",'besmart')),
			"id" => "_footer_top_bg",
			"default" => "",
			"type" => "qodux_upload"
		),
		array(
			"name" => esc_html(__("Footer Position",'besmart')),
			"desc" => "Choose the background image position.",
			"id" => "_footer_top_position_x",
			"default" => 'center',
			"options" => array(
				"left" => esc_html(__('Left','besmart')),
				"center" => esc_html(__('Center','besmart')),
				"right" => esc_html(__('Right','besmart')),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Footer Repeat",'besmart')),
			"desc" => "Choose the background image repeat style.",
			"id" => "_footer_top_repeat",
			"default" => 'no-repeat',
			"options" => array(
				"no-repeat" => esc_html(__('No Repeat','besmart')),
				"repeat" => esc_html(__('Repeat','besmart')),
				"repeat-x" => esc_html(__('Repeat Horizontally','besmart')),
				"repeat-y" => esc_html(__('Repeat Vertically','besmart')),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Footer Top Background Color",'besmart')),
			"desc" => esc_html(__("Here you can choose a specific page background color. Set it to transparent in order to disable this.",'besmart')),
			"id" => "_footer_top_color",
			"default" => "",
			"type" => "qodux_color"		
		),
		
		array(
			"name" => esc_html(__("Footer Middle",'besmart')),
			"type" => "qodux_title",
		),
		array(
			"name" => esc_html(__("Footer Background Image",'besmart')),
			"desc" => esc_html(__("You can paste the full URL of the image (including ",'besmart')) . "<code>http://</code>" . esc_html(__("), to be used as a background image, or you can simply upload it using the button.",'besmart')),
			"id" => "_footer_bg",
			"default" => "",
			"type" => "qodux_upload"
		),
		array(
			"name" => esc_html(__("Footer Position",'besmart')),
			"desc" => "Choose the background image position.",
			"id" => "_footer_position_x",
			"default" => 'center',
			"options" => array(
				"left" => esc_html(__('Left','besmart')),
				"center" => esc_html(__('Center','besmart')),
				"right" => esc_html(__('Right','besmart')),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Footer Repeat",'besmart')),
			"desc" => "Choose the background image repeat style.",
			"id" => "_footer_repeat",
			"default" => 'no-repeat',
			"options" => array(
				"no-repeat" => esc_html(__('No Repeat','besmart')),
				"repeat" => esc_html(__('Repeat','besmart')),
				"repeat-x" => esc_html(__('Repeat Horizontally','besmart')),
				"repeat-y" => esc_html(__('Repeat Vertically','besmart')),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Footer Background Color",'besmart')),
			"desc" => esc_html(__("If you specify a color below, this option will override the global configuration. Set it to transparent in order to disable this.",'besmart')),
			"id" => "_footer_bg_color",
			"default" => "",
			"type" => "qodux_color"		
		),
		
		
		array(
			"name" => esc_html(__("Footer Bottom",'besmart')),
			"type" => "qodux_title",
		),
		array(
			"name" => esc_html(__("Footer Bottom Background Image",'besmart')),
			"desc" => esc_html(__("You can paste the full URL of the image (including ",'besmart')) . "<code>http://</code>" . esc_html(__("), to be used as a background image, or you can simply upload it using the button.",'besmart')),
			"id" => "_footer_bottom_bg",
			"default" => "",
			"type" => "qodux_upload"
		),
		array(
			"name" => esc_html(__("Footer Position",'besmart')),
			"desc" => "Choose the background image position.",
			"id" => "_footer_bottom_position_x",
			"default" => 'center',
			"options" => array(
				"left" => esc_html(__('Left','besmart')),
				"center" => esc_html(__('Center','besmart')),
				"right" => esc_html(__('Right','besmart')),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Footer Repeat",'besmart')),
			"desc" => "Choose the background image repeat style.",
			"id" => "_footer_bottom_repeat",
			"default" => 'no-repeat',
			"options" => array(
				"no-repeat" => esc_html(__('No Repeat','besmart')),
				"repeat" => esc_html(__('Repeat','besmart')),
				"repeat-x" => esc_html(__('Repeat Horizontally','besmart')),
				"repeat-y" => esc_html(__('Repeat Vertically','besmart')),
			),
			"type" => "qodux_select",
		),
		array(
			"name" => esc_html(__("Footer Bottom Background Color",'besmart')),
			"desc" => esc_html(__("Here you can choose a specific page background color. Set it to transparent in order to disable this.",'besmart')),
			"id" => "_footer_bottom_color",
			"default" => "",
			"type" => "qodux_color"		
		),
	array(
		"type" => "qodux_group_end",
	),	
);

new qodux_metaboxes($config,$options);