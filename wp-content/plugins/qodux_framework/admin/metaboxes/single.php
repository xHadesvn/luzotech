<?php 
$config = array(
	'title' => esc_html(__('Blog Single Options','besmart')),
	'id' => 'single',
	'pages' => array('post'),
	'callback' => '',
	'context' => 'normal',
	'priority' => 'low',
);
$options = array(
	array(
		"name" => esc_html(__("Featured Post Entry",'besmart')),
		"desc" => esc_html(__("Here you can choose to dispaly or not Featured Image/Video/Mp3/Slideshow in Single Blog post only.",'besmart')),
		"id" => "_featured_image",
		"default" => '',
		"type" => "qodux_tritoggle",
	),
	array(
		"name" => esc_html(__("Thumbnail Types",'besmart')),
		"desc" => sprintf(esc_html(__("Thumbnail Types",'besmart')),QODUX_FRAME_NAME),
		"id" => "_thumbnail_type",
		"default" => 'timage',
		"options" => array(
			"timage" => esc_html(__('Image','besmart')),
			"tvideo" => esc_html(__('Video','besmart')),
			"tplayer" => esc_html(__('Audio','besmart')),
			"tslide" => esc_html(__('Slide','besmart')),
		),
		"type" => "qodux_select",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "thumbnail_player",
		"group_class" => "featured_type",
	),
	array(
		"name" => esc_html(__("SoundCloud Link",'besmart')),
		"desc" => esc_html(__("The SoundCloud url, ex: <b>\"https://soundcloud.com/fiersa/fiersa-besari-roar-katy-perry\"</b> or <b>\"http://api.soundcloud.com/tracks/129129144\"</b>",'besmart')),
		"size" => 30,
		"id" => "_thumbnail_player",
		"default" => '',
		"class" => 'full',
		"type" => "qodux_text",
	),
	array(
		"type" => "qodux_group_end",
	),
	
	array(
		"type" => "qodux_group_start",
		"group_id" => "thumbnail_slide",
		"group_class" => "featured_type",
	),

	array(
		"name" => esc_html(__("Slide Type",'besmart')),
		"id" => "_slide_type",
		"desc" => esc_html(__("Here you can choose the type of slideshow you want to use on this post.",'besmart')),
		"default" => 'owl',
		"options" => array(
			"owl" => 'Owl Slider',
			"flex" => 'Flex Slider',
			"nivo" => 'Nivo Slider',
		),
		"type" => "qodux_select",
	),
	array(
		"name" => esc_html(__("Flex Slide Effect",'besmart')),
		"id" => "_flex_slide_effect",
		"desc" => esc_html(__("Here you can choose the FLEX slide effect.",'besmart')),
		"default" => 'fade',
		"options" => array(
			"fade" => 'Fade',
			"slide" => 'Slide',
		),
		"type" => "qodux_select",
	),
	array(
		"name" => esc_html(__("Nivo Slide Effect",'besmart')),
		"id" => "_slide_effect",
		"desc" => esc_html(__("Here you can choose the NIVO slide effect.",'besmart')),
		"default" => 'slideInLeft',
		"options" => array(
			"sliceDown" => 'sliceDown',
			"sliceDownLeft" => 'sliceDownLeft',
			"sliceUp" => 'sliceUp',
			"sliceUpLeft" => 'sliceUpLeft',
			"sliceUpDown" => 'sliceUpDown',
			"sliceUpDownLeft" => 'sliceUpDownLeft',
			"fade" => 'fade',
			"fold" => 'fold',
			"random" => 'random',
			"slideInRight" => 'slideInRight',
			"slideInLeft" => 'slideInLeft',
			"boxRandom" => 'boxRandom',
			"boxRain" => 'boxRain',
			"boxRainReverse" => 'boxRainReverse',
			"boxRainGrow" => 'boxRainGrow',
			"boxRainGrowReverse" => 'boxRainGrowReverse',
		),
		"type" => "qodux_select",
	),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"name" => esc_html(__("Layout",'besmart')),
		"desc" => esc_html(__("Choose the layout for this single page/post.",'besmart')),
		"id" => "_sidebar_alignment",
		"default" => 'default',
		"options" => array(
			"default" => esc_html(__('Default','besmart')),
			"full" => esc_html(__('Full Width','besmart')),
			"right" => esc_html(__('Right Sidebar','besmart')),
			"left" => esc_html(__('Left Sidebar','besmart')),
		),
		"type" => "qodux_select",
	),
	array(
		"name" => esc_html(__("Disable Breadcrumbs",'besmart')),
		"desc" => esc_html(__('This option disables breadcrumbs on a page/post.','besmart')),
		"id" => "_disable_breadcrumb",
		"label" => "Check to disable breadcrumbs on this post",
		"default" => "",
		"type" => "qodux_tritoggle"
	),
	array(
		"name" => esc_html(__("Custom Sidebar",'besmart')),
		"desc" => esc_html(__("If there are any custum sidebars created in your theme option panel then you can choose one of them to be displayed on this.",'besmart')),
		"id" => "_sidebar",
		"prompt" => esc_html(__("Choose one..",'besmart')),
		"default" => '',
		"options" => get_sidebar_options(),
		"type" => "qodux_select",
	),
);
new qodux_metaboxes($config,$options);