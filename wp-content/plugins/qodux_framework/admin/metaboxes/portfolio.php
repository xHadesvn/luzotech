<?php

$config = array(
	'title' => sprintf( esc_html(__('Portfolio Item Options','besmart')),QODUX_FRAME_NAME),
	'id' => 'portfolio',
	'pages' => array('wt_portfolio'),
	'callback' => '',
	'context' => 'normal',
	'priority' => 'high',
);
function get_sidebar_portfolio(){
	$sidebars = besmart_get_option('sidebar','sidebars');
	if(!empty($sidebars)){
		$sidebars_array = explode(',',$sidebars);
		
		$options = array();
		foreach ($sidebars_array as $sidebar){
			$options[$sidebar] = $sidebar;
		}
		return $options;
	}else{
		return array();
	}
}
$options = array(
	array(
		"name" => esc_html(__("Featured Portfolio Entry",'besmart')),
		"desc" => esc_html(__("Here you can choose to dispaly or not the Featured Portfolio Entry only for this portfolio item.",'besmart')),
		"id" => "_featured_image",
		"default" => '',
		"type" => "qodux_tritoggle",
	),
	array(
		"name" => esc_html(__("Disable Breadcrumbs",'besmart')),
		"desc" => esc_html(__('This option disables breadcrumbs on a page/post.','besmart')),
		"id" => "_disable_breadcrumb",
		"label" => "Check to disable breadcrumbs on this post",
		"default" => "",
		"type" => "qodux_tritoggle"
	),	
	array(
		"name" => esc_html(__("Portfolio Type",'besmart')),
		"desc" => sprintf(esc_html(__("The lightbox supports just images and videos. If the portfolio is a document type then the thumbnail image is linked to the portfolio item.",'besmart')),QODUX_FRAME_NAME),
		"id" => "_portfolio_type",
		"default" => 'image',
		"options" => array(
			"image" => esc_html(__('Image','besmart')),
			"video" => esc_html(__('Video','besmart')),
			"doc" => esc_html(__('Document','besmart')),
			"link" => esc_html(__('Link','besmart')),
		),
		"type" => "qodux_select",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "portfolio_image",
		"group_class" => "portfolio_type",
	),
	array(
		"name" => esc_html(__("Fullsize Image for Lightbox (optional)",'besmart')),
		"desc" => esc_html(__("If this field is empty then the lightbox will be opened with the feature image. Otherwise you should upload a full size image to open the lightbox on click.",'besmart')),
		"id" => "_image",
		"button" => "Insert Image",
		"default" => '',
		"type" => "qodux_upload",
	),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "portfolio_video",
		"group_class" => "portfolio_type",
	),
	array(
		"name" => esc_html(__("Video Link for Lightbox",'besmart')),
		"desc" => esc_html(__("If the portfolio is a video type one, you can paste here the full url of your video.",'besmart')),
		"size" => 30,
		"id" => "_video",
		"default" => '',
		"class" => 'full',
		"type" => "qodux_text",
	),
	array(
		"name" => esc_html(__("Video Width",'besmart')),
		"desc" => esc_html(__("The width you specify here is going to override the global configuration.",'besmart')),
		"id" => "_video_width",
		"default" => '',
		"type" => "qodux_text"
	),
	array(
		"name" => esc_html(__("Video Height",'besmart')),
		"desc" => esc_html(__("The height you specify here is going to override the global configuration.",'besmart')),
		"id" => "_video_height",
		"default" => '',
		"type" => "qodux_text"
	),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "portfolio_document",
		"group_class" => "portfolio_type",
	),
	array(
		"name" => esc_html(__("Document Target",'besmart')),
		"id" => "_doc_target",
		"default" => '_self',
		"options" => array(
			"_self" => esc_html(__('Opens in the same window and same frame.','besmart')),
			"_top" => esc_html(__('Opens in the same window, taking the full window if there is more than one frame.','besmart')),
			"_parent" => esc_html(__('Opens in the parent frame.','besmart')),
			"_blank" => esc_html(__('Opens in a new window.','besmart')),
		),
		"type" => "qodux_select",
	),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "portfolio_link",
		"group_class" => "portfolio_type",
	),
	array(
		"name" => esc_html(__("Link for Portfolio item",'besmart')),
		"desc" => esc_html(__("If the portfolio is a link type one, you can paste here the full link.",'besmart')),
		"id" => "_portfolio_link",
		"default" => "",
		"shows" => array('page','cat','post','manually'),
		"type" => "qodux_superlink"	
	),
	array(
		"name" => esc_html(__("Link Target",'besmart')),
		"id" => "_portfolio_link_target",
		"default" => '_self',
		"options" => array(
			"_self" => esc_html(__('Opens in the same window and same frame.','besmart')),
			"_top" => esc_html(__('Opens in the same window, taking the full window if there is more than one frame.','besmart')),
			"_parent" => esc_html(__('Opens in the parent frame.','besmart')),
			"_blank" => esc_html(__('Opens in a new window.','besmart')),
		),
		"type" => "qodux_select",
	),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"name" => esc_html(__("Custom Sidebar",'besmart')),
		"desc" => esc_html(__("If there are any custum sidebars created in your theme option panel then you can choose one of them to be displayed on this.",'besmart')),
		"id" => "_sidebar",
		"prompt" => esc_html(__("Choose one...",'besmart')),
		"default" => '',
		"options" => get_sidebar_portfolio(),
		"type" => "qodux_select",
	),
);
new qodux_metaboxes($config,$options);