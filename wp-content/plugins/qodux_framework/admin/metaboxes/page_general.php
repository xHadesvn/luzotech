<?php
$config = array(
	'title' => sprintf( esc_html(__('Page General Options','besmart')),QODUX_FRAME_NAME),
	'id' => 'page_general',
	'pages' => array('page','post'),
	'callback' => '',
	'context' => 'normal',
	'priority' => 'low',
);
function get_sidebar_options(){
	$sidebars = besmart_get_option('sidebar','sidebars');
	if(!empty($sidebars)){
		$sidebars_array = explode(',',$sidebars);
		
		$options = array();
		foreach ($sidebars_array as $sidebar){
			$options[$sidebar] = $sidebar;
		}
		return $options;
	}else{
		return array();
	}
}
$options = array(
	array(
		"name" => esc_html(__("Page Intro Area Type",'besmart')),
		"desc" => esc_html(__("Choose which type of header area you want to display on this page. Static images / videos are setted in the \"Featured Image\" / \"Whoathemes Featured Video\" areas.",'besmart')),
		"id" => "_intro_type",
		"options" => array(
			"default" => "Default",
			"title" => "Title only",
			"custom" => "Custom text only",
			"title_custom" => "Title with custom text",
			//"slideshow" => "Slideshow",
			/*"static_image" => "Static Image",
			"static_video" => "Static Video",*/
			"disable" => "Disable",
		),
		"default" => "default",
		"chosen" => "true", 
		"type" => "qodux_select",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "intro_title",
		"group_class" => "intro_type",
	),
	array(
		"name" => esc_html(__("Page Intro Custom Title",'besmart')),
		"desc" => esc_html(__('If you enter a text here, this will override the default header title.','besmart')),
		"id" => "_custom_title",
		"default" => "",
		"class" => 'full',
		"type" => "qodux_text"		
	),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "intro_text",
		"group_class" => "intro_type",
	),
	array(
		"name" => esc_html(__("Page Intro Custom Text",'besmart')),
		"desc" => esc_html(__('If you enter a text here, this will override your default header custom text only if custom text option above is selected.','besmart')),
		"id" => "_custom_introduce_text",
		"rows" => "2",
		"default" => "",
		"type" => "qodux_textarea"
	),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "intro_slideshow",
		"group_class" => "intro_type",
	),
	array(
		"name" => esc_html(__("SlideShow Type",'besmart')),
		"desc" => esc_html(__("Select which type of slideshow you want on this page/post.",'besmart')),
		"id" => "_slideshow_type",
		"prompt" => esc_html(__("Choose Slideshow Type",'besmart')),
		"default" => '',
		"options" => array(
			"rev" => esc_html(__('Revolution Slider','besmart')),
			"flex" => esc_html(__('Flex Slider','besmart')),
			"nivo" => esc_html(__('Nivo Slider','besmart')),
			"cycle" => esc_html(__('Cycle Slider','besmart')),
		),
		"type" => "qodux_select",
	),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "slideshow_rev",
		"group_class" => "slideshow_type",
	),
	array(
		"name" => esc_html(__("Rev SlideShow Type",'besmart')),
		"prompt" => esc_html(__("Choose Slideshow Type",'besmart')),
		"desc" => esc_html(__("Select which type of slideshow you want on this page/post.",'besmart')),
		"id" => "_rev_slideshow",
		"type" => "qodux_selectRev",
	),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "slideshow_layerS",
		"group_class" => "slideshow_type",
	),
	array(
		"name" => esc_html(__("Layer SlideShow Type",'besmart')),
		"prompt" => esc_html(__("Choose Slideshow Type",'besmart')),
		"desc" => esc_html(__("Select which type of slideshow you want on this page/post.",'besmart')),
		"id" => "_layer_slideshow",
		"type" => "qodux_selectLayerS",
	),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"name" => esc_html(__("Disable Breadcrumbs",'besmart')),
		"desc" => esc_html(__('This option disables breadcrumbs on a page/post.','besmart')),
		"id" => "_disable_breadcrumb",
		"label" => "Check to disable breadcrumbs on this post",
		"default" => "",
		"type" => "qodux_tritoggle"
	),	
	array(
		"name" => esc_html(__("Top section margin",'besmart')),
		"id" => "_top_margins",
		"min" => "0",
		"max" => "150",
		"step" => "1",
		"unit" => 'px',
		"default" => "0",
		"type" => "qodux_range",
	),
	
	array(
		"name" => esc_html(__("Bottom section margin",'besmart')),
		"id" => "_bottom_margins",
		"min" => "0",
		"max" => "150",
		"step" => "1",
		"unit" => 'px',
		"default" => "0",
		"type" => "qodux_range",
	),
	array(
		"name" => esc_html(__("Custom Sidebar",'besmart')),
		"desc" => esc_html(__("If there are any custum sidebars created in your theme option panel then you can choose one of them to be displayed on this.",'besmart')),
		"id" => "_sidebar",
		"prompt" => esc_html(__("Choose one...",'besmart')),
		"default" => '',
		"options" => get_sidebar_options(),
		"type" => "qodux_select",
	),
	
);

new qodux_metaboxes($config,$options);