<?php
$config = array(
	'title' => sprintf( esc_html(__('Section Options','besmart')),besmart_NAME),
	'id' => 'section',
	'pages' => array('wt_section'),
	'callback' => '',
	'context' => 'normal',
	'priority' => 'low',
);
$options = array(
	array(
		"name" => esc_html(__("Page Intro Area Type",'besmart')),
		"desc" => esc_html(__("Choose which type of header area you want to display on this page.",'besmart')),
		"id" => "_intro_type",
		"options" => array(
			"default" => "Default",
			"title" => "Title only",
			"custom" => "Custom text only",
			"title_custom" => "Title with custom text",
			"disable" => "Disable",
		),
		"default" => "default",
		"chosen" => "true", 
		"type" => "qodux_select",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "intro_title",
		"group_class" => "intro_type",
	),
	array(
		"name" => esc_html(__("Page Intro Custom Title",'besmart')),
		"desc" => esc_html(__('If you enter a text here, this will override the default header title.','besmart')),
		"id" => "_custom_title",
		"default" => "",
		"class" => 'full',
		"type" => "qodux_text"		
	),
	array(
		"type" => "group_end",
	),
	array(
		"type" => "group_start",
		"group_id" => "intro_text",
		"group_class" => "intro_type",
	),
	array(
		"name" => esc_html(__("Page Intro Custom Text",'besmart')),
		"desc" => esc_html(__('If you enter a text here, this will override your default header custom text only if custom text option above is selected.','besmart')),
		"id" => "_custom_introduce_text",
		"rows" => "2",
		"default" => "",
		"type" => "qodux_textarea"
	),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "intro_slideshow",
		"group_class" => "intro_type",
	),
	array(
		"name" => esc_html(__("SlideShow Type",'besmart')),
		"desc" => esc_html(__("Select which type of slideshow you want on this page/post.",'besmart')),
		"id" => "_slideshow_type",
		"prompt" => esc_html(__("Choose Slideshow Type",'besmart')),
		"default" => '',
		"options" => array(
			"rev" => esc_html(__('Revolution Slider','besmart')),
			"flex" => esc_html(__('Flex Slider','besmart')),
			"nivo" => esc_html(__('Nivo Slider','besmart')),
			"cycle" => esc_html(__('Cycle Slider','besmart')),
		),
		"type" => "qodux_select",
	),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "slideshow_rev",
		"group_class" => "slideshow_type",
	),
	array(
		"name" => esc_html(__("Rev SlideShow Type",'besmart')),
		"prompt" => esc_html(__("Choose Slideshow Type",'besmart')),
		"desc" => esc_html(__("Select which type of slideshow you want on this page/post.",'besmart')),
		"id" => "_rev_slideshow",
		"type" => "qodux_selectRev",
	),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "slideshow_layerS",
		"group_class" => "slideshow_type",
	),
	array(
		"name" => esc_html(__("Layer SlideShow Type",'besmart')),
		"prompt" => esc_html(__("Choose Slideshow Type",'besmart')),
		"desc" => esc_html(__("Select which type of slideshow you want on this page/post.",'besmart')),
		"id" => "_layer_slideshow",
		"type" => "qodux_selectLayerS",
	),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"name" => esc_html(__("Disable Breadcrumbs",'besmart')),
		"desc" => esc_html(__('This option disables breadcrumbs on a page/post.','besmart')),
		"id" => "_disable_breadcrumb",
		"label" => "Check to disable breadcrumbs on this post",
		"default" => "",
		"type" => "qodux_tritoggle"
	),	
	array(
		"name" => esc_html(__("Background Style",'besmart')),
		"desc" => esc_html(__("Choose background style for sections", 'besmart')),
		"id" => "_background_style",
		"default" => '',
		"prompt" => esc_html(__("Choose Type",'besmart')),
		"options" => array(
			"wt_section_white" => esc_html(__('White','besmart')),
			"wt_section_dark" => esc_html(__('Dark','besmart')),
		),
		"type" => "qodux_select",
	),	
	array(
		"name" => esc_html(__("Page Background Type",'besmart')),
		"desc" => esc_html(__("Choose which type of background area you want to display on this section.",'besmart')),
		"id" => "_bg_type",
		"options" => array(
			"pattern" => "Pattern",
			"parallax" => "Parallax",
			"cover" => "Cover Image",
			"video" => "Video",
			"color" => "Color",
		),
		"default" => "pattern",
		"chosen" => "true", 
		"type" => "qodux_select",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "pattern",
		"group_class" => "bg_type",
	),
        array(
            "name" => esc_html(__("Page Background Pattern Image",'besmart')),
            "desc" => esc_html(__( "You can paste the full URL (including <code>http://</code>) of the image to be used as a background image or you can simply upload it using the button.",'besmart')),
            "id" => "_bg_style_image",
            "default" => "",
            "type" => "qodux_upload"
        ),
        array(
            "name" => esc_html(__("Page Background Position",'besmart')),
            "desc" => "Choose the background image position.",
            "id" => "_bg_style_position_x",
            "default" => 'center',
            "options" => array(
                "left" => esc_html(__('Left','besmart')),
                "center" => esc_html(__('Center','besmart')),
                "right" => esc_html(__('Right','besmart')),
            ),
            "type" => "qodux_select",
        ),
        array(
            "name" => esc_html(__("Page Background Repeat",'besmart')),
            "desc" => "Choose the background image repeat style.",
            "id" => "_bg_style_repeat",
            "default" => 'no-repeat',
            "options" => array(
                "no-repeat" => esc_html(__('No Repeat','besmart')),
                "repeat" => esc_html(__('Repeat','besmart')),
                "repeat-x" => esc_html(__('Repeat Horizontally','besmart')),
                "repeat-y" => esc_html(__('Repeat Vertically','besmart')),
            ),
            "type" => "qodux_select",
        ),
        array(
            "name" => esc_html(__("Background Style Color",'besmart')),
            "desc" => esc_html(__("Choose background style for sections", 'besmart')),
            "id" => "_background_style_color",
            "default" => '',
            "type" => "qodux_color",
        ),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "parallax",
		"group_class" => "bg_type",
	),

        array(
            "name" => esc_html(__("Page Background Parallax Image",'besmart')),
            "desc" => esc_html(__( "You can paste the full URL (including <code>http://</code>) of the image to be used as a background image or you can simply upload it using the button.",'besmart')),
            "id" => "_bg_style_parallax",
            "default" => "",
            "type" => "qodux_upload"
        ),
   	array(
		"type" => "qodux_group_end",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "cover",
		"group_class" => "bg_type",
	),

        array(
            "name" => esc_html(__("Page Background Cover Image",'besmart')),
            "desc" => esc_html(__( "You can paste the full URL (including <code>http://</code>) of the image to be used as a background cover image or you can simply upload it using the button.",'besmart')),
            "id" => "_bg_style_cover",
            "default" => "",
            "type" => "qodux_upload"
        ),
   	array(
		"type" => "qodux_group_end",
	),
	array(
		"type" => "qodux_group_start",
		"group_id" => "video",
		"group_class" => "bg_type",
	),
        array(
            "name" => esc_html(__("Youtube Video Background",'besmart')),
			 "desc" => esc_html(__( "You need to paste only the video ID (for example http://www.youtube.com/watch?v=<code>Ufnf0ecwzVI</code>).",'besmart')),
            "id" => "_bg_video",
            "default" => "",
            "type" => "qodux_text"
        ),
   	array(
		"type" => "qodux_group_end",
	),

	array(
		"type" => "qodux_group_start",
		"group_id" => "color",
		"group_class" => "bg_type",
	),
        array(
            "name" => esc_html(__("Background Style Color",'besmart')),
            "desc" => esc_html(__("Choose background style for sections", 'besmart')),
            "id" => "_bg_style_color",
            "default" => '',
            "type" => "qodux_color",
        ),
	array(
		"type" => "qodux_group_end",
	),
	array(
		"name" => esc_html(__("Background Overlay Color",'besmart')),
		"desc" => esc_html(__("Choose background style for parallax overlay color", 'besmart')),
		"id" => "_bg_overlay",
		"default" => '',
		"type" => "qodux_color",
	),
	array(
		"name" => esc_html(__("Disable top-bottom section margins",'besmart')),
		"desc" => esc_html(__("Set the button Off if you want to disable the top-bottom margins for section.", 'besmart')),
		"id" => "_disable_margins",
		"default" => '',
		"type" => "qodux_toggle",
	),
	array(
		"name" => esc_html(__("Display Top Arrow",'besmart')),
		"desc" => esc_html(__("Choose if you want to display top arrow", 'besmart')),
		"id" => "_display_arrow",
		"default" => '',
		"type" => "qodux_toggle",
	),
	
);

new qodux_metaboxes($config,$options);