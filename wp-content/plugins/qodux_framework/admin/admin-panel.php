<?php
class Qodux_Admin {
	function qodux_init(){
		$this->qodux_functions();
		add_action('admin_menu', array(&$this,'qodux_menus'));
		add_action('admin_notices',  array(&$this,'qodux_warnings'));
		
		$this->qodux_metaboxes();	
		add_action('wp_ajax_theme-flush-rewrite-rules', array(&$this,'flush_rewrite_rules'));
		
		add_action('admin_init', array(&$this,'qodux_after_qodux_is_activated'));
	}
	/**
	 * Check if users need to set the file permissions in order to support the theme, and if not, displays warnings messages in admin option page.
	 */
	function qodux_warnings(){
		global $wp_version;

		$warnings = array();
		if(!qodux_check_wp_version()){
			$warnings[]='Wordpress version(<b>'.$wp_version.'</b>) is too low. Please upgrade to the latest version.';
		}
		if(!function_exists("imagecreatetruecolor")){
			$warnings[]='GD Library Error: <b>imagecreatetruecolor does not exist</b>. Please contact your host provider and ask them to install the GD library, otherwise this theme won\'t work properly.';
		}
		if(!is_writeable(get_template_directory() . '/css')){
			$warnings[]='The css folder (<b>'.str_replace( WP_CONTENT_DIR, '', get_template_directory() . '/css' ).'</b>) is not writeable. Please set the correct file permissions (<b>\'777\' or \'755\'</b>), otherwise this theme won\'t work properly.';
		}		
		if(!file_exists(get_template_directory() . '/css'.DIRECTORY_SEPARATOR.'skin.css')){
			$warnings[]='The skin style file (<b>'.str_replace( WP_CONTENT_DIR, '', get_template_directory() . '/css' ).'/skin.css'.'</b>) doesn\'t exists or it was deleted. Please manually create this file or click on \'Save changes\' and it will be automatically created.';
		}
		if(!is_writeable(get_template_directory() . '/css'.DIRECTORY_SEPARATOR.'skin.css')){
			$warnings[]='The skin style file (<b>'.str_replace( WP_CONTENT_DIR, '', get_template_directory() . '/css' ).'/skin.css'.'</b>) is not writeable. Please set the correct permissions (<b>\'777\' or \'755\'</b>), otherwise this theme won\'t work properly.';
		}			
		
		$str = '';
		if(!empty($warnings)){
			$str = '<ul>';
			foreach($warnings as $warning){
				$str .= '<li>'.$warning.'</li>';
			}
			$str .= '</ul>';
			echo "
				<div id='theme-warning' class='error fade'><p><strong>".sprintf(esc_html(__('%1$s Error Messages','besmart')), 'BeSmart')."</strong><br/>".$str."</p></div>
			";
		}
		
	}
	function qodux_functions(){
		require_once(QODUX_FW_PATH . '/admin/functions/theme-functions.php');
		require_once(QODUX_FW_PATH . '/admin/functions/custom_scripts.php');
		require_once(QODUX_FW_PATH . '/admin/functions/option-media-upload.php');
	}
	/**
	 * Create theme options menu
	 */
	function qodux_menus(){
		add_menu_page('Whoathemes', 'Whoathemes', 'edit_theme_options', 'general', array(&$this,'_load_option_page'),'', '59.7');
		add_submenu_page('general', 'General', 'General', 'edit_theme_options', 'general', array(&$this,'_load_option_page'));
		add_submenu_page('general', 'Blog', 'Blog', 'edit_theme_options', 'blog', array(&$this,'_load_option_page'));
		add_submenu_page('general', 'Portfolio', 'Portfolio', 'edit_theme_options', 'portfolio', array(&$this,'_load_option_page'));
		add_submenu_page('general', 'Fonts', 'Fonts', 'edit_theme_options', 'fonts', array(&$this,'_load_option_page'));
		add_submenu_page('general', 'Background', 'Background', 'edit_theme_options', 'background', array(&$this,'_load_option_page'));
		add_submenu_page('general', 'Color', 'Color', 'edit_theme_options', 'color', array(&$this,'_load_option_page'));
		add_submenu_page('general', 'Sidebar', 'Sidebar', 'edit_theme_options', 'sidebar', array(&$this,'_load_option_page'));
		add_submenu_page('general', 'Footer', 'Footer', 'edit_theme_options', 'footer', array(&$this,'_load_option_page'));
	}
	
	/**
	 * call and display the requested options page
 	 */
	function _load_option_page(){
		include_once (QODUX_FW_PATH . '/includes/options.php');
		$page = include(QODUX_FW_PATH . '/admin/options' . "/" . $_GET['page'] . '.php');
	
		if($page['auto']){
			new qodux_options($page['name'],$page['options']);
		}
	}
	
	/**
	 * Create post type metabox.
	 */
	function qodux_metaboxes(){
		require_once (QODUX_FW_PATH . '/includes/metaboxes.php');
		require_once (QODUX_FW_PATH . '/admin/metaboxes/portfolio.php');
		require_once (QODUX_FW_PATH . '/admin/metaboxes/page_general.php');
		require_once (QODUX_FW_PATH . '/admin/metaboxes/page_bg.php');
		require_once (QODUX_FW_PATH . '/admin/metaboxes/single.php');
		require_once (QODUX_FW_PATH . '/admin/metaboxes/featured_video.php');
		require_once (QODUX_FW_PATH . '/admin/metaboxes/product_options.php');
	}
	
	function flush_rewrite_rules(){
		flush_rewrite_rules();
		die (1);
	}
	
	function qodux_after_qodux_is_activated(){
		if ('themes.php' == basename($_SERVER['PHP_SELF']) && isset($_GET['activated']) && $_GET['activated']=='true' ) {
			qodux_generate_skin_css();
			wp_redirect( admin_url('admin.php?page=general') );
		}
	}
}