<?php

/**
 * Admin scripts
 */
 
add_action('admin_head', 'qodux_add_head');

/**
 * Change the icon on every page where theme use.
 */
function qodux_add_head() {
}
add_action('admin_enqueue_scripts', 'qodux_admin_register_script');
function qodux_admin_register_script(){
	wp_register_script( 'footer-column', QODUX_FW_URL . 'admin/assets/js/theme-footer-column.js', array('jquery'), null);
	wp_enqueue_script('jquery-fitvids',QODUX_FW_URL . 'admin/assets/js/jquery.fitvids.js');
	wp_enqueue_script('jquery-outside-events',QODUX_FW_URL . 'admin/assets/js/jquery.ba-outside-events.min.js',array('jquery'),'1.1');	
	wp_enqueue_script('jquery-tools-rangeinput',QODUX_FW_URL . 'admin/assets/js/rangeinput.js',array('jquery'),'1.2.5');
	wp_enqueue_script('jquery-tools-tooltip',QODUX_FW_URL . 'admin/assets/js/tooltip.js',array('jquery'),'1.2.5');
	wp_enqueue_script('jquery-tools-validator',QODUX_FW_URL . 'admin/assets/js/validator.js',array('jquery'),'1.2.7');
	wp_enqueue_script('mColorPicker',QODUX_FW_URL . 'admin/assets/js/jquery.colorInput.js',array('jquery'),'0.1.0');
	wp_enqueue_script('chosen',QODUX_FW_URL . 'admin/assets/js/chosen.jquery.js',array('jquery'),'0.9.8');
	wp_enqueue_script('iphone-style-checkboxes',QODUX_FW_URL . 'admin/assets/js/iphone-style-checkboxes.js',array('jquery'));
	wp_enqueue_script('iphone-style-tri-toggle',QODUX_FW_URL . 'admin/assets/js/iphone-style-tri-toggle.js',array('jquery'));
	wp_enqueue_script('jquery-tablednd',QODUX_FW_URL . 'admin/assets/js/jquery.tablednd.js',array('jquery'),'0.5');
	wp_enqueue_script('jquery-elastic',QODUX_FW_URL . 'admin/assets/js/jquery.elastic.min.js',array('jquery'),'1.6.11');
	wp_enqueue_script('theme-script', QODUX_FW_URL . 'admin/assets/js/script.js');
   $data = 'var qodux_admin_assets_uri= "';
   $data .= QODUX_FW_URL . '/admin/assets';
   $data .= '";';
   return wp_scripts()->add_inline_script( 'jquery-fitvids', $data, $position = 'before' );
}
if(qodux_is_options() || qodux_is_post_type()){
	add_action('admin_enqueue_scripts', 'qodux_admin_add_script');
}
function qodux_admin_add_script() {
	wp_enqueue_script('theme-script');
	add_thickbox();
	
	global $wp_version;
	if(qodux_is_options() && version_compare($wp_version, "3.5", '>=')){
		wp_enqueue_media();
	}
}
if(is_admin()){
	add_action('admin_enqueue_scripts', 'qodux_admin_add_style');
}
function qodux_admin_add_style() {
	wp_enqueue_style('thickbox');
	wp_enqueue_style('theme-style', QODUX_FW_URL . 'admin/assets/css/admin.css');
	wp_enqueue_style('theme-style-chosen', QODUX_FW_URL . 'admin/assets/css/chosen.css', false, false, 'all');
}
