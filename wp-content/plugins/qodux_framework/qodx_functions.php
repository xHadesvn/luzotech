<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**
 * Qodux Framework - functions
 *
 * @since     1.0.0
 */

/**
 * Echo escaped post title
 */
function qodx_esc_title() {
	echo qodx_get_esc_title();
}

/**
 * Return escaped post title
 */
function qodx_get_esc_title() {
	return esc_attr( the_title_attribute( 'echo=0' ) );
}

/**
 * Displays post/pages thumbnails dashboard column
 *
 * @since 1.0.0
 */
if ( is_admin() ) {
	
	add_filter( 'manage_posts_columns', 'qodx_posts_columns', 5 );
	add_filter( 'manage_pages_columns', 'qodx_posts_columns', 5 );
	add_action( 'manage_posts_custom_column', 'qodx_posts_custom_columns', 5, 2 );
	add_action( 'manage_pages_custom_column', 'qodx_posts_custom_columns', 5, 2 );

	if ( ! function_exists( 'qodx_posts_columns' ) ) {
		function qodx_posts_columns( $cols ){
			$cols['qodx_post_thumb'] = esc_html__( 'Featured Image', 'qodux' );
			return $cols;
		}
	}

	if ( ! function_exists( 'qodx_posts_custom_columns' ) ) {
		function qodx_posts_custom_columns( $col, $id ){
			if ( $col != 'qodx_post_thumb' ) {
				return;
			}
			if ( has_post_thumbnail( $id ) ) {
				$img_src = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'thumbnail', false );
				if ( ! empty( $img_src[0] ) ) { ?>
						<img src="<?php echo esc_url( $img_src[0] ); ?>" alt="<?php qodx_esc_title(); ?>" style="max-width:100%;max-height:75px;" />
					<?php
				}
			} else {
				echo '&mdash;';
			}
		}
	}

}/**
 * @param FW_Ext_Backups_Demo[] $demos
 * @return FW_Ext_Backups_Demo[]
 *
 * @since     1.0.0
 */
if ( ! function_exists('_filter_theme_fw_ext_backups_demos') ) {
	function _filter_theme_fw_ext_backups_demos($demos) {
		$demos_array = array(
			'classicv1' => array(
				'title' => __('Classic V1', 'qodux_fw'),
				'screenshot' => 'http://xml.epic-webdesign.com/unyson/besmart/classicv1.jpg',
				'preview_link' => 'http://demo.epic-webdesign.com/tf-besmart-wp/v1',
			),
			'classicv2' => array(
				'title' => __('Classic V2', 'qodux_fw'),
				'screenshot' => 'http://xml.epic-webdesign.com/unyson/besmart/classicv2.jpg',
				'preview_link' => 'http://demo.epic-webdesign.com/tf-besmart-wp/v2',
			),
			'classicv3' => array(
				'title' => __('Classic V3', 'qodux_fw'),
				'screenshot' => 'http://xml.epic-webdesign.com/unyson/besmart/classicv3.jpg',
				'preview_link' => 'http://demo.epic-webdesign.com/tf-besmart-wp/v3',
			),
			'classicv4' => array(
				'title' => __('Classic V4', 'qodux_fw'),
				'screenshot' => 'http://xml.epic-webdesign.com/unyson/besmart/classicv4.jpg',
				'preview_link' => 'http://demo.epic-webdesign.com/tf-besmart-wp/v4',
			),
			'classicv5' => array(
				'title' => __('Classic V5', 'qodux_fw'),
				'screenshot' => 'http://xml.epic-webdesign.com/unyson/besmart/classicv5.jpg',
				'preview_link' => 'http://demo.epic-webdesign.com/tf-besmart-wp/v5',
			),
			'classicv6' => array(
				'title' => __('Classic V6', 'qodux_fw'),
				'screenshot' => 'http://xml.epic-webdesign.com/unyson/besmart/classicv1.jpg',
				'preview_link' => 'http://demo.epic-webdesign.com/tf-besmart-wp/v6',
			),
			'app' => array(
				'title' => __('App', 'qodux_fw'),
				'screenshot' => 'http://xml.epic-webdesign.com/unyson/besmart/app.jpg',
				'preview_link' => 'http://demo.epic-webdesign.com/tf-besmart-wp/app',
			),
			'barber' => array(
				'title' => __('Barber', 'qodux_fw'),
				'screenshot' => 'http://xml.epic-webdesign.com/unyson/besmart/barber.jpg',
				'preview_link' => 'http://demo.epic-webdesign.com/tf-besmart-wp/barber',
			),
			'beauty' => array(
				'title' => __('Beauty', 'qodux_fw'),
				'screenshot' => 'http://xml.epic-webdesign.com/unyson/besmart/beauty.jpg',
				'preview_link' => 'http://demo.epic-webdesign.com/tf-besmart-wp/beauty',
			),
			'cleaning' => array(
				'title' => __('Cleaning', 'qodux_fw'),
				'screenshot' => 'http://xml.epic-webdesign.com/unyson/besmart/cleaning.jpg',
				'preview_link' => 'http://demo.epic-webdesign.com/tf-besmart-wp/cleaning',
			),
			'construction' => array(
				'title' => __('Construction', 'qodux_fw'),
				'screenshot' => 'http://xml.epic-webdesign.com/unyson/besmart/construction.jpg',
				'preview_link' => 'http://demo.epic-webdesign.com/tf-besmart-wp/construction',
			),
			'events' => array(
				'title' => __('Events', 'qodux_fw'),
				'screenshot' => 'http://xml.epic-webdesign.com/unyson/besmart/events.jpg',
				'preview_link' => 'http://demo.epic-webdesign.com/tf-besmart-wp/events',
			),
			'gym' => array(
				'title' => __('Gym', 'qodux_fw'),
				'screenshot' => 'http://xml.epic-webdesign.com/unyson/besmart/gym.jpg',
				'preview_link' => 'http://demo.epic-webdesign.com/tf-besmart-wp/gym',
			),
			'medical' => array(
				'title' => __('Medical', 'qodux_fw'),
				'screenshot' => 'http://xml.epic-webdesign.com/unyson/besmart/medical.jpg',
				'preview_link' => 'http://demo.epic-webdesign.com/tf-besmart-wp/medical',
			),
			// ...
		);
	
		$download_url = 'http://xml.epic-webdesign.com/unyson/besmart/';
	
		foreach ($demos_array as $id => $data) {
			$demo = new FW_Ext_Backups_Demo($id, 'piecemeal', array(
				'url' => $download_url,
				'file_id' => $id,
			));
			$demo->set_title($data['title']);
			$demo->set_screenshot($data['screenshot']);
			$demo->set_preview_link($data['preview_link']);
	
			$demos[ $demo->get_id() ] = $demo;
	
			unset($demo);
		}
	
		return $demos;
	}
	add_filter('fw:ext:backups-demo:demos', '_filter_theme_fw_ext_backups_demos');
}