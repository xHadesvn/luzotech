<?php 

/**
 * Grab framework options as registered by the theme.
 * If "qodx_fw_options" isn't set then we'll pull a default list where all options are turned off.
 */
$defaults = array(
	'portfolio_post_type'   => '0',
	'team_post_type'        => '0',
);
$fw_options = wp_parse_args( get_option('qodx_fw_options'), $defaults);

/**
 * Register Portfolio Post Type
 */
if( '1' == $fw_options['portfolio_post_type'] ){
	add_action( 'init', 'qodx_fw_register_portfolio', 10 );
	add_action( 'init', 'qodx_fw_register_portfolio_taxonomies', 10  );
}

/**
 * Register Team Post Type
 */
if( '1' == $fw_options['team_post_type'] ){
	add_action( 'init', 'qodx_fw_register_team', 10  );
	add_action( 'init', 'qodx_fw_register_team_taxonomies', 10  );
}