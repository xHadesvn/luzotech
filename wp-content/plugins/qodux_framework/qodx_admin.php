<?php

require_once get_template_directory() . '/framework/functions/theme-functions.php';
$theme = new Qodux_FrameFiles();
$theme->qodux_init(array());
/**
 * Used for the theme's initialization.
 */
class Qodux_FrameFiles {

	function qodux_init($options) {
		
		/* Define theme's constants. */
		$this->qodux_constants($options);
		
		/* Load theme's functions. */
		$this->qodux_functions();
		
		/* Load WhoaThemes shortcodes within Visual Composer Plugin. */
		$this->qodux_visual_composer_extensions();
		
		/* Load admin files. */
        $this->qodux_admin();
		
	}
	
	/**
	 * Defines the constant paths for use within the theme.
	 */
	function qodux_constants($options) {
		define('QODUX_FRAME_NAME', 'WhoaThemes');
		define('QODUX_FRAME_SLUG','whoathemes');
	}
	/**
	 * Loads the core theme functions.
	 */
	function qodux_functions() {
		
		/* Load theme's options. */
		$this->qodux_options();
	}
	/**
	 * Loads the theme options.
	 */
	function qodux_options() {
		global $besmart_options;
		$besmart_options = array();
		$option_files = array(
			'general',
			'blog',
			'portfolio',
			'background',
			'color',
			'fonts',
			'sidebar',
			'footer',
		);
		foreach($option_files as $file){
			$page = include (QODUX_FW_PATH . '/admin/options' . "/" . $file.'.php');
			$besmart_options[$page['name']] = array();
			foreach($page['options'] as $option) {
				if (isset($option['default']) && isset($option['id'])) {
					$besmart_options[$page['name']][$option['id']] = $option['default'];
				}
			}
			$besmart_options[$page['name']] = array_merge((array) $besmart_options[$page['name']], (array) get_option('whoathemes' . '_' . $page['name']));
		}
	}
			
	/**
	 * Extend Visual Composer with WhoaThemes Shortcodes
	 */
	function qodux_visual_composer_extensions() {
		
		if (class_exists('WPBakeryVisualComposerAbstract')) {
					
			require_once (QODUX_FW_PATH . '/shortcodes/wt-visual-composer-extensions.php');
				
		}
		
	}
	
	/**
	 * Load admin files.
	 */
	function qodux_admin() {
		if (is_admin()) {
			require_once (QODUX_FW_PATH . '/admin/admin-panel.php');
			$admin = new qodux_admin();
			$admin->qodux_init();
		}
	}
}