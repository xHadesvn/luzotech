<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**
 * Qodux Framework - admin functions
 *
 * @since     1.0.0
 */


/**
 * Enqueue admin scripts and styles
 *
 * @since     1.0.0
 */
/**
 * Enqueue scripts and styles
 */
if(!( function_exists('qodx_fw_admin_load_scripts') )){
	function qodx_fw_admin_load_scripts(){
		wp_enqueue_style('qodx_fw_admin_styles', plugins_url( '/assets/css/qodx-fw-admin.css' , __FILE__ ) );
		wp_enqueue_script('qodx_fw_admin_scripts', plugins_url( '/assets/js/qodx-fw-admin.js' , __FILE__ ) );
	}
	add_action('admin_enqueue_scripts', 'qodx_fw_admin_load_scripts', 200);
}

function qodux_fw_enqueue_scripts() {
		wp_enqueue_script( 'wt-visual-composer-extensions-front', QODUX_FW_URL. 'shortcodes/assets/wt-visual-composer-extensions-front.js', array('besmart-main', 'besmart-plugins'), null, true);
		wp_enqueue_style( 'wt-visual-composer-extensions-front', QODUX_FW_URL. 'shortcodes/assets/wt-visual-composer-extensions-front.css', array('js_composer_front'), null, 'all');
}
add_action('wp_enqueue_scripts', 'qodux_fw_enqueue_scripts');


add_filter( 'manage_posts_columns', 'qodx_posts_columns', 5 );
add_filter( 'manage_pages_columns', 'qodx_posts_columns', 5 );
add_action( 'manage_posts_custom_column', 'qodx_posts_custom_columns', 5, 2 );
add_action( 'manage_pages_custom_column', 'qodx_posts_custom_columns', 5, 2 );

if ( ! function_exists( 'qodx_posts_columns' ) ) {
	function qodx_posts_columns( $cols ){
		$cols['qodx_post_thumb'] = esc_html__( 'Featured Image', 'qodux' );
		return $cols;
	}
}

if ( ! function_exists( 'qodx_posts_custom_columns' ) ) {
	function qodx_posts_custom_columns( $col, $id ){
		if ( $col != 'qodx_post_thumb' ) {
			return;
		}
		if ( has_post_thumbnail( $id ) ) {
			$img_src = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'thumbnail', false );
			if ( ! empty( $img_src[0] ) ) { ?>
					<img src="<?php echo esc_url( $img_src[0] ); ?>" alt="<?php qodx_esc_title(); ?>" style="max-width:100%;max-height:75px;" />
				<?php
			}
		} else {
			echo '&mdash;';
		}
	}
}