<?php

/**
 * Register Portfolio custom post type
 *
 * @todo Rewrite portfolio permalink slug from theme options
 */
function qodx_fw_register_portfolio() {
 
	$slug = 'wt_portfolio';

    $labels = array( 
        'name' 				 => esc_html_x( 'Portfolio', 'post type general name', 'qodux' ),
        'singular_name'		 => esc_html_x( 'Portfolio', 'post type singular name', 'qodux' ),
        'add_new'			 => esc_html__( 'Add New', 'qodux' ),
        'add_new_item'		 => esc_html__( 'Add New Portfolio', 'qodux' ),
        'edit_item'			 => esc_html__( 'Edit Portfolio', 'qodux' ),
        'new_item'           => esc_html__( 'New Portfolio', 'qodux' ),
        'view_item'          => esc_html__( 'View Portfolio', 'qodux' ),
        'search_items'       => esc_html__( 'Search Portfolios', 'qodux' ),
        'not_found'          => esc_html__( 'No portfolios found', 'qodux' ),
        'not_found_in_trash' => esc_html__( 'No portfolios found in Trash', 'qodux' ),
        'parent_item_colon'  => esc_html__( 'Parent Portfolio:', 'qodux' ),
        'menu_name' 		 => esc_html__( 'Portfolio', 'qodux' ),
    );

    $args = array( 
        'labels' 			  => $labels,
        'hierarchical'  	  => false,
        'description'   	  => esc_html__( 'Portfolio entries for the Qodux Themes.', 'qodux' ),
        'supports' 			  => array( 'title', 'editor', 'thumbnail', 'excerpt', 'page-attributes', 'post-formats', 'comments' ),
        'public' 			  => true,
        'show_ui' 			  => true,
        'show_in_menu'  	  => true,
        'menu_position'		  => 5,
        'menu_icon'     	  => 'dashicons-portfolio',
		        
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'query_var' 		  => true,
        'can_export' 		  => true,
        'rewrite' 			  => array( 'slug' => $slug ),
        'capability_type'	  => 'post'
    );

    register_post_type( 'wt_portfolio', $args );
}

/**
 * Register taxonomies for portfolio custom post type
 */
function qodx_fw_register_portfolio_taxonomies(){
	
	$labels = array(
	    'name'              => esc_html_x( 'Portfolio Categories', 'taxonomy general name', 'qodux' ),
	    'singular_name'     => esc_html_x( 'Portfolio Category', 'taxonomy singular name', 'qodux' ),
	    'search_items'      => esc_html__( 'Search Portfolio Categories', 'qodux' ),
	    'popular_items'     => esc_html__( 'Popular Portfolio Categories', 'qodux' ),
	    'all_items'         => esc_html__( 'All Portfolio Categories', 'qodux' ),
	    'parent_item'       => esc_html__( 'Parent Portfolio Category', 'qodux' ),
	    'parent_item_colon' => esc_html__( 'Parent Portfolio Category:', 'qodux' ),
	    'edit_item'         => esc_html__( 'Edit Portfolio Category', 'qodux' ), 
	    'update_item'       => esc_html__( 'Update Portfolio Category', 'qodux' ),
	    'add_new_item'      => esc_html__( 'Add New Portfolio Category', 'qodux' ),
	    'new_item_name'     => esc_html__( 'New Portfolio Category Name', 'qodux' ),
	    'menu_name'         => esc_html__( 'Portfolio Categories', 'qodux' ),
	  );
	   	
	register_taxonomy('wt_portfolio_category', array('portfolio'), array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => true,
	));
	
}

/**
 * Register Team custom post type
 *
 * @todo Rewrite team permalink slug from theme options
 */
function qodx_fw_register_team() {
 
	$slug = 'team';

    $labels = array( 
        'name' 				 => esc_html_x( 'Team Members', 'post type general name', 'qodux' ),
        'singular_name' 	 => esc_html_x( 'Team Member', 'post type singular name', 'qodux' ),
        'add_new' 			 => esc_html__( 'Add New', 'qodux' ),
        'add_new_item' 	 	 => esc_html__( 'Add New Team Member', 'qodux' ),
        'edit_item' 		 => esc_html__( 'Edit Team Member', 'qodux' ),
        'new_item' 			 => esc_html__( 'New Team Member', 'qodux' ),
        'view_item' 		 => esc_html__( 'View Team Member', 'qodux' ),
        'search_items'  	 => esc_html__( 'Search Team Members', 'qodux' ),
        'not_found' 		 => esc_html__( 'No Team Members found', 'qodux' ),
        'not_found_in_trash' => esc_html__( 'No Team Members found in Trash', 'qodux' ),
        'parent_item_colon'  => esc_html__( 'Parent Team Member:', 'qodux' ),
        'menu_name' 		 => esc_html__( 'Team Members', 'qodux' ),
    );

    $args = array( 
        'labels' 			  => $labels,
        'hierarchical'		  => false,
        'description' 		  => esc_html__('Team Member entries for the Qodux Themes.', 'qodux'),
        'supports' 			  => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
        'public' 			  => true,
        'show_ui' 			  => true,
        'show_in_menu' 		  => true,
        'menu_position'		  => 5,
        'menu_icon' 		  => 'dashicons-groups',
		
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive' 		  => true,
        'query_var' 		  => true,
        'can_export' 		  => true,
        'rewrite' 			  => array( 'slug' => $slug ),
        'capability_type' 	  => 'post'
    );

    register_post_type( 'team', $args );
}

/**
 * Register taxonomies for team custom post type
 */
function qodx_fw_register_team_taxonomies(){
	
	$labels = array(
		'name'			 	=> esc_html_x( 'Team Categories', 'taxonomy general name', 'qodux' ),
		'singular_name' 	=> esc_html_x( 'Team Category', 'taxonomy singular name', 'qodux' ),
		'search_items' 		=> esc_html__( 'Search Team Categories', 'qodux' ),
		'all_items' 		=> esc_html__( 'All Team Categories', 'qodux' ),
		'parent_item' 		=> esc_html__( 'Parent Team Category', 'qodux' ),
		'parent_item_colon' => esc_html__( 'Parent Team Category:', 'qodux' ),
		'edit_item'		    => esc_html__( 'Edit Team Category', 'qodux' ), 
		'update_item' 		=> esc_html__( 'Update Team Category', 'qodux' ),
		'add_new_item' 		=> esc_html__( 'Add New Team Category', 'qodux' ),
		'new_item_name'		=> esc_html__( 'New Team Category Name', 'qodux' ),
		'menu_name' 		=> esc_html__( 'Team Categories', 'qodux' ),
	); 
		
	register_taxonomy('team_category', array('team'), array(
		'hierarchical'		=> true,
		'labels' 			=> $labels,
		'show_ui' 			=> true,
		'show_admin_column' => true,
		'query_var' 		=> true,
		'rewrite'			=> true,
	));
  
}