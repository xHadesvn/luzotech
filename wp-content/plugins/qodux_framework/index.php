<?php

/**
 * Plugin Name:   Qodux Framework
 * Plugin URI:    http://qodux.com
 * Description:   A flexible WordPress Framework for Qodux Themes
 * Author:        Qodux
 * Author URI:    http://qodux.com
 * Version:       1.0.0
 * Text Domain:   qodux
 
 */
 
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}	

/**
 * Returns the main instance of Qodux_Framework to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object Qodux_Framework
 */
function Qodux_Framework() {
	return Qodux_Framework::instance();
} // End Qodux_Framework()

add_action( 'plugins_loaded', 'Qodux_Framework' );

/**
 * Main Qodux_Framework Class
 *
 * @class Qodux_Framework
 * @since 1.0.0
 */
final class Qodux_Framework {
	/**
	 * Qodux_Framework - The single instance of Qodux_Framework.
	 * @var 	object
	 * @since 	1.0.0
	 */
	private static $_instance = null;

	/**
	 * The token.
	 * @var     string
	 * @since   1.0.0
	 */
	public $token;

	/**
	 * The version number.
	 * @var     string
	 * @since   1.0.0
	 */
	public $version;

	/**
	 * The plugin directory URL.
	 * @var     string
	 * @since   1.0.0
	 */
	public $plugin_url;

	/**
	 * The plugin directory path.
	 * @var     string
	 * @since   1.0.0
	 */
	public $plugin_path;
	
	// Admin - Start
	/**
	 * The admin object.
	 * @var     object
	 * @since   1.0.0
	 */
	public $admin;

	/**
	 * The settings object.
	 * @var     object
	 * @since   1.0.0
	 */
	public $settings;
	// Admin - End

	// Post Types - Start
	/**
	 * The post types we're registering.
	 * @var     array
	 * @since   1.0.0
	 */
	public $post_types = array();
	// Post Types - End
	
	/**
	 * Constructor function.
	 * @since   1.0.0
	 */
	public function __construct () {
		$this->token 			= 'qodux-framework';
		$this->plugin_url 		= plugin_dir_url( __FILE__ );
		$this->plugin_path 		= plugin_dir_path( __FILE__ );
		$this->version 			= '1.0.0';
				
		/* setup the constants */
		$this->constants();		
		
        /* include the required admin files */
        $this->admin_includes();	
		
		/* include the required files */
		$this->includes();	
		
		/**
		* Grab custom post type functions
		*/
		require_once( QODUX_FW_PATH . 'qodx_posttypes.php' );
		
		/**
		* Include plugin features depending on theme options
		*/
		require_once( QODUX_FW_PATH . 'init.php' );
		
		/**
		* Grab theme admin
		*/
		require_once( QODUX_FW_PATH . 'qodx_admin.php' );
		
		/* include unyson QODUX_FW_PATH */
		require_once( QODUX_FW_PATH . 'unyson/unyson.php' );
	
		register_activation_hook( __FILE__, array( $this, 'install' ) );

		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );
		
	} // End __construct()

	/**
	 * Main Qodux_Framework Instance
	 *
	 * Ensures only one instance of Qodux_Framework is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Qodux_Framework()
	 * @return Main Qodux_Framework instance
	 */
	public static function instance () {
		if ( is_null( self::$_instance ) )
			self::$_instance = new self();
		return self::$_instance;
	} // End instance()
	
	/**
     * Constants
     *
     * Defines the constants for use within plugin. Constants 
     * are prefixed with 'QODUX_' to avoid any naming collisions.
     *
     * @return    void
	 * @since   1.0.0
	 */
	private function constants() {	
		define( 'QODUX_FW_PATH', trailingslashit(plugin_dir_path(__FILE__)) );
		
		define( 'QODUX_FW_URL', trailingslashit(plugin_dir_url(__FILE__)) );
		
		define( 'QODUX_FW_VERSION', '1.0.0');
	} // End constants()

	/**
	 * Load the localisation file.
	 * @since   1.0.0
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain( 'qodux_fw', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	} // End load_plugin_textdomain()
	
	/**
     * Include admin files
     *
     * These functions are included on admin pages only.
     *
     * @since     1.0.0
     */
    private function admin_includes() {
      
		require_once( QODUX_FW_PATH . 'qodx-admin-functions.php' );
            
    }
	
	/**
     * Include front-end files
     *
     * These functions are included on every page load 
     * in case other plugins need to access them.
     *
	 * @since   1.0.0
     */
    private function includes() {
	
	/**
	* Grab general functions
	*/
		require_once( QODUX_FW_PATH . 'qodx_functions.php' );
      
    }    
    
    /**
     * Load a file
     *
     * @return    void
     * @since     1.0.0
     */
    private function load_file( $file ){
      
      include_once( $file );
      
    }
		
	/**
	 * Cloning is forbidden.
	 * @since 1.0.0
	 */
	public function __clone () {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), $this->version );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 * @since 1.0.0
	 */
	public function __wakeup () {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), $this->version );
	} // End __wakeup()

	/**
	 * Installation. Runs on activation.
	 * @since   1.0.0
	 */
	public function install () {
		$this->_log_version_number();
	} // End install()

	/**
	 * Log the plugin version number.
	 * @access  private
	 * @since   1.0.0
	 */
	private function _log_version_number () {
		// Log the version number.
		update_option( $this->token . '-version', $this->version );
	} // End _log_version_number()
} // End Class
	