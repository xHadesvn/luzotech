<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Declare vars
$el_class = $full_height = $parallax_speed_bg = $parallax_speed_video = $full_width = $equal_height = $flex_row = $columns_placement = $content_placement = $parallax = $parallax_image = $css = $el_id = $video_bg = $video_bg_url = $video_bg_parallax = '';
$output = $after_output = '';

// Get vc attributes
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );

// Extract shortcode atts
extract( $atts );

wp_enqueue_script( 'wpb_composer_front_js' );

// VC core classes
$el_class = $this->getExtraClass( $el_class );

$css_classes = array(
	'vc_row',
	'wpb_row', //deprecated
	'vc_row-fluid',
	$el_class,
	vc_shortcode_custom_css_class( $css ),
);

if (vc_shortcode_custom_css_has_property( $css, array('border', 'background') ) || $video_bg || $parallax) {
	$css_classes[]='vc_row-has-fill';
}

if (!empty($atts['gap'])) {
	$css_classes[] = 'vc_column-gap-'.$atts['gap'];
}

$wrapper_attributes = array();
// build attributes for wrapper

// VC core custom id
if ( ! empty( $el_id ) ) {
	$wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
}

if ( ! empty( $full_width ) ) {
	$wrapper_attributes[] = 'data-vc-full-width="true"';
	$wrapper_attributes[] = 'data-vc-full-width-init="false"';
	if ( 'stretch_row_content' === $full_width ) {
		$wrapper_attributes[] = 'data-vc-stretch-content="true"';
	} elseif ( 'stretch_row_content_no_spaces' === $full_width ) {
		$wrapper_attributes[] = 'data-vc-stretch-content="true"';
		$css_classes[] = 'vc_row-no-padding';
	}
	$after_output .= '<div class="vc_row-full-width vc_clearfix"></div>';
}

if ( ! empty( $full_height ) ) {
	$css_classes[] = 'vc_row-o-full-height';
	if ( ! empty( $columns_placement ) ) {
		$flex_row = true;
		$css_classes[] = 'vc_row-o-columns-' . $columns_placement;
		if ( 'stretch' === $columns_placement ) {
			$css_classes[] = 'vc_row-o-equal-height';
		}
	}
}

if ( ! empty( $equal_height ) ) {
	$flex_row = true;
	$css_classes[] = 'vc_row-o-equal-height';
}

if ( ! empty( $content_placement ) ) {
	$flex_row = true;
	$css_classes[] = 'vc_row-o-content-' . $content_placement;
}

if ( ! empty( $flex_row ) ) {
	$css_classes[] = 'vc_row-flex';
}

$has_video_bg = ( ! empty( $video_bg ) && ! empty( $video_bg_url ) && vc_extract_youtube_id( $video_bg_url ) );

$parallax_speed = $parallax_speed_bg;

if ( $has_video_bg ) {
	$parallax = $video_bg_parallax;
	$parallax_speed = $parallax_speed_video;
	$parallax_image = $video_bg_url;
	$css_classes[] = 'vc_video-bg-container';
	wp_enqueue_script( 'vc_youtube_iframe_api_js' );
}

if ( ! empty( $parallax ) ) {
	wp_enqueue_script( 'vc_jquery_skrollr_js' );
	$wrapper_attributes[] = 'data-vc-parallax="' . esc_attr( $parallax_speed ) . '"'; // parallax speed
	$css_classes[] = 'vc_general vc_parallax vc_parallax-' . $parallax;
	if ( false !== strpos( $parallax, 'fade' ) ) {
		$css_classes[] = 'js-vc_parallax-o-fade';
		$wrapper_attributes[] = 'data-vc-parallax-o-fade="on"';
	} elseif ( false !== strpos( $parallax, 'fixed' ) ) {
		$css_classes[] = 'js-vc_parallax-o-fixed';
	}
}

if ( ! empty( $parallax_image ) ) {
	if ( $has_video_bg ) {
		$parallax_image_src = $parallax_image;
	} else {
		$parallax_image_id = preg_replace( '/[^\d]/', '', $parallax_image );
		$parallax_image_src = wp_get_attachment_image_src( $parallax_image_id, 'full' );
		if ( ! empty( $parallax_image_src[0] ) ) {
			$parallax_image_src = $parallax_image_src[0];
		}
	}
	$wrapper_attributes[] = 'data-vc-parallax-image="' . esc_attr( $parallax_image_src ) . '"';
}
if ( ! $parallax && $has_video_bg ) {
	$wrapper_attributes[] = 'data-vc-video-bg="' . esc_attr( $video_bg_url ) . '"';
}

$css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( array_unique( $css_classes ) ) ), $this->settings['base'], $atts ) );
$wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

/* ---- QODUX => Start extensions ----- */

// Animations
$this->wt_sc = new WT_VCSC_SHORTCODE;
$anim_class  = $this->wt_sc->getWTCSSAnimationClass($css_animation,$anim_type);
$anim_data   = $this->wt_sc->getWTCSSAnimationData($css_animation,$anim_delay);

// WT row container classes
$qodx_wrap_classes = array(
	'wt-row-container',
);

if ( $center_row == 'yes' ) {
	$qodx_wrap_classes[] = 'wt-row-centered'; 
}

if ( $full_mobile_row == 'yes' ) {
	$qodx_wrap_classes[] = 'wt-row-full-mobile'; 
}

if ( $bg_type != '' ) {
	$qodx_wrap_classes[] = 'wt-background-' . $bg_type; 
} elseif ($bg_type == '' && $bck_color) {
	$qodx_wrap_classes[] = 'wt-background'; 
}

if ( $default_bg == 'yes' ) {
	$qodx_wrap_classes[] = 'wt_row_default_bg';
}

if ( $default_skin_bg == 'yes' ) {
	$qodx_wrap_classes[] = 'wt_skin_bg_color';
}

if ( $default_border == 'yes' ) {
	$qodx_wrap_classes[] = 'wt_row_default_border';
}

if ( $shadow == 'yes' ) {
	$qodx_wrap_classes[] = 'wt_row_shadow';
}

if ( $typography == 'light' ) {
	$qodx_wrap_classes[] = 'wt_skin_light';
}

$qodx_wrap_classes[] = $anim_class;

$qodx_wrap_classes = implode( ' ', $qodx_wrap_classes );

$bg_size = esc_html($bg_size);

// BG image
if ( $bg_type != '' && $bg_type != 'youtube' ) {
	if ( $bck_image ) { // if background image is not empty
		$img_id = preg_replace('/[^\d]/', '', $bck_image);
		$img = wp_get_attachment_image_src( $img_id, $bg_size);
		$bg_img_url = $img[0];
	}
} else {
	$bg_img_url       = NULL;
	$bg_position      = NULL;
	$bg_size_standard = NULL;
	$bg_repeat        = NULL;
}

// Style tag for background, margin
$add_style = array();

	if ( $min_height ) {
		$add_style[] = 'min-height: '. $min_height .'px;';
	}
	
	if ( $bck_color ) {
		$add_style[] = 'background-color: '. $bck_color .';';
	}	

	if ( $bg_img_url ) {
		$add_style[] = 'background-image: url('. $bg_img_url .');';
	}
	
	if ( $bg_position ) {
		$add_style[] = 'background-position: '. $bg_position .' center;';
	}
	
	if ( $bg_size_standard ) {
		$add_style[] = '-webkit-background-size: '. $bg_size_standard .';-moz-background-size: '. $bg_size_standard .';-o-background-size: '. $bg_size_standard .';background-size: '. $bg_size_standard .';';
	}
	
	if ( $bg_repeat ) {
		$add_style[] = 'background-repeat: '. $bg_repeat .';';
	}
	
	if ( $border_color && $border_style && $border_width ) {
		$add_style[] = 'border-color: '. $border_color .';';
		$add_style[] = 'border-style: '. $border_style .';';
		$add_style[] = 'border-width: '. $border_width .';';
	}		
	
	if ( $margin_top ) {
		$add_style[] = 'margin-top: ' . intval($margin_top) . 'px;';
	}
	
	if ( $margin_bottom ) {
		$add_style[] = 'margin-bottom: ' . intval($margin_bottom) . 'px;';
	}

// Style tag for paddings
$add_style_padd = array(); 

	if ( $padding_top ) {
		$add_style_padd[] = 'padding-top: ' . intval($padding_top) . 'px;';
	}
	
	if ( $padding_bottom ) {
		$add_style_padd[] = 'padding-bottom: ' . intval($padding_bottom) . 'px;';
	}
	
	if ( $padding_left ) {
		$add_style_padd[] = 'padding-left: ' . intval($padding_left) . 'px;';
	}
	
	if ( $padding_right ) {
		$add_style_padd[] = 'padding-right: ' . intval($padding_right) . 'px;';
	}

$add_style      = implode('', $add_style);
$add_style_padd = implode('', $add_style_padd);

if ( $add_style ) {
	$add_style = wp_kses( $add_style, array() );
	$add_style = ' style="' . esc_attr($add_style) . '"';
}

if ( $add_style_padd ) {
	$add_style_padd = wp_kses( $add_style_padd, array() );
	$wrapper_attributes[] = ' style="'. esc_attr($add_style_padd) .'"';
}

// Overlay
$overlay         = '';	
$overlay_color   = '';
$overlay_pattern = '';

if ( $bg_pattern_overlay != '' ) {
	$overlay_pattern = ' wt_row_pattern_'.$bg_pattern_overlay;	
}

if ( $bg_color_overlay != '' ) {
	$overlay_color = ' style="background-color:'.$bg_color_overlay.'"';	
}

if ( $bg_color_overlay != '' || $bg_pattern_overlay != '' ) {
	$overlay = '<span class="wt_row_overlay'.$overlay_pattern.'"'.$overlay_color.'></span>';	
}

// Video bck type
$output_video = '';	

if ( $bg_type == 'youtube' && $youtube_video_id != '' ) {
	wp_enqueue_style('wt-extend-youtube-player');
	wp_enqueue_script('wt-extend-youtube-player');
	
	if ( $yt_video_controls == 'yes' ) {
		$yt_controls = 'true';
	} else {
		$yt_controls = 'false';
	}		
	
	if ( $yt_video_bg_img ) { // if video background image is not empty
		$yt_img_id = preg_replace('/[^\d]/', '', $yt_video_bg_img);
		$yt_img = wp_get_attachment_image_src( $yt_img_id, 'full');
		$yt_bg_img_url = $yt_img[0];		
		$output_video .= '<div class="wt_row_bg_video_mobile" style="background-image: url('.$yt_bg_img_url.');"></div>'; 
	}
	
	$output_video .= '<div class="wt-youtube-bg-wrap wt-youtube-video-'. $id .'">';
	$output_video .= $overlay; // Overlay for videos
	$output_video .= '<a id="youtube-bg-video_'. $id .'" class="wt_youtube_player" data-property="{videoURL:\'http://www.youtube.com/watch?v='. $youtube_video_id .'\', containment:\'.wt-youtube-video-'. $id .'\', showControls:'.$yt_controls.', autoPlay:true, loop:true, mute:true, startAt:0, opacity:1, ratio:\'4/3\', addRaster:false, quality:\'default\'}"></a>';
	$output_video .= '</div>';
} elseif ( $bg_type == 'html_video' && $html_mp4 != '' ) {
	
	if ( $html_video_bg_img ) { // if video background image is not empty
		$html_img_id = preg_replace('/[^\d]/', '', $html_video_bg_img);
		$html_img = wp_get_attachment_image_src( $html_img_id, 'full');
		$html_bg_img_url = $html_img[0];		
		//$output_video .= '<div class="wt_row_bg_video_mobile" style="background-image: url('.$html_bg_img_url.');"></div>'; 
	}
	
	$output_video .= '<video autoplay="" loop="" muted="" class="bgvid" poster="'.$html_bg_img_url.'">';
	$output_video .= '<source src="'.$html_mp4.'" type="video/mp4">';
	$output_video .= '<source src="'.$html_wbm.'" type="video/webm">';
	$output_video .= '</video>';
}

/* ---- QODUX => End extensions ----- */

// Open wt row container
$output .= '<div class="' . esc_attr( trim( $qodx_wrap_classes ) ) . '"' . $anim_data . $add_style .'>';

	$output .= $output_video;	
	
	if ( $bg_type != 'youtube' ) {
		$output .= $overlay; // Overlay for images
	} 

	$output .= '<div ' . implode( ' ', $wrapper_attributes ) . '>';
		// Center the row
		if ( $center_row == 'yes' ) {
			$output .= '<div class="container">';
				$output .= '<div class="row">';
		}
		
		$output .= wpb_js_remove_wpautop( $content );
		
		// Center the row
		if ( $center_row == 'yes' ) {
				$output .= '</div>'; // End Row
			$output .= '</div>'; // End container
		}
	$output .= '</div>';
	$output .= $after_output;
	
// Close wt row container
$output .= '</div>';

echo $output;