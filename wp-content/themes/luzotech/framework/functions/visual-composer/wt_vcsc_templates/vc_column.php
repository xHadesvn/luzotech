<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Declare vars
$el_class = $width = $css = $offset = '';
$output = '';

// Get vc attributes
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );

// Extract shortcode atts
extract( $atts );

$width = wpb_translateColumnWidthToSpan( $width );
$width = vc_column_offset_class_merge( $offset, $width );

// VC core classes
$css_classes = array(
	$this->getExtraClass( $el_class ),
	'wpb_column',
	'vc_column_container',
	$width,
);

if (vc_shortcode_custom_css_has_property( $css, array('border', 'background') )) {
	$css_classes[]='vc_col-has-fill';
}

$wrapper_attributes = array();
// build attributes for wrapper

// VC core custom id
if ( ! empty( $el_id ) ) {
	$wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
}

// Create design class
if ( $css ) {
	$design_class = ' '. trim( vc_shortcode_custom_css_class( $css ) );
} else {
	$design_class = '';
}

/* ---- QODUX => Start extensions ----- */

// Animations
$this->wt_sc = new WT_VCSC_SHORTCODE;
$anim_class  = $this->wt_sc->getWTCSSAnimationClass($css_animation,$anim_type);
$anim_data   = $this->wt_sc->getWTCSSAnimationData($css_animation,$anim_delay);

// WT column wrapper classes
$qodx_wrap_classes = array(
	'wt_wpb_wrapper',
	'clearfix',
);

if ( $bg_type != '' ) {
	$qodx_wrap_classes[] = 'wt-background-' . $bg_type; 
} elseif ($bg_type == '' && $bck_color) {
	$qodx_wrap_classes[] = 'wt-background'; 
}

if ( $style != '' ) {
	$qodx_wrap_classes[] = 'wt_column_' . $style;
}

if ( $shadow == 'yes' ) {
	$qodx_wrap_classes[] = 'wt_column_shadow';
}

if ( $typography == 'light' ) {
	$qodx_wrap_classes[] = 'wt_skin_light';
}

$bg_size = esc_html($bg_size);

// BG image
if ( $bg_type != '' ) {
	if ( $bck_image ) { // if background image is not empty
		$img_id = preg_replace('/[^\d]/', '', $bck_image);
		$img = wp_get_attachment_image_src( $img_id, $bg_size);
		$bg_img_url = $img[0];
	}
} else {
	$bg_img_url       = NULL;
	$bg_position      = NULL;
	$bg_size_standard = NULL;
	$bg_repeat        = NULL;
}

// Style tag for background, margin
$add_style = array();
	
	if ( $bck_color ) {
		$add_style[] = 'background-color: '. $bck_color .';';
	}	

	if ( $bg_img_url ) {
		$add_style[] = 'background-image: url('. $bg_img_url .');';
	}
	
	if ( $bg_position ) {
		$add_style[] = 'background-position: '. $bg_position .' center;';
	}
	
	if ( $bg_size_standard ) {
		$add_style[] = '-webkit-background-size: '. $bg_size_standard .';-moz-background-size: '. $bg_size_standard .';-o-background-size: '. $bg_size_standard .';background-size: '. $bg_size_standard .';';
	}
	
	if ( $bg_repeat ) {
		$add_style[] = 'background-repeat: '. $bg_repeat .';';
	}
	
	if ( $border_color && $border_style && $border_width ) {
		$add_style[] = 'border-color: '. $border_color .';';
		$add_style[] = 'border-style: '. $border_style .';';
		$add_style[] = 'border-width: '. $border_width .';';
	}		
	
	if ( $margin_top ) {
		$add_style[] = 'margin-top: ' . intval($margin_top) . 'px;';
	}
	
	if ( $margin_bottom ) {
		$add_style[] = 'margin-bottom: ' . intval($margin_bottom) . 'px;';
	}

// Style inline paddings
$inline_style = array();

	if ( $min_height ) {
		$inline_style[] = 'min-height: '. $min_height .'px;';
	} 

	if ( $padding_top ) {
		$inline_style[] = 'padding-top: ' . intval($padding_top) . 'px;';
	}
	
	if ( $padding_bottom ) {
		$inline_style[] = 'padding-bottom: ' . intval($padding_bottom) . 'px;';
	}
	
	if ( $padding_left ) {
		$inline_style[] = 'padding-left: ' . intval($padding_left) . 'px;';
	}
	
	if ( $padding_right ) {
		$inline_style[] = 'padding-right: ' . intval($padding_right) . 'px;';
	}

$add_style      = implode('', $add_style);
$inline_style   = implode('', $inline_style);

if ( $add_style || $inline_style ) {
	$add_style    = wp_kses( $add_style, array() );
	$inline_style = wp_kses( $inline_style, array() );
	
	$inline_style = ' style="' . esc_attr($add_style) . esc_attr($inline_style) . '"';
}

/* ---- QODUX => End extensions ----- */

// Wrapper classes and styles
$qodx_wrap_classes = implode( ' ', $qodx_wrap_classes ) . $anim_class;

$css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( array_unique( $css_classes ) ) ), $this->settings['base'], $atts ) );
$wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

$output .= '<div ' . implode( ' ', $wrapper_attributes ) . '>';
$output .= '<div class="vc_column-inner clearfix">';
$output .= '<div class="wpb_wrapper ' . esc_attr( $qodx_wrap_classes ) . esc_attr( $design_class ) . '"'  . $anim_data . $inline_style .'>';
$output .= wpb_js_remove_wpautop( $content );
$output .= '</div>';
$output .= '</div>';
$output .= '</div>';

echo $output;