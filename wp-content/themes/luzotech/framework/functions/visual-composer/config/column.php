<?php
/**
 * Visual Composer Column Configuration
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Global var for class
global $qodx_vc_col_config;

// Start Class
if ( ! class_exists( 'QODX_VC_Col_Config' ) ) {
	
	class QODX_VC_Col_Config {

		/**
		 * Main constructor
		 *
		 */
		public function __construct() {

			// Add new parameters to the column
			add_action( 'init', array( $this, 'add_remove_params' ) );

		}

		/**
		 * Adds new params for the VC Columns
		 *
		 */
		public function add_remove_params() {

			// Array of params to add
			$add_params = array();
			
			$add_params['el_id'] = array(
				'type'                          => 'textfield',
				'heading'                       => esc_html__( 'Column ID', 'besmart' ),
				'param_name'                    => 'el_id',
				'description'                   => sprintf( esc_html__( 'Enter column ID (Note: make sure it is unique and valid according to', 'besmart' ) . ' <a href="%s" target="_blank">' . esc_html__( 'w3c specification', 'besmart' ) . '</a>).', 'http://www.w3schools.com/tags/att_global_id.asp' )
			);			
			$add_params['separator'] = array(
				'type'              			=> 'wt_separator',
				'heading'           			=> '',
				'param_name'        			=> 'separator',
				'separator'             		=> 'Background Extended Settings',
				'description'       			=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['min_height'] = array(
				'type'                  		=> 'qodux_range',
				'heading'               		=> esc_html__( 'Minimum Height', 'besmart' ),
				'param_name'            		=> 'min_height',
				'value'                 		=> '0',
				'min'                   		=> '0',
				'max'                   		=> '2048',
				'step'                  		=> '1',
				'unit'                  		=> 'px',
				'description'           		=> esc_html__( 'Define the minimum height for this column.', 'besmart' ),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['style'] = array(
				'type'							=> 'dropdown',
				'heading'						=> esc_html__( 'Column Style', 'besmart' ),
				'param_name'					=> 'style',
				'value'							=> array(
					esc_html__( 'None', 'besmart' )     => '',
					esc_html__( 'Bordered', 'besmart' ) => 'bordered',
					esc_html__( 'Boxed', 'besmart' )    => 'boxed',
				),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['shadow'] = array(
				'type'							=> 'checkbox',
				'heading'						=> esc_html__( 'Drop Shadow?', 'besmart' ),
				'param_name'					=> 'shadow',
				'value'							=> Array( esc_html__( 'Yes please.', 'besmart' ) => 'yes' ),
				'description'           		=> esc_html__( 'Check this option to add a default shadow to this column.', 'besmart' ),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['typography'] = array(
				'type' 							=> 'dropdown',
				'heading' 						=> esc_html__( 'Typography Style', 'besmart' ),
				'param_name' 					=> 'typography',
				'value' 						=> array(
					esc_html__( 'Dark Text', 'besmart' )	=> 'dark',
					esc_html__( 'White Text', 'besmart' )	=> 'light'
				),
				'description' 					=> esc_html__( 'Select typography style.', 'besmart' ),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['bck_color'] = array(
				'type'                          => 'colorpicker',
				'heading'                       => esc_html__( 'Background Color', 'besmart' ),
				'param_name'                    => 'bck_color',
				'description'                   => esc_html__( 'Select background color for this column.', 'besmart' ),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['bg_type'] = array(
				'type' 							=> 'dropdown',
				'heading' 						=> esc_html__( 'Background Type', 'besmart' ),
				'param_name' 					=> 'bg_type',
				'value' 						=> array(
					esc_html__( 'None', 'besmart' )			  => '',
					esc_html__( 'Simple Image', 'besmart' )	  => 'image',
					esc_html__( 'Fixed Image', 'besmart' )	  => 'fixed',
					esc_html__( 'Parallax Image', 'besmart' ) => 'parallax',
				),
				'admin_label' 					=> true,
				'description' 					=> esc_html__( 'Select background type for this column.', 'besmart' ),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['bck_image'] = array(
				'type'							=> 'attach_image',
				'heading'						=> esc_html__( 'Background Image', 'besmart' ),
				'param_name'					=> 'bck_image',
				'value'							=> '',
				'description'					=> esc_html__( 'Select the background image for your column.', 'besmart' ),
				'dependency' 					=> array(
					'element' 	=> 'bg_type',
					'value' 	=> array('image', 'fixed', 'parallax')
				),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['bg_size'] = array(
				'type'                  		=> 'dropdown',
				'heading'               		=> esc_html__( 'Background Image Size', 'besmart' ),
				'param_name'            		=> 'bg_size',
				'value'                 		=> array(
					esc_html__( 'Full Size Image', 'besmart' )		=> 'full',
					esc_html__( 'Large Size Image', 'besmart' )		=> 'large',
					esc_html__( 'Medium Size Image', 'besmart' )	=> 'medium',
					esc_html__( 'Thumbnail Size Image', 'besmart' )	=> 'thumbnail',
				),
				'description'           		=> esc_html__( 'Select which image size based on WordPress settings should be used.', 'besmart' ),
				'dependency' 					=> array(
					'element' 	=> 'bg_type',
					'value' 	=> array('image', 'fixed', 'parallax')
				),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['bg_position'] = array(
				'type' 							=> 'dropdown',
				'heading' 						=> esc_html__( 'Background Position', 'besmart' ),
				'param_name' 					=> 'bg_position',
				'value' 						=> array(
					esc_html__( 'Top', 'besmart' )	  => 'top',
					esc_html__( 'Middle', 'besmart' ) => 'center',
					esc_html__( 'Bottom', 'besmart' ) => 'bottom'
				),
				'description' 					=> '',
				'dependency' 					=> array(
					'element' 	=> 'bg_type',
					'value' 	=> array('image', 'fixed', 'parallax')
				),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['bg_size_standard'] = array(
				'type' 							=> 'dropdown',
				'heading' 						=> esc_html__( 'Background Size', 'besmart' ),
				'param_name' 					=> 'bg_size_standard',
				'value' 						=> array(
					esc_html__( 'Cover', 'besmart' ) 	=> 'cover',
					esc_html__( 'Contain', 'besmart' ) 	=> 'contain',
					esc_html__( 'Initial', 'besmart' ) 	=> 'initial',
					esc_html__( 'Auto', 'besmart' ) 	=> 'auto',
				),
				'description' 					=> '',
				'dependency' 					=> array(
					'element' 	=> 'bg_type',
					'value' 	=> array('image', 'fixed', 'parallax')
				),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['bg_repeat'] = array(
				'type' 							=> 'dropdown',
				'heading' 						=> esc_html__( 'Background Repeat', 'besmart' ),
				'param_name' 					=> 'bg_repeat',
				'value' 						=> array(
					esc_html__( 'No Repeat', 'besmart' )	=> 'no-repeat',
					esc_html__( 'Repeat X + Y', 'besmart' )	=> 'repeat',
					esc_html__( 'Repeat X', 'besmart' )		=> 'repeat-x',
					esc_html__( 'Repeat Y', 'besmart' )		=> 'repeat-y'
				),
				'description' 					=> '',
				'dependency' 					=> array(
					'element' 	=> 'bg_type',
					'value' 	=> array('image', 'fixed', 'parallax')
				),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['border_color'] = array(
				'type'							=> 'colorpicker',
				'class'							=> '',
				'heading'						=> esc_html__( 'Border Color', 'besmart' ),
				'param_name'					=> 'border_color',
				'value' 						=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['border_style'] = array(
				'type'							=> 'dropdown',
				'class'							=> '',
				'heading'						=> esc_html__( 'Border Style', 'besmart' ),
				'param_name'					=> 'border_style',
				'value'							=> array(
					esc_html__( 'Solid', 'besmart' )	=> 'solid',
					esc_html__( 'Dotted', 'besmart' )	=> 'dotted',
					esc_html__( 'Dashed', 'besmart' )	=> 'dashed',
				),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['border_width'] = array(
				'type'							=> 'textfield',
				'class'							=> '',
				'heading'						=> esc_html__( 'Border Width', 'besmart' ),
				'param_name'					=> 'border_width',
				'value'							=> '0px 0px 0px 0px',
				'description'					=> wp_kses( __( 'Your border width in pixels. Example: <strong>1px 1px 1px 1px</strong> (top, right, bottom, left).', 'besmart' ), 'strong' ),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			
			// Paddings & Margins
			$add_params['separator_2'] = array(
				'type'              			=> 'wt_separator',
				'heading'           			=> '',
				'param_name'        			=> 'separator_2',
				'separator'             		=> 'Paddings and Margins',
				'description'       			=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['padding_top'] = array(
				'type'                  		=> 'qodux_range',
				'heading'               		=> esc_html__( 'Padding: Top', 'besmart' ),
				'param_name'            		=> 'padding_top',
				'value'                 		=> '0',
				'min'                   		=> '0',
				'max'                   		=> '250',
				'step'                  		=> '1',
				'unit'                  		=> 'px',
				'description'           		=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);			
			$add_params['padding_bottom'] = array(
				'type'                  		=> 'qodux_range',
				'heading'               		=> esc_html__( 'Padding: Bottom', 'besmart' ),
				'param_name'            		=> 'padding_bottom',
				'value'                 		=> '0',
				'min'                   		=> '0',
				'max'                   		=> '250',
				'step'                  		=> '1',
				'unit'                  		=> 'px',
				'description'           		=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['padding_left'] = array(
				'type'                  		=> 'qodux_range',
				'heading'               		=> esc_html__( 'Padding: Left', 'besmart' ),
				'param_name'            		=> 'padding_left',
				'value'                 		=> '0',
				'min'                   		=> '0',
				'max'                   		=> '250',
				'step'                  		=> '1',
				'unit'                  		=> 'px',
				'description'           		=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['padding_right'] = array(
				'type'                  		=> 'qodux_range',
				'heading'               		=> esc_html__( 'Padding: Right', 'besmart' ),
				'param_name'            		=> 'padding_right',
				'value'                 		=> '0',
				'min'                   		=> '0',
				'max'                   		=> '250',
				'step'                  		=> '1',
				'unit'                  		=> 'px',
				'description'           		=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['margin_top'] = array(
				'type'                  		=> 'qodux_range',
				'heading'               		=> esc_html__( 'Margin: Top', 'besmart' ),
				'param_name'            		=> 'margin_top',
				'value'                 		=> '0',
				'min'                   		=> '-250',
				'max'                   		=> '250',
				'step'                  		=> '1',
				'unit'                  		=> 'px',
				'description'           		=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['margin_bottom'] = array(
				'type'                  		=> 'qodux_range',
				'heading'               		=> esc_html__( 'Margin: Bottom', 'besmart' ),
				'param_name'            		=> 'margin_bottom',
				'value'                 		=> '0',
				'min'                   		=> '-250',
				'max'                   		=> '250',
				'step'                  		=> '1',
				'unit'                  		=> 'px',
				'description'           		=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			
			// Animations	
			$add_params['separator_3'] = array(
				'type'              			=> 'wt_separator',
				'heading'           			=> '',
				'param_name'        			=> 'separator_3',
				'separator'             		=> 'Animations',
				'description'       			=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);	
			$add_params['css_animation'] = array(
				'type'                          => 'dropdown',
				'heading'                       => esc_html__( 'CSS WT Animation', 'besmart' ),
				'param_name'                    => 'css_animation',
				'value' => array( esc_html__( 'No', 'besmart' ) => '', esc_html__( 'Hinge', 'besmart' ) => 'hinge', esc_html__( 'Flash', 'besmart' ) => 'flash', esc_html__( 'Shake', 'besmart' ) => 'shake', esc_html__( 'Bounce', 'besmart' ) => 'bounce', esc_html__( 'Tada', 'besmart' ) => 'tada', esc_html__( 'Swing', 'besmart' ) => 'swing', esc_html__( 'Wobble', 'besmart' ) => 'wobble', esc_html__( 'Pulse', 'besmart' ) => 'pulse', esc_html__( 'Flip', 'besmart' ) => 'flip', esc_html__( 'FlipInX', 'besmart' ) => 'flipInX', esc_html__( 'FlipOutX', 'besmart' ) => 'flipOutX', esc_html__( 'FlipInY', 'besmart' ) => 'flipInY', esc_html__( 'FlipOutY', 'besmart' ) => 'flipOutY', esc_html__( 'FadeIn', 'besmart' ) => 'fadeIn', esc_html__( 'FadeInUp', 'besmart' ) => 'fadeInUp', esc_html__( 'FadeInDown', 'besmart' ) => 'fadeInDown', esc_html__( 'FadeInLeft', 'besmart' ) => 'fadeInLeft', esc_html__( 'FadeInRight', 'besmart' ) => 'fadeInRight', esc_html__( 'FadeInUpBig', 'besmart' ) => 'fadeInUpBig', esc_html__( 'FadeInDownBig', 'besmart' ) => 'fadeInDownBig', esc_html__( 'FadeInLeftBig', 'besmart' ) => 'fadeInLeftBig', esc_html__( 'FadeInRightBig', 'besmart' ) => 'fadeInRightBig', esc_html__( 'FadeOut', 'besmart' ) => 'fadeOut', esc_html__( 'FadeOutUp', 'besmart' ) => 'fadeOutUp', esc_html__( 'FadeOutDown', 'besmart' ) => 'fadeOutDown', esc_html__( 'FadeOutLeft', 'besmart' ) => 'fadeOutLeft', esc_html__( 'FadeOutRight', 'besmart' ) => 'fadeOutRight', esc_html__( 'fadeOutUpBig', 'besmart' ) => 'fadeOutUpBig', esc_html__( 'FadeOutDownBig', 'besmart' ) => 'fadeOutDownBig', esc_html__( 'FadeOutLeftBig', 'besmart' ) => 'fadeOutLeftBig', esc_html__( 'FadeOutRightBig', 'besmart' ) => 'fadeOutRightBig', esc_html__( 'BounceIn', 'besmart' ) => 'bounceIn', esc_html__( 'BounceInUp', 'besmart' ) => 'bounceInUp', esc_html__( 'BounceInDown', 'besmart' ) => 'bounceInDown', esc_html__( 'BounceInLeft', 'besmart' ) => 'bounceInLeft', esc_html__( 'BounceInRight', 'besmart' ) => 'bounceInRight', esc_html__( 'BounceOut', 'besmart' ) => 'bounceOut', esc_html__( 'BounceOutUp', 'besmart' ) => 'bounceOutUp', esc_html__( 'BounceOutDown', 'besmart' ) => 'bounceOutDown', esc_html__( 'BounceOutLeft', 'besmart' ) => 'bounceOutLeft', esc_html__( 'BounceOutRight', 'besmart' ) => 'bounceOutRight', esc_html__( 'RotateIn', 'besmart' ) => 'rotateIn', esc_html__( 'RotateInUpLeft', 'besmart' ) => 'rotateInUpLeft', esc_html__( 'RotateInDownLeft', 'besmart' ) => 'rotateInDownLeft', esc_html__( 'RotateInUpRight', 'besmart' ) => 'rotateInUpRight', esc_html__( 'RotateInDownRight', 'besmart' ) => 'rotateInDownRight', esc_html__( 'RotateOut', 'besmart' ) => 'rotateOut', esc_html__( 'RotateOutUpLeft', 'besmart' ) => 'rotateOutUpLeft', esc_html__( 'RotateOutDownLeft', 'besmart' ) => 'rotateOutDownLeft', esc_html__( 'RotateOutUpRight', 'besmart' ) => 'rotateOutUpRight', esc_html__( 'RotateOutDownRight', 'besmart' ) => 'rotateOutDownRight', esc_html__( 'RollIn', 'besmart' ) => 'rollIn', esc_html__( 'RollOut', 'besmart' ) => 'rollOut', esc_html__( 'LightSpeedIn', 'besmart' ) => 'lightSpeedIn', esc_html__( 'LightSpeedOut', 'besmart' ) => 'lightSpeedOut' ),
				'description' => esc_html__( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'besmart' ),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['anim_type'] = array(
				'type'                          => 'dropdown',
				'heading'                       => esc_html__('WT Animation Visible Type', 'besmart' ),
				'param_name'                    => 'anim_type',
				'value'                         => array( esc_html__('Animate when element is visible', 'besmart' ) => 'wt_animate_if_visible', esc_html(__('Animate if element is almost visible', 'besmart')) => 'wt_animate_if_almost_visible' ),
				'description'                   => esc_html__('Select when the type of animation should start for this element.', 'besmart' ),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);
			$add_params['anim_delay'] = array(
				'type'                          => 'textfield',
				'heading'                       => esc_html__('WT Animation Delay', 'besmart'),
				'param_name'                    => 'anim_delay',
				'description'                   => esc_html__('Here you can set a specific delay for the animation (miliseconds). Example: \'100\', \'500\', \'1000\'.', 'besmart'),
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' )
			);	
				
			$add_params['el_file'] = array(
				'type'                  		=> 'wt_loadfile',
				'heading'               		=> '',
				'param_name'            		=> 'el_file',
				'value'                 		=> '',
				'file_type'             		=> 'js',
				'file_path'             		=> 'wt-visual-composer-extend-element.min.js',
				'param_holder_class'            => 'wt_loadfile_field',
				'description'           		=> '',
				'group' 						=> esc_html__( 'WT_VC Extensions', 'besmart' ),
			);
			
			// Loop through array and add new params
			foreach( $add_params as $key => $val ) {
				vc_add_param( 'vc_column', $val );
			}

		}
		
	}

}
$qodx_vc_col_config = new QODX_VC_Col_Config();