<?php
/**
 * Used for the theme's initialization.
 */
class besmart_ThemeFiles {

	function besmart_init($options) {
		
		/* Add theme support. */
		add_action('after_setup_theme', array(&$this, 'besmart_supports'));
		
		/* Load theme's functions. */
		$this->besmart_functions();
		
		/* Load theme's plugin. */
		$this->besmart_plugins();
		
		/* Initialize the theme's widgets. */
		add_action('widgets_init',array(&$this, 'besmart_widgets'));
		
	}
	
	/**
	 * Add theme support.
	 */
	function besmart_supports() {
		if (function_exists('add_theme_support')) {
			
			//This enables post-thumbnail support for a theme.
			add_theme_support('post-thumbnails', array('post', 'page', 'wt_portfolio', 'product'));
			
			//This enables post-thumbnail support for a theme.
			add_theme_support( 'title-tag' );
			
			//This enables the naviagation menu ability. 
			add_theme_support('menus');
			
			register_nav_menus(array(
				'primary-menu' => esc_html__('BeSmart Navigation', 'besmart' ), 
			));
			
			//This enables post and comment RSS feed links to head. This should be used in place of the deprecated automatic_feed_links.
			add_theme_support('automatic-feed-links');
			
			// reference to: http://codex.wordpress.org/Function_Reference/add_editor_style
			add_theme_support('editor-style');
			add_theme_support( 'woocommerce' );

		}
	}
		
	/**
	 * Loads the core theme functions.
	 */
	function besmart_functions() {
		require_once (get_template_directory() . '/framework/functions/theme-functions.php');
		require_once (get_template_directory() . '/framework/includes/theme-features.php');
		require_once (get_template_directory() . '/framework/includes/sidebar.php');
		if(besmart_get_option('general', 'woocommerce')){
			require_once (get_template_directory() . '/framework/functions/woocommerce.php');
			require_once (get_template_directory() . '/framework/functions/woocommerce_config.php');
		}
	}
	
	
	
	/**
	 * Load plugins integrated in a theme.
	 */
	function besmart_plugins() {
		require_once (get_template_directory() . '/framework/plugins/breadcrumbs-plus/breadcrumbs-plus.php');
		require_once (get_template_directory() . '/framework/plugins/class-tgm-plugin-activation.php');
	}
	
	/**
	 * Register theme's extra widgets.
	 */
	function besmart_widgets() {
		/* Load each widget file. */
		require_once (get_template_directory() . '/framework/widgets/subnav.php');
		require_once (get_template_directory() . '/framework/widgets/social.php');
		require_once (get_template_directory() . '/framework/widgets/social_font_awesome.php');
		require_once (get_template_directory() . '/framework/widgets/recent.php');
		require_once (get_template_directory() . '/framework/widgets/popular.php');
		require_once (get_template_directory() . '/framework/widgets/related.php');
		require_once (get_template_directory() . '/framework/widgets/most-read.php');
		require_once (get_template_directory() . '/framework/widgets/recent-portfolio.php');
		require_once (get_template_directory() . '/framework/widgets/popular-portfolio.php');
		require_once (get_template_directory() . '/framework/widgets/contactinfo.php');
		require_once (get_template_directory() . '/framework/widgets/customlinks.php');
		require_once (get_template_directory() . '/framework/widgets/advertisement-125.php');
		
		/* Register each widget. */
		register_widget('besmart_Widget_SubNav');
		register_widget('besmart_Widget_Social');
		register_widget('besmart_Widget_Social_Font_Awesome');
		register_widget('besmart_Widget_Recent_Posts');
		register_widget('besmart_Widget_Popular_Posts');
		register_widget('besmart_Widget_Related_Posts');
		register_widget('besmart_Widget_Most_Read_Posts');
		register_widget('besmart_Widget_Recent_Portfolio_Posts');
		register_widget('besmart_Widget_Popular_Portfolio_Posts');
		register_widget('besmart_Widget_Contact_Info');
		register_widget('besmart_Widget_Custom_Links');
		register_widget('besmart_Widget_Advertisement_125');
	}
}
?>