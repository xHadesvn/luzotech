<?php
class besmart_sidebar {
	var $besmart_sidebar_names = array();
	var $besmart_sidebar_desc = array();
	var $besmart_footer_sidebar_count = 0;
	var $besmart_footer_sidebar_names = array();
	var $besmart_footer_top_sidebar_names = array();
	var $besmart_sub_footer_sidebar_names = array();
	
	public function __construct(){
		$this->besmart_sidebar_names = array(
			'page'         => esc_html(__('Page Area','besmart')),
			'blog'         => esc_html(__('Blog Area','besmart')),
		);
		$this->besmart_sidebar_desc = array(
			'page_desc'         => esc_html(__('Main Sidebar that appears in left / right sidebar template.','besmart')),
			'blog_desc'         => esc_html(__('Appears in blog page template & single blog posts.','besmart')),
		);
		$this->besmart_footer_sidebar_names = array(
			esc_html(__('Footer 1 Area','besmart')),
			esc_html(__('Footer 2 Area','besmart')),
			esc_html(__('Footer 3 Area','besmart')),
			esc_html(__('Footer 4 Area','besmart')),
			esc_html(__('Footer 5 Area','besmart')),
			esc_html(__('Footer 6 Area','besmart')),
		);
	}

	function besmart_register_sidebar(){
		
		foreach ($this->besmart_sidebar_names as $slug => $name){
			$desc = $this->besmart_sidebar_desc[$slug.'_desc'];
			
			register_sidebar(array(
				'id'			=> 'sidebar-'.$slug,
				'name'          => $name,
				'description'   => $desc,
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h3 class="widgettitle"><span>',
				'after_title'   => '</span></h3>',
			));
		}
		
		register_sidebar(array(
			'id'            => "sidebar-footer-top-area",
			'name'          =>  esc_html(__('Footer Top Area','besmart')),
			'description'   =>  esc_html(__('Appears in the Footer Top Area of the site.','besmart')),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '',
			'after_title'   => '',
		));
		
		//register footer sidebars
		foreach ($this->besmart_footer_sidebar_names as $slug => $name){
			register_sidebar(array(
				'name'          => $name,
				'id'			=> 'sidebar-'.$slug,
				'description'   =>  esc_html(__('Appears in the Footer Area of the site.','besmart')),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h4 class="widgettitle"><span>',
				'after_title'   => '</span></h4>',
			));
		}
		
		//register sub footer sidebars
		register_sidebar(array(
			'id'            => "sidebar-footer-bottom-area",
			'name'          =>  esc_html(__('Footer Bottom Area','besmart')),
			'description'   =>  esc_html(__('Appears in the Footer Bottom Area of the site.','besmart')),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '',
			'after_title'   => '',
		));
		
		//register header sidebars
		register_sidebar(array(
			'id'            => "sidebar-header-area",
			'name'          =>  esc_html(__('Header Area','besmart')),
			'description'   =>  esc_html(__('Appears in the Header Area of the site.','besmart')),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="widgettitle">',
			'after_title'   => '</h3>',
		));
		
		//register portfolio sidebars
		if(class_exists('Whoathemes_Portfolio')){
			register_sidebar(array(
				'id'            => "sidebar-portfolio-area",
				'name'          =>  esc_html(__('Portfolio Area','besmart')),
				'description'   =>  esc_html(__('Appears in single portfolio posts.','besmart')),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widgettitle">',
				'after_title'   => '</h3>',
			));
		}
		
		//register woocommerce sidebars
		if(besmart_get_option('general', 'woocommerce')){
			register_sidebar(array(
				'id'            => "sidebar-product-area",
				'name'          =>  esc_html(__('Products Area','besmart')),
				'description'   =>  esc_html(__('Appears in your shop.','besmart')),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widgettitle">',
				'after_title'   => '</h3>',
			));
		}
		
		//register custom sidebars
		$besmart_custom_sidebars = besmart_get_option('sidebar','sidebars');
		if(!empty($besmart_custom_sidebars)){
			$besmart_custom_sidebar_names = explode(',',$besmart_custom_sidebars);
			foreach ($besmart_custom_sidebar_names as $name){
				$slug = strtolower(preg_replace('/[^a-zA-Z0-9-]/', '-', $name));
				register_sidebar(array(			
					'name'          => $name,	
					'id' 			=> 'sidebar-custom-'.$slug,
					'description'   =>  $name. ' custom sidebar',
					'before_widget' => '<section id="%1$s" class="widget %2$s">',
					'after_widget'  => '</section>',
					'before_title'  => '<h3 class="widgettitle">',
					'after_title'   => '</h3>',
				));
			}
		}
	}
	
	function besmart_get_sidebar($post_id){
		if(is_page()){
			$besmart_sidebar = $this->besmart_sidebar_names['page'];
		}
		if(is_home() || is_front_page() || $post_id == besmart_get_option('homepage','one_page_home') ){
			$home_page_id = besmart_get_option('homepage','one_page_home');
			$post_id = get_object_id($home_page_id,'page');
				$besmart_sidebar = $this->besmart_sidebar_names['page'];
		}
		if(is_page_template( 'template_blog.php' )){
			$besmart_sidebar = $this->besmart_sidebar_names['blog'];
		}
		if(is_singular('post')){
			$besmart_sidebar = $this->besmart_sidebar_names['blog'];
		}elseif(is_singular('wt_portfolio')){
			$besmart_sidebar = dynamic_sidebar( esc_html(__('Portfolio Area','besmart')));
		}
		if(is_search() || is_archive()){
			$besmart_sidebar = $this->besmart_sidebar_names['blog'];
		}
		if (besmart_get_option('general', 'woocommerce')) {
			if(is_woocommerce() || is_cart() || is_checkout() || is_account_page()){
				$besmart_sidebar = dynamic_sidebar( esc_html(__('Products Area','besmart')));
			}
		}
		if(!empty($post_id) && !is_archive()){
			$besmart_custom = get_post_meta($post_id, '_sidebar', true);
			if(!empty($besmart_custom)){
				$besmart_sidebar = $besmart_custom;
			}
		}
		if(isset($besmart_sidebar)){
			dynamic_sidebar($besmart_sidebar);
		}
	}
	function besmart_get_footer_sidebar(){
		dynamic_sidebar($this->besmart_footer_sidebar_names[$this->besmart_footer_sidebar_count]);
		$this->besmart_footer_sidebar_count++;
	}
}
global $_besmart_sidebar;
$_besmart_sidebar = new besmart_sidebar;

add_action('widgets_init', array($_besmart_sidebar,'besmart_register_sidebar'));

function besmart_sidebar_generator($function){
	global $_besmart_sidebar;
	$args = array_slice( func_get_args(), 1 );
	return call_user_func_array(array( &$_besmart_sidebar, $function ), $args );
}