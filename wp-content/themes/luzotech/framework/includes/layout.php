<?php 
global $layout;
if (is_front_page()) {
	$layout = 'full';	
}
elseif (!is_search() && !is_404() && $post->post_type == 'wt_portfolio') {
		$layout = get_post_meta($post->ID, '_sidebar_alignment', true);
	if((empty($layout) || $layout == 'default') && !is_search()){
		$layout= besmart_get_option('portfolio','layout');
	}
	elseif (is_search()) {
		$layout= besmart_get_option('blog','layout');
	}
}
elseif (is_page_template( 'template_blog.php' )  || is_tag() ) {
	$layout= besmart_get_option('blog','layout');
}
elseif (is_single()) {
	$layout = get_post_meta($post->ID, '_sidebar_alignment', true);
	if(empty($layout) || $layout == 'default'){
		$layout= besmart_get_option('blog','single_layout');
	}
}
elseif (is_archive()) {
	if (is_tax('wt_portfolio')) {
		$layout = besmart_get_option('portfolio','layout');
	}
	elseif ((besmart_get_option('general', 'woocommerce')) && (is_woocommerce() || is_cart() || is_checkout() || is_account_page())) {
		$layout = 'right';
	}
	else {
		$layout = 'right';
	}
}
elseif (is_search() ) {
	$layout= besmart_get_option('blog','layout');
}
elseif (is_page_template('template_left_sidebar.php') ) {
    $layout = 'left';
}
elseif (is_page_template('template_right_sidebar.php') ) {
    $layout = 'right';
}
elseif (is_page_template('template_fullwidth.php') || is_404()) {
	$layout = 'full';
}
else {
	$layout = 'full';
}

if(!is_search()) {
$type          = get_post_meta($post->ID, '_intro_type', true);
$bg            = besmart_check_input(get_post_meta($post->ID, '_page_bg', true));
$bg_position   = get_post_meta($post->ID, '_page_position_x', true);
$bg_repeat     = get_post_meta($post->ID, '_page_repeat', true);
$color         = get_post_meta($post->ID, '_page_bg_color', true);
} else {
$type          = 'default'; 
}
$homeContent   = besmart_get_option('general', 'home_page');
$stickyHeader  = besmart_get_option('general', 'sticky_header');
$noStickyOnSS  = besmart_get_option('general', 'no_sticky_on_ss');
$retinaLogo    = besmart_get_option('general', 'logo_retina');
$enable_retina = besmart_get_option('general', 'enable_retina');
$animations    = besmart_get_option('general','enable_animation');
$besmart_pageLoader    = besmart_get_option('general','page_loader');
$besmart_responsiveNav = besmart_get_option('general', 'responsive_nav');
$menu_type     = 'top';

if($stickyHeader) {
	$navbar = ' navbar-fixed-top';
}else{
	$navbar = ' navbar-static-top';
}

if(!empty($color) && $color != "transparent"){
	$color = 'background-color:'.$color.';';
}else{
	$color = '';
}
if(!empty($bg)){
	$bg = 'background-image:url('.$bg.');background-position:top '.$bg_position.';background-repeat:'.$bg_repeat.'';
}else{
	$bg = '';
}
if ($stickyHeader) {
	wp_enqueue_script('jquery-sticky');
}
if($besmart_smoothScroll) {
	wp_enqueue_script('smooth-scroll');
}
?>