<?php
besmart_setPostViews(get_the_ID());
$blog_page = besmart_get_option('blog','blog_page');
if($blog_page == $post->ID){
	return require(get_template_directory() . "/template_blog.php");
}
$featured_image_type = besmart_get_option('blog', 'single_featured_image_type');
$type = get_post_meta($post->ID, '_intro_type', true);
?>
<?php get_header(); ?>
</div> <!-- End headerWrapper -->
<div id="wt_containerWrapper" class="clearfix">
    <?php besmart_generator('besmart_breadcrumbs',$post->ID); ?>
	<?php besmart_generator('besmart_custom_header',$post->ID); ?>
    <?php besmart_generator('besmart_containerWrapp',$post->ID);?>
        <div id="wt_container" class="clearfix">
            <?php besmart_generator('besmart_content',$post->ID);?>
                <div class="container">
                    <div class="row">
						<?php if($layout == 'full') {
                            echo '<div class="col-md-12">';
                        }?>
						<?php if($layout == 'left') {
                            echo '<aside id="besmart_sidebar" class="col-md-3">';
                            get_sidebar(); 
                            echo '</aside> <!-- End besmart_sidebar -->'; 
                        }?>
                        <?php if($layout != 'full') {
                            echo '<div id="wt_main" role="main" class="col-md-9">'; 
                            echo '<div id="wt_mainInner">';
                        }?> 
                        <article id="post-<?php the_ID(); ?>" class="blogEntry wt_single wt_entry_<?php echo esc_attr( $featured_image_type );?>">
                            <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                            <?php 
                            if(besmart_is_enabled(get_post_meta($post->ID, '_featured_image', true), besmart_get_option('blog','featured_image'))){ ?>
                            <figure>
                            <?php
                            $thumbnail_type = get_post_meta($post->ID, '_thumbnail_type', true);
                                switch($thumbnail_type){					
                                    case "timage" : 
                                        echo besmart_generator('besmart_blog_featured_image',$featured_image_type,$layout);
                                        break;
                                    case "tvideo" : 
                                        $video_link = get_post_meta($post->ID, '_featured_video', true);
                                        echo '<div class="blog-thumbnail-video">';
                                        echo besmart_video_featured($video_link,$featured_image_type,$layout,$height='',$width='');
                                        echo '</div>';							
                                        break;
                                    case "tplayer" : 
                                        $player_link = get_post_meta($post->ID,'_thumbnail_player', true);
                                        echo '<div class="blog-thumbnail-player">';
                                        echo besmart_media_player($featured_image_type,$layout,$player_link);
                                        echo '</div>';							
                                        break;
                                    case "tslide" : 
                                        echo '<div class="blog-thumbnail-slide">';
                                        echo besmart_get_slide($featured_image_type,$layout);
                                        echo '</div>';							
                                        break;
                                }
                            ?> </figure> <?php	
                            }				
                            ?>
                            
                            <footer class="blogEntry_metadata">
                            <?php if (besmart_get_option('blog','single_meta_date')){
                            } ?>
                                <?php echo besmart_generator('besmart_blog_single_meta'); 
                                ?>
                            </footer>
                            <?php the_content(); ?>
                             <?php wp_link_pages( array( 'before' => '<div class="wp-pagenavi post_navi"><span class="page-links-title">' . esc_html__( 'Pages:', 'besmart' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
                                <?php edit_post_link(esc_html__('Edit', 'besmart'),'<p class="entry_edit">','</p>'); ?>
                                <?php if(besmart_get_option('blog','author') || besmart_get_option('blog','related_popular')):?><footer><?php endif; ?>
                                    <?php if(besmart_get_option('blog','author')):besmart_generator('besmart_blog_author_info');endif;?>
                                                      
                                <?php if(besmart_get_option('blog','author') || besmart_get_option('blog','related_popular')):?></footer><?php endif; ?>
                                <?php if(besmart_get_option('blog','entry_navigation')):?>
                                <div class="entry_navigation">
                                    <div class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . esc_html_x( '&larr;', 'Previous post link', 'besmart' ) . '</span> %title', true ); ?></div>
                                    <div class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . esc_html_x( '&rarr;', 'Next post link', 'besmart' ) . '</span>', true ); ?></div>
                                </div>
                                <?php endif;?>
                            <?php comments_template( '', true ); ?>
                            <?php //comment_form(); ?>
                            <?php endwhile; // end of the loop.?>
                        </article> <!-- End blogEntry -->
                        
                        <?php if($layout != 'full') {
                            echo '</div> <!-- End wt_mainInner -->'; 
                            echo '</div> <!-- End wt_main -->'; 
                        }?>
                        
                        <?php if($layout == 'right') {
                            echo '<aside id="besmart_sidebar" class="col-md-3">';
                            get_sidebar(); 
                            echo '</aside> <!-- End besmart_sidebar -->'; 
                        }?>                        
                        
						<?php if($layout == 'full') {
                            echo '</div>';
                        }?>
                    </div> <!-- End row -->
                </div> <!-- End container -->
            </div> <!-- End wt_content -->
        </div> <!-- End wt_container -->
    </div> <!-- End wt_containerWrapp -->
</div> <!-- End wt_containerWrapper -->
<?php get_footer(); ?>
