<?php
besmart_setPostViews(get_the_ID());
$blog_page = besmart_get_option('blog','blog_page');
if($blog_page == $post->ID){
	return require(get_template_directory() . "/template_blog.php");
}
$featured_image_type = besmart_get_option('portfolio', 'layout');
$type = get_post_meta($post->ID, '_intro_type', true);
$layout= besmart_get_option('portfolio','layout');
$terms = get_the_terms(get_the_ID(), 'wt_portfolio_category');
?>
<?php get_header(); ?>
</div> <!-- End headerWrapper -->
<div id="wt_containerWrapper" class="clearfix">
	<?php besmart_generator('besmart_breadcrumbs',$post->ID); ?>
    <?php besmart_generator('besmart_custom_header',$post->ID); ?>
    <?php besmart_generator('besmart_containerWrapp',$post->ID);?>
        <div id="wt_container" class="clearfix">
            <?php besmart_generator('besmart_content',$post->ID);?>
                <div class="container">
                	<div class="row"> 
						<?php if($layout == 'full') {
                            echo '<div class="col-md-12">';
                        }?>
						<?php if($layout == 'left') {
                            echo '<aside id="besmart_sidebar" class="col-md-3">';
                            get_sidebar(); 
                            echo '</aside> <!-- End besmart_sidebar -->'; 
                        }?>
                        <?php if($layout != 'full') {
                            echo '<div id="wt_main" role="main" class="col-md-9">'; 
                            echo '<div id="wt_mainInner">'; 
                        }?>
                        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                            <article id="post-<?php the_ID(); ?>" class="portEntry">
                                <?php if(besmart_get_option('portfolio','featured_image')):?>
                                    <?php echo besmart_generator('besmart_portfolio_featured_image'); ?>
                                <?php endif; ?>
                               <?php //besmart_generator('besmart_breadcrumbs',$post->ID); ?>
                                <div class="portEntry_content">
                                <?php the_content(); ?> 
                                <?php wp_link_pages( array( 'before' => '<div class="wp-pagenavi post_navi"><span class="page-links-title">' . esc_html__( 'Pages:', 'besmart' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
                                </div>
                                
                                
                            </article>     
                            
                            <?php if($layout != 'full') {
                                echo '</div> <!-- End wt_mainInner -->'; 
                                echo '</div> <!-- End wt_main -->'; 
                                
                            } ?>
                            <?php if($layout == 'right') {
							   echo '<aside id="besmart_sidebar" class="col-md-3">';
                               get_sidebar(); 
                               echo '</aside> <!-- End besmart_sidebar -->'; 
                            }
                            ?>
                            
                            <?php if(besmart_get_option('portfolio','single_navigation')):?>
                                <div class="entry_navigation">
                                    <div class="nav-previous">
                                        <?php besmart_previous_post_link_plus(); ?>
                                    </div>
                                    <div class="nav-next">
                                        <?php besmart_next_post_link_plus(); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        
                            <?php if(besmart_get_option('portfolio','enable_comment')) comments_template( '', true ); ?>
                        <?php endwhile; // end of the loop ?>
                        
						<?php if($layout == 'full') {
                            echo '</div>';
                        }?>
                    </div> <!-- End row -->
            	</div> <!-- End container -->
            </div> <!-- End wt_content -->
        </div> <!-- End wt_container -->
    </div> <!-- End wt_containerWrapp -->
</div> <!-- End wt_containerWrapper -->
<?php get_footer(); ?>