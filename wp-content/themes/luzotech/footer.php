<?php besmart_generator('besmart_footerWrapper',$post->ID);?>

<?php if(besmart_get_option('footer','footer_top')):?>
<?php besmart_generator('besmart_footerTop',$post->ID);?>
	<?php if(is_active_sidebar('sidebar-footer-top-area')){dynamic_sidebar('sidebar-footer-top-area'); } ?>         
	</div> <!-- End container -->
</footer> <!-- End footerTop -->
<?php endif;?>

<?php if(besmart_get_option('footer','footer')):?>
<?php besmart_generator('besmart_footer',$post->ID);?>
	<div class="container">
		<div class="row">
			<?php get_template_part( 'template-parts/footer-column' ); ?>      
		</div> <!-- End row -->
	</div> <!-- End container -->
</footer> <!-- End footer -->
<?php endif;?>
<?php if(besmart_get_option('footer','sub_footer')):?>
<?php besmart_generator('besmart_footerBottom',$post->ID);?>
		<?php if(besmart_get_option('footer','copyright')):?>
			<div id="copyright">
				<p class="copyright">
                <?php echo wp_kses(stripslashes(besmart_get_option('footer','copyright')), besmart_allowed_tags()); ?>
			</div>
		<?php endif;?>
        	<?php if(is_active_sidebar('sidebar-footer-bottom-area')){dynamic_sidebar('sidebar-footer-bottom-area'); } ?> 
	</div> <!-- End container -->
</footer> <!-- End footerBottom -->
<?php endif;?>
<?php if(!class_exists( 'Qodux_Framework' )) { ?>
<footer id="wt_footer" class="clearfix">	
	<div class="container">
		<div class="row">
			<div class="wt_footer_col col-md-12">
			
			<?php if(is_active_sidebar('sidebar-0')): ?>
                    <?php qodx_generator('qodx_footer_sidebar'); ?>
                <?php else: ?>
                	<div class="copyright-block">
                        <div class="copyright-block-container">
                            <p class="wt_copyright"><?php esc_html_e('© 2016 Your Website, all rights reserved.', 'besmart'); ?></p>
                        </div>
                    </div> 
                <?php endif; ?>
            </div>
        </div>
    </div>
</footer>
<?php } ?>
</div> <!-- End footerWrapper -->

</div> <!-- End wt_page -->
</div> <!-- End wrapper -->
<?php besmart_scripts(); ?>
<?php wp_footer(); ?>
</body>
</html>