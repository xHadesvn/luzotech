<!DOCTYPE html>
<?php 
if(besmart_get_option('general','enable_responsive')){ 
	$besmart_responsive = 'responsive ';
} else {
	$besmart_responsive = '';
}
$besmart_smoothScroll = besmart_get_option('general', 'smooth_scroll');
?>
<!--[if lt IE 7]><html class="<?php echo esc_attr( $besmart_responsive ); ?>no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]><html class="<?php echo esc_attr( $besmart_responsive ); ?>no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]><html class="<?php echo esc_attr( $besmart_responsive ); ?>no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<html class="<?php echo esc_attr( $besmart_responsive ); ?><?php if($besmart_smoothScroll):?>wt-smooth-scrolling <?php endif; ?>no-js" <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php 
if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
<?php 
if($besmart_favicon = besmart_get_option('general','favicon')) { ?>
<link rel="shortcut icon" href="<?php echo esc_url($besmart_favicon); ?>" />
<?php } 
if($besmart_favicon_57 = besmart_get_option('general','favicon_57')) { ?>
<link rel="apple-touch-icon" href="<?php echo esc_url($besmart_favicon_57); ?>" />
<?php } 
if($besmart_favicon_72 = besmart_get_option('general','favicon_72')) { ?>
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo esc_url($besmart_favicon_72); ?>" />
<?php } 
if($besmart_favicon_114 = besmart_get_option('general','favicon_114')) { ?>
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo esc_url($besmart_favicon_114); ?>" />
<?php } 
if($besmart_favicon_144 = besmart_get_option('general','favicon_144')) { ?>
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo esc_url($besmart_favicon_144); ?>" />
<?php }
} else {
    // display a message advising the user to use the site icon feature
} ?>
<?php wp_head(); ?>
</head>
<?php require_once (get_template_directory() . '/framework/includes/layout.php'); ?>
<body <?php body_class(); ?>>

<?php if($besmart_pageLoader){ ?>
    <div id="wt_loader"><div class="wt_loader_html"></div></div>
<?php } ?>
<div id="wt_wrapper" class="<?php if($layout=='right'):?>withSidebar rightSidebar<?php endif;?><?php if($layout=='left'):?>withSidebar leftSidebar<?php endif;?><?php if($layout=='full'):?> fullWidth<?php endif; ?><?php if($stickyHeader):?> wt_stickyHeader<?php endif; ?><?php if($noStickyOnSS):?> wt_noSticky_on_ss<?php endif; ?><?php if($type=='disable'):?> wt_intro_disabled<?php endif; ?><?php if($animations):?> wt_animations<?php endif; ?><?php if($menu_type == "top"):?> wt_nav_top<?php else:?> wt_nav_side<?php endif; ?> clearfix" <?php if(!empty($color) || !empty($bg)){echo' style="'.$color.''.$bg.'"';} ?>>
<div id="wt_page" class="<?php if(besmart_get_option('general','layout_style')== 'wt_boxed'){echo 'wt_boxed';} else {echo 'wt_wide';} ?>">
<?php if(is_search()) {
		echo '<div id="wt_headerWrapper" role="banner" class="clearfix">';
		echo '<header id="wt_header" class="'.$besmart_responsiveNav.'navbar'.$navbar.' clearfix" role="banner">';
    } else { 
		besmart_generator('besmart_headerWrapper',$post->ID);
		besmart_generator('besmart_header',$post->ID); }?>
    	<div class="container">
			<?php if($custom_logo = besmart_get_option('general','logo')): ?>			
                <div id="logo" class="navbar-header">
                    <?php if($enable_retina): ?>
                            <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo besmart_get_option('general','logo'); ?>" data-at2x="<?php echo esc_attr( $retinaLogo ); ?>" alt="" /></a>
                    <?php else:?>
                            <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo besmart_get_option('general','logo'); ?>" alt="" /></a>
                    <?php endif; ?> 
                </div>
            <?php else:?>
                <div id="logo" class="navbar-header">
                    <a class="navbar-brand nav_description" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>
                <?php if(empty($custom_logo) && besmart_get_option('general','display_site_desc')){
                        $site_desc = get_bloginfo( 'description' );
                        if(!empty($site_desc)):?>
                        <div id="siteDescription"><?php bloginfo( 'description' ); ?></div>
                <?php endif;}?>
                </div>
            <?php endif; ?>  
            
            <!-- Header Widget -->       
            <div id="headerWidget"><?php if(is_active_sidebar('sidebar-header-area')){dynamic_sidebar('sidebar-header-area'); } ?></div>  
            <?php  		
            if ( $besmart_responsiveNav == 'drop_down' ) { 
                wp_enqueue_script('mobileMenu');
            }			
            ?> 
            <!-- Navigation -->
            <?php 
			if(!is_search() || is_404()) {
				besmart_generator('besmart_nav',$post->ID);
			} else {
				echo '<nav id="nav" class="wt_nav_top collapse navbar-collapse" role="navigation" data-select-name="-- Main Menu --">';}?>      
            <?php  if ( has_nav_menu( 'primary-menu' ) ) {
                if ( ( $locations = get_nav_menu_locations() ) && $locations['primary-menu'] && (empty($homeContent)) && (!is_front_page() || !is_blog() )) {
				besmart_generator('besmart_menu_menu');
			} else {
				besmart_generator('besmart_menu_menu_one_page');
			}
            } else {
            echo '<ul class="menu nav navbar-nav navbar-right">';
                $short_walker = new besmart_My_Page_Walker; wp_list_pages(array( 'walker' => $short_walker,'link_before' => '<span>','link_after' => '</span>','title_li' => '' ));
            echo '</ul>';
            }
            ?>
            </nav>
		</div> 	<!-- End container -->    
	</header> <!-- End header --> <?php echo "\n"; ?> 