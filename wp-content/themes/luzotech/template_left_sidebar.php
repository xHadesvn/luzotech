<?php
/*
Template Name: Left Sidebar
*/
if(is_blog()){
	return require(get_template_directory() . "/template_blog.php");
}
$type = get_post_meta($post->ID, '_intro_type', true);
$intro_title = get_post_meta($post->ID, '_intro_title', true);
$intro_text = get_post_meta($post->ID, '_intro_text', true);
?>
<?php get_header(); ?>
</div> <!-- End headerWrapper -->
<div id="wt_containerWrapper" class="clearfix">
	<?php besmart_generator('besmart_breadcrumbs',$post->ID); ?>
    <?php besmart_generator('besmart_custom_header',$post->ID); ?>
    <?php besmart_generator('besmart_containerWrapp',$post->ID);?>
        <div id="wt_container" class="clearfix">
            <?php besmart_generator('besmart_content',$post->ID);?>
				<?php
                if (!empty($intro_title) || !empty($intro_text)) {
                    echo '<div class="intro_box wt_animate wt_animate_if_visible" data-animation="fadeInUp">';
                    echo apply_filters('the_content', get_post_meta($post->ID, '_intro_title', true));
                    echo apply_filters('the_content', get_post_meta($post->ID, '_intro_text', true));
                    echo '</div>';
                } ?>
                <div class="container">
                    <div class="row">
                        <aside id="besmart_sidebar" class="col-md-3">
                        <?php get_sidebar(); ?>
                        </aside>  <!-- End besmart_sidebar -->	
                        <div id="wt_main" role="main" class="col-md-9">
                            <div id="wt_mainInner">
                                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                    <?php if(has_post_thumbnail()): ?>
                                    <div class="styled_image">
                                        <?php the_post_thumbnail('full'); ?>
                                    </div>
                                    <?php endif; ?>
                                     <?php the_content(); ?>
                                <?php endwhile; else: ?>
                                <?php endif; ?>
                            </div>  <!-- End wt_mainInner -->
                        </div> <!-- End wt_main -->
						 <?php comments_template( '', true ); ?>
                         <?php //comment_form(); ?>
                	</div> <!-- End wt_row -->
                </div> <!-- End container -->
            </div> <!-- End wt_content -->
        </div> <!-- End wt_container -->
    </div> <!-- End wt_containerWrapp -->
</div> <!-- End wt_containerWrapper -->
<?php get_footer(); ?>