<?php 
/*
Template Name: Blog Page
*/
get_header(); ?>
<?php
global $blog_page_id, $r;
?>
</div> <!-- End headerWrapper -->
<div id="wt_containerWrapper" class="clearfix">
    <?php besmart_generator('besmart_breadcrumbs',$post->ID); ?>
	<?php besmart_generator('besmart_custom_header',$post->ID); ?>
    <?php besmart_generator('besmart_containerWrapp',$post->ID);?>
        <div id="wt_container" class="clearfix">
            <?php besmart_generator('besmart_content',$post->ID);?>
                <div class="container">
                    <div class="row">
                    	<div class="wt_spacer_sc wt_clearboth"></div>
                    	<?php if($layout == 'full') {
                            echo '<div id="wt_content_inner" class="col-md-12">';
						}?>
						<?php if($layout == 'left') {
                            echo '<aside id="besmart_sidebar" class="col-md-3">';
                            get_sidebar(); 
                            echo '</aside> <!-- End besmart_sidebar -->'; 
                        }?>
                        <?php if($layout != 'full') {
                            echo '<div id="wt_main" role="main" class="col-md-9">'; 
                            echo '<div id="wt_mainInner">';
                        }?>
                            <?php 
                                $featured_image_type = besmart_get_option('blog', 'featured_image_type');
								$layout = besmart_get_option('blog','layout');
								
								?>
								<?php 
								$exclude_cats = besmart_get_option('blog','exclude_categorys');
								$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
								if(is_array($exclude_cats)){
									foreach ($exclude_cats as $key => $value) {
										$exclude_cats[$key] = -$value;
									}
									$query = array(
										'post_type' => 'post',
										'cat' => implode(",",$exclude_cats),
										'category__and'       => '',
										'paged' => $paged
									);
								} else {
									$query = array(
										'post_type' => 'post',
										'category__and'       => '',
										'paged' => $paged
									);
								}
								$r = new WP_Query($query);
										while($r->have_posts()) {
											$r->the_post(); ?>
									<article id="post-<?php the_ID(); ?>" class="blogEntry">
										<div <?php post_class(); ?>>
											  
										<?php    
											$thumbnail_type = get_post_meta($post->ID, '_thumbnail_type', true);
												switch($thumbnail_type){					
													case "timage" : 
													if ( has_post_thumbnail() ) {
														echo '<header class="blogEntry_frame entry_' . $featured_image_type . '">  ';
														echo besmart_generator('besmart_blog_featured_image',$featured_image_type,$layout); 
														echo '</header>';	
														}
														break;
													case "tvideo" : 
														$video_link = get_post_meta($post->ID, '_featured_video', true);
														echo '<header class="blogEntry_frame entry_' . $featured_image_type . '">  ';
														echo '<div class="blog-thumbnail-video">';
														echo besmart_video_featured($video_link,$featured_image_type,$layout,$height='',$width='');
														echo '</div>';	
														echo '</header>';													
														break;
													case "tplayer" : 
														$player_link = get_post_meta($post->ID,'_thumbnail_player', true);
														echo '<header class="blogEntry_frame entry_' . $featured_image_type . '">  ';
														echo '<div class="blog-thumbnail-player">';
														echo besmart_media_player($featured_image_type,$layout,$player_link);
														echo '</div>';	
														echo '</header>';													
														break;
													case "tslide" : 
														echo '<header class="blogEntry_frame entry_' . $featured_image_type . '">  ';
														echo '<div class="blog-thumbnail-slide">';
														echo besmart_get_slide($featured_image_type,$layout);
														echo '</div>';	
														echo '</header>';													
														break;
												}
											 ?>
										
										<div class="blogEntry_content">
											<h2 class="blogEntry_title"><a href="<?php echo get_permalink() ?>" rel="bookmark" title="<?php printf( esc_html__("Permanent Link to %s", 'besmart'), get_the_title() ); ?>"><?php the_title(); ?></a></h2>
										<?php if (!is_search()): ?>
										<footer class="blogEntry_metadata">
											<?php echo besmart_generator('besmart_blog_meta'); ?>
										</footer>
										<?php endif; ?>
										<?php 
											if(besmart_get_option('blog','display_full')):
												global $more;
												$more = 0;
												the_content(esc_html__('Read more &raquo;','besmart'),false);
											else:
												the_excerpt();
										?>
										<?php wp_link_pages( array( 'before' => '<div class="wp-pagenavi post_navi"><span class="page-links-title">' . esc_html__( 'Pages:', 'besmart' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
										<a class="read_more_link" href="<?php the_permalink(); ?>"><?php echo esc_html__('Read more &raquo;','besmart')?></a>
										<?php endif; ?>
										</div>
										<?php if ($featured_image_type == 'left') { echo '<div class="wt_clearboth"></div>'; } ?>        
										</div>
									</article>
								<?php wp_reset_postdata(); }
								if (function_exists("besmart_blog_pagenavi")) {
									besmart_blog_pagenavi('','', $r, $paged);
								} 
								 ?>
                        <?php if($layout != 'full') {
                            echo '</div> <!-- End wt_mainInner -->'; 
                            echo '</div> <!-- End wt_main -->'; 
                        }?>
                        <?php if($layout == 'right') {
                            echo '<aside id="besmart_sidebar" class="col-md-3">';
                            get_sidebar(); 
                            echo '</aside> <!-- End besmart_sidebar -->'; 
                        }?>                 
                    	<?php if($layout == 'full') {
                            echo '</div>'; // End "wt_content_inner" if layout is full
						}?>
                    </div> <!-- End row -->
                </div> <!-- End container -->
            </div> <!-- End wt_content -->
        </div> <!-- End wt_container -->
    </div> <!-- End wt_containerWrapp -->
</div> <!-- End wt_containerWrapper -->
<?php get_footer(); ?>