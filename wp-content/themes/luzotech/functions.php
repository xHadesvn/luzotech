<?php
require_once get_template_directory() . '/framework/theme-files.php';
$theme = new besmart_ThemeFiles();
$theme->besmart_init(array());

/**
 * JavaScripts In Header
 */
function besmart_enqueue_scripts() {
	if(is_admin()){
		return;
	}
	
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/vendor/modernizr-2.6.1.min.js', array('jquery'), null, false);
	wp_enqueue_script( 'besmart-conditional', get_template_directory_uri() . '/js/vendor/conditional.js', array('jquery'), null, true);	
	$bgType = besmart_get_option('background','background_type');
	if($bgType == 'slideshow') {
		wp_enqueue_script( 'besmart-main', get_template_directory_uri() . '/js/main.js', array('jquery','jquery-supersized','jquery-supersized-shutter'), null, true);
	} else {
		wp_enqueue_script( 'besmart-main', get_template_directory_uri() . '/js/main.js', array('jquery', 'besmart-conditional'), null, true);}
		
	wp_enqueue_script( 'besmart-plugins', get_template_directory_uri() . '/js/plugins.js', array('jquery'), null, true);
	wp_enqueue_script( 'fitvids', get_template_directory_uri() . '/js/vendor/jquery.fitvids.js', array('jquery'), null, true);		
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/vendor/bootstrap.min.js', array('jquery'), null, true);	
		
	if(besmart_get_option('general','enable_retina')){
		wp_enqueue_script( 'retina', get_template_directory_uri() . '/js/vendor/retina.js', array('jquery'), null, true);	
	}
	if ( is_singular() && comments_open() ){
		wp_enqueue_script( 'comment-reply', true, true, true, true );
	}
	wp_register_script( 'jquery-supersized', get_template_directory_uri() . '/js/vendor/supersized.3.2.7.min.js', array('jquery'), null, true);
	wp_register_script( 'jquery-supersized-shutter', get_template_directory_uri() . '/js/vendor/supersized.shutter.min.js', array('jquery'), null, true);
		
	wp_register_script( 'nivo-slider', get_template_directory_uri() . '/js/vendor/jquery.nivo.slider.pack.js', array('jquery'), null, true);
	wp_register_script( 'flexslider', get_template_directory_uri() . '/js/vendor/jquery.flexslider-min.js', array('jquery'), null, true);
	wp_register_script( 'owlCarousel', get_template_directory_uri() . '/js/vendor/jquery.owlCarousel.js', array('jquery'), null, true);	
		
	wp_register_script( 'mobileMenu', get_template_directory_uri() . '/js/vendor/init.mobileMenu.js', array('jquery'), null, true);
	wp_register_script( 'jquery-sticky', get_template_directory_uri() . '/js/vendor/jquery.sticky.js', array('jquery'), null, true);		
	wp_register_script( 'smooth-scroll', get_template_directory_uri() . '/js/vendor/SmoothScroll.min.js', array('jquery'), null, true);
	wp_register_script( 'jquery-uisearch', get_template_directory_uri() . '/js/vendor/jquery.uisearch.js', array('jquery'), null, true);
	wp_register_script( 'jquery-isotope', get_template_directory_uri() . '/js/vendor/jquery.isotope.min.js', array('jquery'), null, true);	
	
	wp_register_script( 'validate', get_template_directory_uri() . '/js/vendor/validator/jquery.validate.js', array('jquery'), null, true);	
	$besmart_locale = get_locale();
	$besmart_spc_locales = array( 'pt_BR', 'pt_PT', 'zh_TW' );
	if ( ! in_array( $besmart_locale, $besmart_spc_locales ) ) {
		$besmart_locale = current( explode( '_', $besmart_locale ) );
	}
	if ($besmart_locale != 'en') {
		wp_register_script( 'validate-translation', get_template_directory_uri() . '/js/vendor/validator/localization/messages_' . $besmart_locale . '.js', array('jquery','validate'), null, true );
	}
	
	wp_register_script( 'jquery-YTPlayer', get_template_directory_uri() . '/js/vendor/jquery.mb.YTPlayer.js', array('jquery'), null, true);
}
add_action('wp_print_scripts', 'besmart_enqueue_scripts');


function besmart_scripts() {
   $data = 'var besmart_uri= "';
   $data .= get_template_directory_uri();
   $data .= '";';
   return wp_scripts()->add_inline_script( 'besmart-conditional', $data, $position = 'before' );
	echo "\n";
	wp_print_scripts('besmart-main');
	wp_print_scripts('besmart-plugins');
	if (class_exists('WPBakeryVisualComposerAbstract')) {
		wp_print_scripts( 'wt-visual-composer-extensions-front');
	}
	wp_print_scripts('fitvids');
	wp_print_scripts('bootstrap');
}

function besmart_enqueue_styles(){
	if(is_admin()){
		return;
	}
	wp_enqueue_style('boostrap', get_template_directory_uri() . '/css/bootstrap.css', array(), null, 'all');
	wp_enqueue_style('besmart-style', get_template_directory_uri() . '/css/main.css', array(), null, 'all');
	if(besmart_get_option('general','woocommerce')){
		wp_enqueue_style('woocommerce', get_template_directory_uri() . '/css/woocommerce.css', array('besmart-style'), null, 'all');
	}
	
	wp_enqueue_style('fontawesome', get_template_directory_uri() . '/css/font-awesome.css', array(), null, 'all');
	wp_enqueue_style('entypo', get_template_directory_uri() . '/css/entypo-fontello.css', array(), null, 'all');
	wp_enqueue_style('besmart-fonts', get_template_directory_uri() . '/css/fonts.css', array(), null, 'all');
	if(besmart_get_option('general','enable_animation')){
		wp_enqueue_style('animate', get_template_directory_uri() . '/css/animate.css', array('besmart-style'), null, 'all');
	}
		
    wp_enqueue_style('prettyPhoto', get_template_directory_uri() . '/css/prettyPhoto.css', array(), null, 'all');
	if(besmart_get_option('general','enable_responsive')){
		wp_enqueue_style('besmart-media-styles', get_template_directory_uri() . '/css/main-media.css', array('besmart-style'), null, 'all');
	}
	
	if(is_multisite()){
		global $blog_id;
		wp_enqueue_style('besmart-skin', get_template_directory_uri() . '/css/skin_'.$blog_id.'.css', array('besmart-style'), false, 'all');
	}else{
		wp_enqueue_style('besmart-skin', get_template_directory_uri() . '/css/skin.css', array('besmart-style'), false, 'all');
	}
		
	wp_register_style('YTPlayer', get_template_directory_uri() . '/css/YTPlayer.css', array('besmart-style'), null, 'all');
	
	wp_register_style('mediaelementjs-styles', get_template_directory_uri().'/js/mediaelement/mediaelementplayer.css', array(), null, 'all');
	wp_register_style('mejs-skins', get_template_directory_uri().'/js/mediaelement/mejs-skins.css', array(), null, 'all');
	
}
add_action('wp_enqueue_scripts', 'besmart_enqueue_styles');

if(besmart_get_option('fonts','enable_googlefonts')){
	function besmart_add_googlefonts_lib(){
		$http = is_ssl() ? 'https' : 'http';
		$fonts = besmart_get_option('fonts','used_googlefonts');
		if(is_array($fonts)){
			foreach ($fonts as $font){
				wp_enqueue_style('font|'.$font,$http.'://fonts.googleapis.com/css?family='.$font.'&subset=latin,latin-ext');
			}
		}
	}
	add_action("wp_enqueue_scripts", 'besmart_add_googlefonts_lib');
}
function besmart_js_method() {
   wp_enqueue_script( 'validate', get_template_directory_uri() . '/js/vendor/validator/jquery.validate.js', array('jquery'), null, true);
   wp_enqueue_script( 'validate-translation' ); 
   wp_add_inline_script( 'validate', 'jQuery(document).ready(function($) { jQuery("#commentform").validate(); });' );
}
add_action( 'wp_enqueue_scripts', 'besmart_js_method' );