<?php
if(is_blog()){
	return require(get_template_directory() . "/template_blog.php");
}
$type = get_post_meta($post->ID, '_intro_type', true);
?>
<?php get_header(); ?>
</div> <!-- End headerWrapper -->
<div id="wt_containerWrapper" class="clearfix">
	<?php besmart_generator('besmart_breadcrumbs',$post->ID); ?>
    <?php besmart_generator('besmart_containerWrapp',$post->ID);?>
        <div id="wt_container" class="clearfix">
            <?php besmart_generator('besmart_content',$post->ID);?>
                <div class="wt_spacer_sc wt_clearboth"></div>
                <div class="container">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <?php if(has_post_thumbnail()): ?>
                        <div class="styled_image">
                            <?php the_post_thumbnail('full'); ?>
                        </div>
                        <?php endif; ?>
                         <?php 
                         the_content(); ?>
                    <?php endwhile; else: ?>
                    <?php endif; ?>
                </div> <!-- End container -->
            </div> <!-- End wt_content -->
        </div> <!-- End wt_container -->
	</div> <!-- End wt_containerWrapp -->
</div> <!-- End wt_containerWrapper -->
<?php get_footer(); ?>