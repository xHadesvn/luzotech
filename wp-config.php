<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define('DB_NAME', 'luzo');

/** Username của database */
define('DB_USER', 'root');

/** Mật khẩu của database */
define('DB_PASSWORD', '');

/** Hostname của database */
define('DB_HOST', 'localhost');

/** Database charset sử dụng để tạo bảng database. */
define('DB_CHARSET', 'utf8mb4');

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ' Pb]5%_W65+9maC%t+V|RdXuzS_X+w*#.Sn_w}4GvXY<Tf?dYCy ]4~~fe>1GN-P');
define('SECURE_AUTH_KEY',  '/Q2sD,lGP?AG`aJ-CqUNlb|U1JO#h,-/GrYu5Q80pruFltElT!L@XYhX:vm42`,F');
define('LOGGED_IN_KEY',    ']TdT4eZ~J[I.W:U5*RJ`grGMULv/^M^EWLIiy1LLrTbL/qWV${-B!cCI~a1<+P94');
define('NONCE_KEY',        'pK:3Sg=&%~%&uNm#3HvA}9w363PK4X!3p<TC!TDi/|,A&Uja[(n@tl,Q_:ljEhLQ');
define('AUTH_SALT',        'B[Vc$k0nn/8,5~WUt3.v,,%/YJz1oOTx%3UWBF-1rLTQ?28&Lr1vc[gn6~:s:%Uj');
define('SECURE_AUTH_SALT', '8#x3ltPXty|&|9ChbJ^+E1OX5$rbu`*GK:viH8NgW9adjQeiyHq*W`!Nuz}W,?a^');
define('LOGGED_IN_SALT',   'ytnC}D)AeJ_L{Wg[J*ISeJ1br}rsZLYC85R#-`]KX~tkna|3__^[<%E~/:mdk7}:');
define('NONCE_SALT',       '+*hGIbzFV1SFcrqJ31Oi&~t0,^24s2SFL,%QB6ukC#9^q.;}.FLAko<o lY$}s%.');

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix  = 'wp_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
